# Developer Documentation

## Repository Structure

`.idea/`: IntelliJ IDEA project settings - see [IDE configuration](#ide-configuration)

`config/`: misc tool configuration data
* `dictionaries/`: custom dictionaries to be used in the IDE
* `gitlab/`: [GitLab](https://gitlab.com) CI configuration

`doc/`: general project documentation

`gradle/`:
* `wrapper/`: the gradle wrapper
* various `.gradle` files that define specific parts of the build - see [scripts](#scripts)

`modules/`: all modules of this project with their sources and resources
* The source code is mainly written in [Kotlin](https://kotlinlang.org/).
* The directory contains a sub-directory for each module.
* See [modules-readme](../modules/readme.md) for details.

## Tools

The project is configured and build using [Gradle](https://docs.gradle.org/current/userguide/userguide.html).
The recommended IDE is [IntelliJ IDEA](https://www.jetbrains.com/idea/).

### Gradle configuration

JDK used by the build:
* The gradle build uses a JDK-9.
* The JDK path must be specified in an environment variable called `JDK9_HOME`.
* The path variable is used directly in the wrapper batch/shell file to specify the `JAVA_HOME` dir for the gradle build.
  This is the only way (I know) of using environment variables for the JDK path and
  still make it possible to use a different JDK in each project.

Local CreatorCat repository:
* To simplify testing of and dependencies between snapshot builds of CreatorCat projects,
  the gradle scripts support a local repository for dependencies and publishing.
* If the property `CCAT_LOCAL_REPO` is defined, it will be used as the local repository path.
* `CCAT_LOCAL_REPO` should be defined in the `gradle.properties` file in `GRADLE_USER_HOME`.

Bintray publishing:
* To enable publishing to [Bintray](https://bintray.com/) the properties `BINTRAY_USER`
  and `BINTRAY_API_KEY` need to be defined in the `gradle.properties` file in `GRADLE_USER_HOME`.
* They may be left out if publishing is not intended.

See [Build and Configuration](#build-and-configuration) for details about the Gradle projects and tasks.

### IDE configuration

Nearly all relevant project settings are stored within this repository, so the project can be opened right away.
* Open the project (*do not use import!*).
* Import the main gradle module e.g. via *"Project Structure.../ Modules/ '+'-button/ import..."*
  (see [IntelliJ doc-page](https://www.jetbrains.com/help/idea/gradle.html#import_gradle_module)).
* The remaining modules should be loaded automatically, then.

The following plugins are also recommended and supported by the settings:
* [Markdown Navigator](https://github.com/vsch/idea-multimarkdown)
* [Save Actions](https://github.com/dubreuia/intellij-plugin-save-actions)
* [Statistic](https://plugins.jetbrains.com/plugin/4509-statistic)

Spell Checking:
* At `config/dictionaries` there are two word list files:
  * `creatorcat.dic` for general and CreatorCat specific words (should only be changed in the template repository)
  * `project.dic` for project specific words, which is meant to be extended for the project
  * Both files are sorted in alphabetical order but case sensitive (lower case first)
* To make IntelliJ aware of those dictionaries, add the path as custom dictionary path in the project settings.
  Unfortunately, this setting cannot be stored in the provided project properties.

Project file templates:
* There are special templates for new files which must be enabled to be used.
* Set *"Editor/ File and Code templates/ Scheme"* to *"Project"*.

## Build and Configuration

The main `src`-project Gradle file:
* configures only little parts of the module projects directly.
* Instead, it loads the [topic specific scripts](#scripts) and provides the `applyCCC` method,
  that applies the configuration handlers (`cccHandlers`) to each module.

The module projects themselves:
* need to define some `ccc`-properties and then call `applyCCC`.
* add specific dependencies etc. as necessary.

### Scripts

The `gradle` folder contains so called *Gradle script plugins*, that each:
* deal with a specific topic of the build e.g. Kotlin compilation, Dokka generation or publishing.
* may perform some general configuration in the `src`-project level.
* add a *configuration handler* to the `cccHandlers` list that will apply the build configuration
  to each module project on calling `applyCCC`.

So called `ccc`-properties adjust the actual outcome of the configuration via a handler:
* The supported properties are listed at the top of each script file.
* Each module project must define the proper `ccc`-properties before calling `applyCCC` in its `build.gradle` file.

## Open Issues

