# PARD Release Notes

For an explanation of the versioning scheme see [the main readme](readme.md#versioning-scheme).

## H-3 - Annotation Comment and Artifact Filter 

Git-Tag: [H-3]($gitTag(H-3))

### pard-cova-annotation 0.2

* Introduced comment property in cova annotations
* Revised link data handling to fully support link properties (like the new comment)

### pard-cova-gradle 0.2

* Revised link data handling to fully support link properties (like the new comment)
* Introduced $dokkaMd(cova-gradle,cova.gradle,KaptBasedCovaAnnoGatheringTask,compilationTarget.)
  property to fix MPP support of `KaptBasedCovaAnnoGatheringTask`
* Added artifact filter support to $dokkaMd(cova-gradle,cova.gradle,PardValidateForwardCoverageTask)

### pard-model 0.3

* Revised link data handling to fully support link properties (like the new comment)
* Added artifact filter support to $dokkaMd(model,model.simple,SimpleModelForwardCoverageValidator)

### pard-lang-gradle 0.4

*internal changes only*

### Dependency Updates

*none*

### For Developers

* Improved link data serialization: use JSON format

## H-2 - Forward Coverage Validation based on Kotlin Annotations 

Git-Tag: [H-2]($gitTag(H-2))

### pard-cova-annotation 0.1

Introduction of annotations that link code to requirements artifacts
and respective annotation processors to gather the link data (see $dokkaMd(cova-annotation)). 

### pard-cova-gradle 0.1

Introduction of the [Pard-Cova Kotlin Plugin](doc/gradleUserguid#pard-cova-kotlin-plugin)
that provides validation of forward requirement coverage based on Kotlin annotations.

### pard-model 0.2

Code-to-artifact link handling:
* Generic code element representation: $dokkaMd(model,model.code,GenericCodeElement)
* $dokkaMd(model,model.code,CodeToArtifactLinkData.)
  and a respective [serializer]($dokka(model,model.code,CodeToArtifactLinkDataSerializer))
* A [forward coverage validator]($dokka(model,model.simple,SimpleModelForwardCoverageValidator)) for simple models

### pard-lang-gradle 0.3

Minor improvements:
* Documentation updates
* Better task dependency handling
* Tasks now have proper groups and descriptions

### Dependency Updates

* Kotiya H-3 -> He-3

### For Developers

Improved $-token dokka link generation for markdown documentation:
* url-style is not necessary anymore for classes, functions and values (no need to escape capital letters anymore)  
* new macro `$$dokkaMd` generates a full link: `[lastArg](dokka-url)`
* improved TODO-Tag removal

Build script improvements:
* `base`-plugin is now already applied in root project so clean works there too
* automatic publishing configuration for gradle plugin projects is now **disabled** because it interferes with
  our own publishing
* new method for module-projects: `excludeTestResource(path)` to exclude generated test resources from IDEA and Gradle 

## H-1 - Pard language plugin and simple model

Git-Tag: [H-1]($gitTag(H-1))

First public availability.
Contains the following modules with separate versions:

### pard-model 0.1

Definition of a [simple requirements model]($dokka(model,model.simple,SimpleRequirementsModel)).

### pard-lang-gradle 0.2

Initial Gradle plugin to [parse]($dokka(lang-gradle,lang.gradle,PardParseTask)) `.pard` files
and generate a simple model.
