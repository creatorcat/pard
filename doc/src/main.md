# PARD - Plain Analyzable Requirements Development

"The Pard is extremely swift. It kills its prey with a single leap."
\- *freely adapted from the [Medieval Bestiary](http://bestiary.ca/beasts/beast116.htm)*

The goal of Pard is to make requirements development as easy and flexible as writing markdown files
while at the same time providing a specification model that allows for sophisticated validation and analysis.
Furthermore, the definition and processing of requirement artifacts
shall be seamlessly integrable into existing common work flows.

## User Guide

For release notes see the [changelog](changelog.md).

Pard is made to be integrated into your Gradle build.
See the [Pard Gradle Plugin Guide](doc/gradleUserguide.md) for how-to and details. 

Pard consists of the following modules:

Module                                                                                  | Version | Supported Platforms
:-------------------------------------------------------------------------------------- | :-----: | :------------------
[pard-model][model-d] ([javadoc][model-j]) - model of structured requirement artifacts  | 0.3     | common, jvm
[pard-lang-gradle][lang-gradle-d] ([javadoc][lang-gradle-j]) - Gradle plugins for the Pard Language | 0.4   | jvm
[pard-cova-annotation][cova-anno-d] ([javadoc][cova-anno-j]) - Annotations that link code to requirements | 0.2 | common, jvm
[pard-cova-gradle][cova-gradle-d] ([javadoc][cova-gradle-j]) - Gradle plugins for coverage validation | 0.2 | jvm

[model-d]: $dokka(model)
[model-j]: $javadoc(model)
[lang-gradle-d]: $dokka(lang-gradle)
[lang-gradle-j]: $javadoc(lang-gradle)
[cova-gradle-d]: $dokka(cova-gradle)
[cova-gradle-j]: $javadoc(cova-gradle)
[cova-anno-d]: $dokka(cova-annotation)
[cova-anno-j]: $javadoc(cova-annotation)

All modules are available via the [CreatorCat bintray repository](https://bintray.com/knolle/creatorcat).

## Versioning Scheme

Since the Pard modules are developed each in its own speed, they have their own specific version numbers
that follow the general [SemVer](https://semver.org/) approach in their base components (`major.minor.patch`).

However, as can be seen in the [changelog](changelog.md),
subsequent releases of all modules are bundled into one Pard main release.
The version string of these main releases is based on 100-days-stable [nuclides](https://en.wikipedia.org/wiki/Nuclide):
* The initial version was the named after the nuclide of the first element in the periodic table
  with the lowest mass number: `H-1` aka Hydrogen-1
* The version is incremented to the next nuclide of the current element (by mass number)
  which has a half-life of at least 100 days ... unstable versions should be avoided :-) :
  `H-1` --> `H-2` --> `H-3`
* If there is no next 100-days-stable nuclide for the current element,
  the version is incremented to the lowest (by mass number) 100-days-stable nuclide
  of the next element in the periodic table: `H-3` --> `He-3`

See the [table of nuclides](https://en.wikipedia.org/wiki/Table_of_nuclides) to find out which version is next.

## Developer Documentation

See the [detailed developer documentation](doc/devdoc.md).

## License

The [Apache 2 license](http://www.apache.org/licenses/LICENSE-2.0) (given in full in [LICENSE.txt](LICENSE.txt))
applies to all sources in this repository unless explicitly marked otherwise.
