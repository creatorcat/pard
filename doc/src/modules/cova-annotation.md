# Module pard-cova-annotation

This module defines annotations to link code to requirements artifacts
and annotation processors that can gather this link data.
The generated link data may then be used in requirement coverage validation, see $dokkaMd(cova-gradle).

State: *Beta*
* Annotations and processors work fluently but may be extended or adjusted anytime

Dependencies:
* CreatorCat Kotiya: `kotiya-core` and `kotiya-matchers`
* [pard-model]($dokka(model))

# Package de.creatorcat.pard.cova.annotation

Defines the annotations that link to requirement artifacts
and the respective annotation processors.
