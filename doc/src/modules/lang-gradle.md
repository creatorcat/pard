# Module pard-lang-gradle

This module provides the [Pard language Gradle plugin]($dokka(lang-gradle,lang.gradle,PardLangPlugin))
to process *PARDL* (`.pard`) files. 

State: *Alpha*
* Only a [simple model]($dokka(model,model.simple,SimpleRequirementsModel)) can be generated so far.
  Properties of *PARDL* requirements are not present in the model.

Dependencies:
* [pard-model]($dokka(model)) for the generated requirements model
* CreatorCat Kotiya: `kotiya-core`, `kotiya-matchers` and `kotiya-gradle`
* `de.creatorcat.pardl.dsl` - the *PARDL* parser based on Eclipse XText 

# Package de.creatorcat.pard.lang.gradle

Contains the plugin, the tasks and all necessary functionality.
