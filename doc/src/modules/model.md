# Module pard-model

This module is concerned with models representing:
* requirement artifacts and their structure
* source code elements
* references between model objects

Furthermore, it contains validation and analysis functionality for these models. 

It is the base of all other Pard modules.

State: *Alpha*
* Contains only a [simple model]($dokka(model,model.simple,SimpleRequirementsModel))
  that cannot represent requirement properties.
* Contains a basic model of [source code elements]($dokka(model,model.code,GenericCodeElement)).

Dependencies:
* CreatorCat Kotiya: `kotiya-core` and `kotiya-matchers`

# Package de.creatorcat.pard.model.simple

Contains a very simple requirements model definition.

# Package de.creatorcat.pard.model.code

Contains a basic model of source code elements.
