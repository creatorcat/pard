# Module pard-cova-gradle

Provides a Pard coverage validation Gradle plugin for Kotlin source code:
$dokkaMd(cova-gradle,cova.gradle,PardCovaKotlinPlugin)

The coverage validation is based on [Kotlin annotations]($dokka(cova-annotation)) that link code to a
[simple requirement model]($dokka(model,model.simple,SimpleRequirementsModel)).

State: *Alpha*
* Tasks work fluently but may be extended or adjusted anytime
* No coverage reports available, only errors and warnings on validation faults
* Only simple models supported 

Dependencies:
* [pard-model]($dokka(model)) and [pard-cova-annotation]($dokka(cova-annotation))
* CreatorCat Kotiya: `kotiya-core`, `kotiya-matchers` and `kotiya-gradle`
* `kotlin-gradle-plugin` to use the `kapt` annotation processing tools

# Package de.creatorcat.pard.cova.gradle

Contains the plugin, the tasks and all necessary functionality.
