# PARD Modules

## Structure

The project consists of the following modules, each in its own directory:

Public API:
* $dokkaMd(model)
* $dokkaMd(lang-gradle)
* $dokkaMd(cova-annotation)
* $dokkaMd(cova-gradle)

Internal:
* [cova-annotation-functest](cova-annotation-functest/module.md)

Each module directory contains:
* `doc/`: module specific documentation (optional)
* `src/`: sources and resources in the standard Gradle/Kotlin layout
* `build.gradle`: module build file - 
  applies the proper configuration (see [devdoc](../doc/devdoc.md#build-and-configuration))
* `module.md` contains general module and package documentation

The additional directory `shared-resources` holds resource files that are used by multiple modules.

Module naming convention:
* Module names are lower case alphanumeric words
* Multiple words may be separated by a hyphen ('-')
* If there is a module `libname` then there must be no separate module named `libname-subname`.
  This name pattern is reserved for special modules supporting `libname` e.g `libname-functests`.  
