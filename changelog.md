# PARD Release Notes

For an explanation of the versioning scheme see [the main readme](readme.md#versioning-scheme).

## H-3 - Annotation Comment and Artifact Filter 

Git-Tag: [H-3](https://gitlab.com/creatorcat/pard/tags/H-3)

### pard-cova-annotation 0.2

* Introduced comment property in cova annotations
* Revised link data handling to fully support link properties (like the new comment)

### pard-cova-gradle 0.2

* Revised link data handling to fully support link properties (like the new comment)
* Introduced [compilationTarget](https://creatorcat.gitlab.io/pard/docs/cova-gradle/html/pard-cova-gradle/de.creatorcat.pard.cova.gradle/-kapt-based-cova-anno-gathering-task/compilation-target.html)
  property to fix MPP support of `KaptBasedCovaAnnoGatheringTask`
* Added artifact filter support to [PardValidateForwardCoverageTask](https://creatorcat.gitlab.io/pard/docs/cova-gradle/html/pard-cova-gradle/de.creatorcat.pard.cova.gradle/-pard-validate-forward-coverage-task)

### pard-model 0.3

* Revised link data handling to fully support link properties (like the new comment)
* Added artifact filter support to [SimpleModelForwardCoverageValidator](https://creatorcat.gitlab.io/pard/docs/model/html/pard-model/de.creatorcat.pard.model.simple/-simple-model-forward-coverage-validator)

### pard-lang-gradle 0.4

*internal changes only*

### Dependency Updates

*none*

### For Developers

* Improved link data serialization: use JSON format

## H-2 - Forward Coverage Validation based on Kotlin Annotations 

Git-Tag: [H-2](https://gitlab.com/creatorcat/pard/tags/H-2)

### pard-cova-annotation 0.1

Introduction of annotations that link code to requirements artifacts
and respective annotation processors to gather the link data (see [cova-annotation](https://creatorcat.gitlab.io/pard/docs/cova-annotation/html/pard-cova-annotation)). 

### pard-cova-gradle 0.1

Introduction of the [Pard-Cova Kotlin Plugin](doc/gradleUserguid#pard-cova-kotlin-plugin)
that provides validation of forward requirement coverage based on Kotlin annotations.

### pard-model 0.2

Code-to-artifact link handling:
* Generic code element representation: [GenericCodeElement](https://creatorcat.gitlab.io/pard/docs/model/html/pard-model/de.creatorcat.pard.model.code/-generic-code-element)
* [CodeToArtifactLinkData](https://creatorcat.gitlab.io/pard/docs/model/html/pard-model/de.creatorcat.pard.model.code/-code-to-artifact-link-data.html)
  and a respective [serializer](https://creatorcat.gitlab.io/pard/docs/model/html/pard-model/de.creatorcat.pard.model.code/-code-to-artifact-link-data-serializer)
* A [forward coverage validator](https://creatorcat.gitlab.io/pard/docs/model/html/pard-model/de.creatorcat.pard.model.simple/-simple-model-forward-coverage-validator) for simple models

### pard-lang-gradle 0.3

Minor improvements:
* Documentation updates
* Better task dependency handling
* Tasks now have proper groups and descriptions

### Dependency Updates

* Kotiya H-3 -> He-3

### For Developers

Improved $-token dokka link generation for markdown documentation:
* url-style is not necessary anymore for classes, functions and values (no need to escape capital letters anymore)  
* new macro `$dokkaMd` generates a full link: `[lastArg](dokka-url)`
* improved TODO-Tag removal

Build script improvements:
* `base`-plugin is now already applied in root project so clean works there too
* automatic publishing configuration for gradle plugin projects is now **disabled** because it interferes with
  our own publishing
* new method for module-projects: `excludeTestResource(path)` to exclude generated test resources from IDEA and Gradle 

## H-1 - Pard language plugin and simple model

Git-Tag: [H-1](https://gitlab.com/creatorcat/pard/tags/H-1)

First public availability.
Contains the following modules with separate versions:

### pard-model 0.1

Definition of a [simple requirements model](https://creatorcat.gitlab.io/pard/docs/model/html/pard-model/de.creatorcat.pard.model.simple/-simple-requirements-model).

### pard-lang-gradle 0.2

Initial Gradle plugin to [parse](https://creatorcat.gitlab.io/pard/docs/lang-gradle/html/pard-lang-gradle/de.creatorcat.pard.lang.gradle/-pard-parse-task) `.pard` files
and generate a simple model.
