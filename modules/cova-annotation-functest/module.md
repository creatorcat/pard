# Module pard-cova-annotation-functest

Functional tests for [pard-cova-annotation](https://creatorcat.gitlab.io/pard/docs/pard-cova-annotation/html/pard-pard-cova-annotation).

# Package de.creatorcat.pard.cova.annotation.functest

Contains the functional tests and necessary supporting code.
