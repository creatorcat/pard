/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

// through the file the annotation is used on all possible targets
@file:VerifiesArtifact("FILE")

package de.creatorcat.pard.cova.annotation.functest

import de.creatorcat.kotiya.core.reflect.otherProperty
import de.creatorcat.pard.cova.annotation.VerifiesArtifact
import kotlin.test.Test
import kotlin.test.assertEquals

/**
 * A dummy class for testing pard-cova annotations.
 */
class AnnotatedDummyTest @VerifiesArtifact("CONSTRUCTOR") constructor() : AnnotatedDummy() {

    @VerifiesArtifact("CLASS 1", "CLASS 2")
    internal class DummyExt : Dummy() {
        @field:VerifiesArtifact("FIELD")
        @property:VerifiesArtifact("PROPERTY")
        val pX = p

        @get:VerifiesArtifact("PROPERTY_GETTER")
        @set:VerifiesArtifact("PROPERTY_SETTER")
        var vX by otherProperty(::v)

        @VerifiesArtifact("FUNCTION", comment = "function comment")
        fun bar(@VerifiesArtifact("VALUE_PARAMETER") arg: Int) = foo(arg)
    }

    @Test fun useAll() {
        with(DummyExt()) {
            assertEquals(p, pX)
            assertEquals(v, vX)
            assertEquals(foo(17), bar(17))
            assertEquals(DUMMY_CONST, DUMMY_CONST_EXT)
        }
    }
}

/** this will trigger the file annotation to be processed */
const val DUMMY_CONST_EXT = DUMMY_CONST
