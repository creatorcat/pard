/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.pard.cova.annotation.functest

import de.creatorcat.kotiya.core.text.sloshToSlash
import de.creatorcat.kotiya.test.core.io.assertRecursiveDeletionIfExists
import de.creatorcat.kotiya.test.gradle.FunctionalGradleTestBase
import de.creatorcat.pard.cova.annotation.DataDirOptionCovaAnnoProc
import de.creatorcat.pard.model.code.CodeToArtifactLink
import de.creatorcat.pard.model.code.CodeToArtifactLinkDataSerializer
import java.io.File
import java.io.FileReader
import java.nio.file.Files
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class ProcessAnnotations : FunctionalGradleTestBase() {

    @Test fun `process annotated dummy classes`() {
        initSources()

        versionedPlugins["org.jetbrains.kotlin.jvm"] = KotlinVersion.CURRENT.toString()
        versionedPlugins["org.jetbrains.kotlin.kapt"] = KotlinVersion.CURRENT.toString()

        writeBuildScript(
            """
            repositories {
                // local temp repo to retrieve most current pard-artifacts
                maven {
                    name = "temp"
                    url = "$tempRepoDir"
                }
                maven {
                    name = "creatorCatBintray"
                    url = "https://dl.bintray.com/knolle/creatorcat"
                }
                jcenter()
            }


            dependencies {
                implementation "org.jetbrains.kotlin:kotlin-stdlib-jdk8:${KotlinVersion.CURRENT}"
                implementation "org.jetbrains.kotlin:kotlin-test:${KotlinVersion.CURRENT}"
                implementation "org.jetbrains.kotlin:kotlin-test-junit5:${KotlinVersion.CURRENT}"

                // to use annotations in code
                compileOnly "de.creatorcat.pard:pard-cova-annotation-jvm:+"
                testCompileOnly "de.creatorcat.pard:pard-cova-annotation-jvm:+"

                // to use anno processor
                kapt "de.creatorcat.pard:pard-cova-annotation-jvm:+"
                kaptTest "de.creatorcat.pard:pard-cova-annotation-jvm:+"
            }

            kapt {
                includeCompileClasspath = false
                arguments {
                    arg("pard.cova.linkDataDir", "$annoDataDir")
                }
                annotationProcessor("${DataDirOptionCovaAnnoProc::class.qualifiedName}")
            }

            ${printAssertableValue("kapt.processors")}
            """.trimIndent()
        )
        runAndAssertTask("kaptKotlin", "--stacktrace", "clean").assertOutputValues(
            "kapt.processors" to DataDirOptionCovaAnnoProc::class.qualifiedName
        )
        runAndAssertTask("kaptTestKotlin").assertOutputValues(
            "kapt.processors" to DataDirOptionCovaAnnoProc::class.qualifiedName
        )
        assertEquals(
            readLinkData(expectedImplementationLinkFile),
            readLinkData(File(annoDataDir, DataDirOptionCovaAnnoProc.DEFAULT_IMPLEMENTATION_LINK_FILE_NAME))
        )
        assertEquals(
            readLinkData(expectedVerificationLinkFile),
            readLinkData(File(annoDataDir, DataDirOptionCovaAnnoProc.DEFAULT_VERIFICATION_LINK_FILE_NAME))
        )
    }

    private val linkDataReader = CodeToArtifactLinkDataSerializer()
    private fun readLinkData(file: File): List<CodeToArtifactLink> {
        FileReader(file).use {
            return linkDataReader.read(it.readText())
        }
    }

    private val srcPackagePath = "kotlin/de/creatorcat/pard/cova/annotation/functest"

    private val realSrcFile = File("src/main/$srcPackagePath/AnnotatedDummy.kt")
    private val realTestFile = File("src/test/$srcPackagePath/AnnotatedDummyTest.kt")
    private val functestSrcDir = getProjectFile("src/main/$srcPackagePath")
    private val functestTestDir = getProjectFile("src/test/$srcPackagePath")
    private val annoDataDir = getBuildFile("pard/cova/linkData").absolutePath.sloshToSlash()
    private val tempRepoDir = File("../../build/temp-repo").absolutePath.sloshToSlash()

    private val expectedImplementationLinkFile =
        getProjectFile("expected/linkData/${DataDirOptionCovaAnnoProc.DEFAULT_IMPLEMENTATION_LINK_FILE_NAME}")
    private val expectedVerificationLinkFile =
        getProjectFile("expected/linkData/${DataDirOptionCovaAnnoProc.DEFAULT_VERIFICATION_LINK_FILE_NAME}")

    private fun initSources() {
        for (dir in listOf(functestSrcDir, functestTestDir)) {
            assertRecursiveDeletionIfExists(dir)
            assertTrue(dir.mkdirs())
        }
        Files.copy(realSrcFile.toPath(), functestSrcDir.toPath().resolve(realSrcFile.name))
        Files.copy(realTestFile.toPath(), functestTestDir.toPath().resolve(realTestFile.name))
    }
}
