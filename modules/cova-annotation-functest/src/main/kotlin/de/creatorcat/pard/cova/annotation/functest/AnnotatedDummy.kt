/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

// through the file the annotation is used on all possible targets
@file:ImplementsArtifact("FILE")

package de.creatorcat.pard.cova.annotation.functest

import de.creatorcat.pard.cova.annotation.ImplementsArtifact

/**
 * A dummy class for testing pard-cova annotations.
 */
open class AnnotatedDummy @ImplementsArtifact("CONSTRUCTOR") constructor() {

    @ImplementsArtifact("CLASS 1", "CLASS 2", comment = "class comment")
    internal open class Dummy {
        @field:ImplementsArtifact("FIELD")
        @ImplementsArtifact("PROPERTY")
        val p = 17

        var v = 0
            @ImplementsArtifact("PROPERTY_GETTER")
            get() = field
            @ImplementsArtifact("PROPERTY_SETTER")
            set(value) {
                field = value
            }

        @ImplementsArtifact("FUNCTION")
        fun foo(@ImplementsArtifact("VALUE_PARAMETER") arg: Int) = arg.toString()
    }
}

/** this will trigger the file annotation to be processed */
const val DUMMY_CONST: String = "DUMMY"
