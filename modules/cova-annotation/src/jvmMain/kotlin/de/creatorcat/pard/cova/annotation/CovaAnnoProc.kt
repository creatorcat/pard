/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.pard.cova.annotation

import de.creatorcat.kotiya.core.text.orEmptyIf
import de.creatorcat.pard.cova.annotation.CovaAnnoProc.Companion.supportedAnnotations
import de.creatorcat.pard.model.code.CodeToArtifactLink
import de.creatorcat.pard.model.code.SimpleCodeElement
import de.creatorcat.pard.model.code.StandardCodeToArtifactLink
import javax.annotation.processing.AbstractProcessor
import javax.annotation.processing.ProcessingEnvironment
import javax.annotation.processing.Processor
import javax.annotation.processing.RoundEnvironment
import javax.lang.model.SourceVersion
import javax.lang.model.SourceVersion.RELEASE_9
import javax.lang.model.element.Element
import javax.lang.model.element.ElementKind.PACKAGE
import javax.lang.model.element.TypeElement
import javax.tools.Diagnostic.Kind.NOTE
import kotlin.reflect.KClass

/**
 * Base implementation of an annotation processor for the pard cova code-to-artifact link annotations.
 *
 * It implements [process] to gather all elements annotated with one of the [supportedAnnotations]
 * and generates a [CodeToArtifactLink] list as result of each processing round.
 * Sub classes only need to implement [processLinkDataFor] to further process the gathered data.
 */
abstract class CovaAnnoProc : AbstractProcessor() {

    /**
     * Is called for each processing round for each processed and supported [annoClass]
     * with [linkData] being the code-to-artifact data that was gathered in this round.
     *
     * Sub classes must implement to further process the data.
     */
    protected abstract fun <A : Annotation> processLinkDataFor(
        annoClass: KClass<A>, linkData: List<CodeToArtifactLink>
    )

    /**
     * Returns a set of the [qualified names][KClass.qualifiedName] of the [supportedAnnotations].
     */
    final override fun getSupportedAnnotationTypes(): MutableSet<String> =
        supportedAnnotations.map {
            it.qualifiedName ?: throw AssertionError("no qualified name for $it")
        }.toMutableSet()

    /** See [Processor.getSupportedSourceVersion]. */
    final override fun getSupportedSourceVersion(): SourceVersion = RELEASE_9

    /**
     * Fixes the `init` implementation to first call [AbstractProcessor.init]
     * and afterwards the overridable method [initialize].
     * This assures that the [processingEnv] initialization is always performed first.
     * Sub classes may override [initialize] to inject further logic.
     */
    final override fun init(processingEnv: ProcessingEnvironment) {
        super.init(processingEnv)
        logInfo("initializing...")
        initialize()
    }

    /**
     * Performs type specific initialization after the general [init][AbstractProcessor.init] was called.
     *
     * The default method does nothing, sub classes may override.
     */
    protected open fun initialize() {}

    /**
     * For all [supportedAnnotations] in [annotations] all elements with such an annotation are gathered
     * and a [CodeToArtifactLink] list is generated.
     * Then [processLinkDataFor] is called to further process the gathered data.
     *
     * @return `true` iff all [annotations] are [supportedAnnotations]
     * @see Processor.process
     */
    final override fun process(annotations: MutableSet<out TypeElement>, roundEnv: RoundEnvironment): Boolean {
        logInfo("processing...")
        var allClaimed = true
        for (annoType in annotations) {
            when {
                annoType.qualifiedName.contentEquals(ImplementsArtifact::class.qualifiedName) -> {
                    roundEnv.getElementsAnnotatedWith(annoType)
                        .process(ImplementsArtifact::class, ImplementsArtifact::ids, ImplementsArtifact::comment)
                }
                annoType.qualifiedName.contentEquals(VerifiesArtifact::class.qualifiedName) -> {
                    roundEnv.getElementsAnnotatedWith(annoType)
                        .process(VerifiesArtifact::class, VerifiesArtifact::ids, VerifiesArtifact::comment)
                }
                else -> allClaimed = false
            }
        }
        return allClaimed
    }

    internal fun <A : Annotation> Set<Element>.process(
        annoClass: KClass<A>, idGetter: A.() -> Array<out String>, commentGetter: A.() -> String
    ) {
        logInfo("annotations of type: $annoClass")
        val linkData = mutableListOf<CodeToArtifactLink>()
        for (element in this) {
            for (anno in element.getAnnotationsByType(annoClass.java)) {
                val ids = anno.idGetter()
                val comment = anno.commentGetter()
                val codeElement = SimpleCodeElement(
                    element.kind.toString(),
                    element.simpleName.toString(),
                    element.getScope()
                )
                for (id in ids)
                    linkData.add(StandardCodeToArtifactLink(id, codeElement, comment))

                logInfo("anno with ids=$ids on $element")
            }
        }
        processLinkDataFor(annoClass, linkData)
    }

    internal fun Element.getScope(): String =
        if (this.kind == PACKAGE)
            ""
        else {
            val parentScope = this.enclosingElement.getScope()
            "${parentScope}${".".orEmptyIf(parentScope.isBlank())}${this.enclosingElement.simpleName}"
        }

    /**
     * Prints a log message via the [processingEnv.messager][ProcessingEnvironment.getMessager]
     * starting with some *pard-cova* tag.
     */
    protected fun logInfo(msg: String) {
        processingEnv.messager.printMessage(NOTE, "PardCova-AP: $msg")
    }

    companion object {
        /**
         * Annotation types supported by processors of this type.
         */
        val supportedAnnotations: Set<KClass<out Annotation>> =
            setOf(ImplementsArtifact::class, VerifiesArtifact::class)
    }
}
