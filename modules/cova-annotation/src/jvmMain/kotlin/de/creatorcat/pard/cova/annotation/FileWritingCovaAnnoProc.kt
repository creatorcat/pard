/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.pard.cova.annotation

import de.creatorcat.pard.model.code.CodeToArtifactLink
import de.creatorcat.pard.model.code.CodeToArtifactLinkDataSerializer
import java.io.File
import java.io.FileWriter
import java.nio.file.Files
import kotlin.reflect.KClass

/**
 * Extension of the [CovaAnnoProc] that implements [processLinkDataFor] to write the gathered link data
 * into an [annotation specific file][getLinkDataFileFor].
 */
abstract class FileWritingCovaAnnoProc : CovaAnnoProc() {

    /**
     * Get a file to store the generated [CodeToArtifactLink] list for the given [annoClass].
     *
     * The file will be used in [processLinkDataFor] to write the generated data.
     * Implementing sub classes must ensure, that the parent directory of the file exists before the processing starts.
     */
    protected abstract fun <A : Annotation> getLinkDataFileFor(annoClass: KClass<A>): File

    private val linkDataFiles: MutableMap<KClass<out Annotation>, File> = mutableMapOf()

    /**
     * Initializes the [link data file][getLinkDataFileFor] for the given [annoClass].
     * This is called by [processLinkDataFor] only for annotation classes that actually get processed.
     *
     * The default implementation will delete any existing file because
     * the data of the last processor run is considered obsolete.
     *
     * @return the initialized file
     * @throws IllegalArgumentException if the [annoClass] is not a inside [CovaAnnoProc.supportedAnnotations]
     */
    protected open fun initLinkDataFile(annoClass: KClass<out Annotation>): File {
        if (!supportedAnnotations.contains(annoClass))
            throw IllegalArgumentException("unsupported annotation class: $annoClass")

        val file = getLinkDataFileFor(annoClass)
        Files.deleteIfExists(file.toPath())
        logInfo("using link data file for @${annoClass.simpleName}: $file")
        return file
    }

    /**
     * Appends the current [linkData] to the respective [link data file][getLinkDataFileFor]
     * for the current [annoClass].
     * On first occurrence of the annotation class, the file will be initialized using [initLinkDataFile].
     *
     * Link data is written as text using a [CodeToArtifactLinkDataSerializer]
     * which should also be used to read the data.
     */
    override fun <A : Annotation> processLinkDataFor(annoClass: KClass<A>, linkData: List<CodeToArtifactLink>) {
        val file = linkDataFiles.getOrPut(annoClass) { initLinkDataFile(annoClass) }
        FileWriter(file, true).use {
            CodeToArtifactLinkDataSerializer().write(it, linkData)
        }
    }
}
