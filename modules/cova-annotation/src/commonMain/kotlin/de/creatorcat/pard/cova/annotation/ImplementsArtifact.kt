/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.pard.cova.annotation

import kotlin.annotation.AnnotationRetention.SOURCE
import kotlin.annotation.AnnotationTarget.CLASS
import kotlin.annotation.AnnotationTarget.CONSTRUCTOR
import kotlin.annotation.AnnotationTarget.FIELD
import kotlin.annotation.AnnotationTarget.FILE
import kotlin.annotation.AnnotationTarget.FUNCTION
import kotlin.annotation.AnnotationTarget.PROPERTY
import kotlin.annotation.AnnotationTarget.PROPERTY_GETTER
import kotlin.annotation.AnnotationTarget.PROPERTY_SETTER
import kotlin.annotation.AnnotationTarget.VALUE_PARAMETER

/**
 * Annotation that implies that the annotated element is part of the implementation of
 * the artifacts specified by [ids].
 *
 * @param ids identifiers of the artifacts
 * @param comment optional comment, defaults to the empty string
 */
// TODO design info
// other targets are not supported by the annotation processor
@Target(
    FILE, CLASS, FUNCTION, CONSTRUCTOR,
    PROPERTY, PROPERTY_GETTER, PROPERTY_SETTER, FIELD, VALUE_PARAMETER
)
@Retention(SOURCE)
// TODO @Repeatable is not yet supported to be retrieved by the processor
@MustBeDocumented
annotation class ImplementsArtifact(vararg val ids: String, val comment: String = "")
