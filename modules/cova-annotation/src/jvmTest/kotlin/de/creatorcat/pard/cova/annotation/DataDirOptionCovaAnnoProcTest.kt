/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.pard.cova.annotation

import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import de.creatorcat.kotiya.matchers.common.exists
import de.creatorcat.kotiya.matchers.common.hasElementsEqual
import de.creatorcat.kotiya.matchers.not
import de.creatorcat.kotiya.test.core.assertThat
import de.creatorcat.kotiya.test.core.assertThrows
import de.creatorcat.kotiya.test.core.io.FileCreatingTestBase
import java.io.File
import javax.annotation.processing.Messager
import kotlin.test.Test
import kotlin.test.assertEquals

class DataDirOptionCovaAnnoProcTest : DataDirOptionCovaAnnoProc(), FileCreatingTestBase {

    private val messager: Messager = mock()
    private val linkDataDirFromOption = testOutFile("link/data/dir")

    init {
        processingEnv = mock {
            on { messager } doReturn messager
            on { options } doReturn mutableMapOf(
                DataDirOptionCovaAnnoProc.ARGUMENT_LINK_DATA_DIR to linkDataDirFromOption.absolutePath
            )
        }
    }

    @Test fun `getSupportedOptions - contents`() {
        assertThat(supportedOptions, hasElementsEqual(DataDirOptionCovaAnnoProc.ARGUMENT_LINK_DATA_DIR))
    }

    @Test fun `initialize - creates link data dir from option`() {
        assertThat(linkDataDirFromOption, !exists)
        initialize()
        assertThat(linkDataDirFromOption, exists)
        assertEquals(mainLinkDataDir.canonicalFile, linkDataDirFromOption.canonicalFile)
    }

    @Test fun `initialize - fails on missing option`() {
        processingEnv = mock {
            on { messager } doReturn messager
            on { options } doReturn mutableMapOf()
        }
        assertThrows<IllegalArgumentException> { initialize() }
    }

    @Test fun `getLinkDataFileNameFor - results`() {
        assertEquals(
            DataDirOptionCovaAnnoProc.DEFAULT_IMPLEMENTATION_LINK_FILE_NAME,
            getLinkDataFileNameFor(ImplementsArtifact::class)
        )
        assertEquals(
            DataDirOptionCovaAnnoProc.DEFAULT_VERIFICATION_LINK_FILE_NAME,
            getLinkDataFileNameFor(VerifiesArtifact::class)
        )
        assertThrows<IllegalArgumentException> { getLinkDataFileNameFor(Suppress::class) }
    }

    @Test fun `getLinkDataFileFor - uses link data dir and name`() {
        initialize()
        assertEquals(
            File(linkDataDirFromOption, getLinkDataFileNameFor(ImplementsArtifact::class)).canonicalFile,
            getLinkDataFileFor(ImplementsArtifact::class).canonicalFile
        )
    }
}
