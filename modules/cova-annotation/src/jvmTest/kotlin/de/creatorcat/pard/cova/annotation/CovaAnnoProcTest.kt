/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.pard.cova.annotation

import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import de.creatorcat.kotiya.matchers.common.hasElementsEqual
import de.creatorcat.kotiya.test.core.assertThat
import de.creatorcat.kotiya.test.core.stubbing.countedCalls
import de.creatorcat.kotiya.test.core.stubbing.observedArguments
import de.creatorcat.kotiya.test.core.stubbing.withCallObserver
import de.creatorcat.pard.model.code.CodeToArtifactLink
import de.creatorcat.pard.model.code.SimpleCodeElement
import de.creatorcat.pard.model.code.StandardCodeToArtifactLink
import javax.annotation.processing.Messager
import javax.annotation.processing.ProcessingEnvironment
import javax.annotation.processing.RoundEnvironment
import javax.lang.model.SourceVersion
import javax.lang.model.element.Element
import javax.lang.model.element.ElementKind
import javax.lang.model.element.ElementKind.CLASS
import javax.lang.model.element.ElementKind.METHOD
import javax.lang.model.element.ElementKind.PACKAGE
import javax.lang.model.element.ElementKind.PARAMETER
import javax.lang.model.element.Name
import javax.lang.model.element.TypeElement
import javax.tools.Diagnostic.Kind.NOTE
import kotlin.reflect.KClass
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertNotNull
import kotlin.test.assertSame
import kotlin.test.assertTrue

class CovaAnnoProcTest : CovaAnnoProc() {

    private val messager: Messager = mock()

    init {
        processingEnv = mock {
            on { messager } doReturn messager
        }
    }

    @Test fun `supported annotations`() {
        assertThat(
            supportedAnnotationTypes,
            hasElementsEqual(ImplementsArtifact::class.qualifiedName, VerifiesArtifact::class.qualifiedName)
        )
    }

    @Test fun `supported source version`() {
        assertEquals(SourceVersion.RELEASE_9, supportedSourceVersion)
    }

    @Test fun `element scope`() {
        assertEquals(
            "package.class.func",
            element(
                PARAMETER, "arg", element(
                    METHOD, "func", element(
                        CLASS, "class", element(
                            PACKAGE, "package"
                        )
                    )
                )
            ).getScope()
        )
    }

    @Test fun `logInfo - uses messager`() {
        logInfo("MSG")
        verify(messager).printMessage(NOTE, "PardCova-AP: MSG")
    }

    @Test fun `process element set - code link map creation`() {
        val pkg = element(PACKAGE, "package")
        val elements = setOf(
            element(METHOD, "m1", pkg, implementsIds = arrayOf("a1", "a2")),
            element(CLASS, "c1", pkg, implementsIds = arrayOf("a1"), implementsComment = "a1 c1"),
            element(METHOD, "m2", pkg, implementsIds = arrayOf("a2")),
            element(METHOD, "m3", pkg, verifiesIds = arrayOf("a1"), verifiesComment = "a1 m3")
        )
        val expectedImplementationLinks = listOf(
            linkOf("a1", METHOD.toString(), "m1", "package"),
            linkOf("a2", METHOD.toString(), "m1", "package"),
            linkOf("a1", CLASS.toString(), "c1", "package", "a1 c1"),
            linkOf("a2", METHOD.toString(), "m2", "package")
        )
        val expectedVerificationLinks = listOf(
            linkOf("a1", METHOD.toString(), "m3", "package", "a1 m3")
        )

        elements.process(ImplementsArtifact::class, ImplementsArtifact::ids, ImplementsArtifact::comment)
        elements.process(VerifiesArtifact::class, VerifiesArtifact::ids, VerifiesArtifact::comment)

        val processedArgs = ::processDummyFunc.observedArguments
        assertEquals(ImplementsArtifact::class, processedArgs[0][0])
        assertEquals(expectedImplementationLinks, processedArgs[0][1])
        assertEquals(VerifiesArtifact::class, processedArgs[1][0])
        assertEquals(expectedVerificationLinks, processedArgs[1][1])
    }

    @Test fun `process round - functionality`() {
        val pkg = element(PACKAGE, "package")
        val implementingElements = setOf(
            element(METHOD, "m1", pkg, implementsIds = arrayOf("a1")),
            element(CLASS, "c1", pkg, implementsIds = arrayOf("a1"), implementsComment = "a1 c1"),
            element(METHOD, "m2", pkg, implementsIds = arrayOf("a2"))
        )
        val verifyingElements = setOf(
            element(CLASS, "c2", pkg, verifiesIds = arrayOf("a1")),
            element(METHOD, "m3", pkg, verifiesIds = arrayOf("a2"), verifiesComment = "a2 m3")
        )
        val roundEnv = roundEnv(implementingElements, verifyingElements)

        val expectedImplementationLinks = listOf(
            linkOf("a1", METHOD.toString(), "m1", "package"),
            linkOf("a1", CLASS.toString(), "c1", "package", "a1 c1"),
            linkOf("a2", METHOD.toString(), "m2", "package")
        )
        val expectedVerificationLinks = listOf(
            linkOf("a1", CLASS.toString(), "c2", "package"),
            linkOf("a2", METHOD.toString(), "m3", "package", "a2 m3")
        )

        val annoSet = mutableSetOf(implementsAnnoType, verifiesAnnoType)

        assertTrue(process(annoSet, roundEnv))

        val processedArgs = ::processDummyFunc.observedArguments
        assertEquals(ImplementsArtifact::class, processedArgs[0][0])
        assertEquals(expectedImplementationLinks, processedArgs[0][1])
        assertEquals(VerifiesArtifact::class, processedArgs[1][0])
        assertEquals(expectedVerificationLinks, processedArgs[1][1])
    }

    @Test fun `init - sets processEnv and calls initialize`() {
        val specialProcEnv = mock<ProcessingEnvironment> {
            on { messager } doReturn messager
        }
        init(specialProcEnv)
        assertSame(specialProcEnv, processingEnv)
        assertEquals(1, ::initialize.countedCalls)
    }

    // TODO workaround... kotiya should provide proper tag-variant of this function
    private fun processDummyFunc() {}

    override fun <A : Annotation> processLinkDataFor(annoClass: KClass<A>, linkData: List<CodeToArtifactLink>) =
        withCallObserver(::processDummyFunc, annoClass, linkData) {}

    override fun initialize(): Unit =
        withCallObserver(::initialize) {}

    private fun element(
        kind: ElementKind, name: String, parent: Element? = null,
        implementsIds: Array<String> = emptyArray(), verifiesIds: Array<String> = emptyArray(),
        implementsComment: String = "", verifiesComment: String = ""
    ): Element {
        val implementsAnnoMock = arrayOf(
            mock<ImplementsArtifact> {
                on { ids } doReturn implementsIds
                on { comment } doReturn implementsComment
            })

        val verifiesAnnoMock = arrayOf(
            mock<VerifiesArtifact> {
                on { ids } doReturn verifiesIds
                on { comment } doReturn verifiesComment
            })

        val nameMock = name(name)

        return mock {
            on { getAnnotationsByType(ImplementsArtifact::class.java) } doReturn implementsAnnoMock
            on { getAnnotationsByType(VerifiesArtifact::class.java) } doReturn verifiesAnnoMock
            on { enclosingElement } doReturn parent
            on { this.kind } doReturn kind
            on { simpleName } doReturn nameMock
        }
    }

    private fun roundEnv(
        implementingElements: Set<Element>, verifyingElements: Set<Element>
    ): RoundEnvironment =
        mock {
            on { getElementsAnnotatedWith(implementsAnnoType) } doReturn implementingElements
            on { getElementsAnnotatedWith(verifiesAnnoType) } doReturn verifyingElements
        }

    private fun name(value: String): Name =
        mock {
            on { toString() } doReturn value
            on { contentEquals(value) } doReturn true
        }

    private fun linkOf(
        artifactId: String,
        codeType: String, codeName: String, codeScope: String,
        comment: String = ""
    ): CodeToArtifactLink =
        StandardCodeToArtifactLink(
            artifactId,
            SimpleCodeElement(codeType, codeName, codeScope),
            comment
        )

    private val implementsAnnoType = annoElement(assertNotNull(ImplementsArtifact::class.qualifiedName))
    private val verifiesAnnoType = annoElement(assertNotNull(VerifiesArtifact::class.qualifiedName))

    private fun annoElement(name: String): TypeElement {
        val nameMock = name(name)
        return mock {
            on { qualifiedName } doReturn nameMock
        }
    }

    @Test fun `internal test - mock name contentEquals`() {
        val name = name("name")
        assertTrue(name.contentEquals("name"))
        assertFalse(name.contentEquals("other"))
    }
}
