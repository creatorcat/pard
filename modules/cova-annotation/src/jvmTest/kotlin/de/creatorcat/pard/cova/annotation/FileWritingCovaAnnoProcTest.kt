/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.pard.cova.annotation

import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import de.creatorcat.kotiya.matchers.common.exists
import de.creatorcat.kotiya.matchers.common.isEmpty
import de.creatorcat.kotiya.matchers.not
import de.creatorcat.kotiya.test.core.assertThat
import de.creatorcat.kotiya.test.core.assertThrows
import de.creatorcat.kotiya.test.core.io.FileCreatingTestBase
import de.creatorcat.pard.model.code.CodeToArtifactLink
import de.creatorcat.pard.model.code.CodeToArtifactLinkDataSerializer
import de.creatorcat.pard.model.code.SimpleCodeElement
import de.creatorcat.pard.model.code.StandardCodeToArtifactLink
import java.io.File
import java.io.FileReader
import javax.annotation.processing.Messager
import javax.lang.model.element.ElementKind.CLASS
import javax.lang.model.element.ElementKind.METHOD
import kotlin.reflect.KClass
import kotlin.test.Test
import kotlin.test.assertEquals

class FileWritingCovaAnnoProcTest : FileWritingCovaAnnoProc(), FileCreatingTestBase {

    private val messager: Messager = mock()

    init {
        processingEnv = mock {
            on { messager } doReturn messager
        }
    }

    override fun <A : Annotation> getLinkDataFileFor(annoClass: KClass<A>): File =
        testOutFile(annoClass.simpleName ?: throw AssertionError("annotation has no name"))

    @Test fun `initLinkDataFile - removes existing files`() {
        val file = getLinkDataFileFor(VerifiesArtifact::class)
        file.writeText("I exist!")
        assertThat(file, exists)
        initLinkDataFile(VerifiesArtifact::class)
        // wait a moment to let the system remove the file ... based on bad experience :-)
        Thread.sleep(100)
        assertThat(file, !exists)
    }

    @Test fun `initLinkDataFile - throws on unsupported annotation`() {
        assertThrows<IllegalArgumentException> { initLinkDataFile(Suppress::class) }
    }

    @Test fun `processLinkData - writes file`() {
        val linksToProcess = listOf(
            linkOf("a1", METHOD.toString(), "m1", "package"),
            linkOf("a1", CLASS.toString(), "c1", "package", "comment a1 c1"),
            linkOf("a2", METHOD.toString(), "m1", "package"),
            linkOf("a2", METHOD.toString(), "m2", "package", "comment a2 m2")
        )
        processLinkDataFor(ImplementsArtifact::class, linksToProcess)

        val actualLinks = FileReader(getLinkDataFileFor(ImplementsArtifact::class)).use {
            CodeToArtifactLinkDataSerializer().read(it.readText())
        }

        assertEquals(linksToProcess, actualLinks)
    }

    @Test fun `processLinkData - initializes on first occurrence`() {
        val verifiesFile = getLinkDataFileFor(VerifiesArtifact::class)
        verifiesFile.writeText("I exist!")
        assertThat(verifiesFile, exists)
        val implementsFile = getLinkDataFileFor(ImplementsArtifact::class)
        implementsFile.writeText("I exist!")
        assertThat(implementsFile, exists)
        // wait a moment to let the system handle IO ops ... based on bad experience :-)
        Thread.sleep(100)

        processLinkDataFor(ImplementsArtifact::class, listOf())
        assertEquals("I exist!", verifiesFile.readText())
        assertThat(implementsFile.readText(), isEmpty)
    }

    private fun linkOf(
        artifactId: String,
        codeType: String, codeName: String, codeScope: String,
        comment: String = ""
    ): CodeToArtifactLink =
        StandardCodeToArtifactLink(
            artifactId,
            SimpleCodeElement(codeType, codeName, codeScope),
            comment
        )
}
