/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

// through the file the annotation is used on all possible targets
@file:VerifiesArtifact("FILE")

package de.creatorcat.pard.cova.annotation

import de.creatorcat.kotiya.matchers.common.isEmpty
import de.creatorcat.kotiya.test.core.assertThat
import kotlin.test.Test

class VerifiesArtifactTest @VerifiesArtifact("CONSTRUCTOR") constructor() {

    @Suppress("unused")
    @VerifiesArtifact("CLASS")
    private class Dummy {
        @field:VerifiesArtifact("FIELD", "FIELD AGAIN")
        @VerifiesArtifact("PROPERTY", comment = "Say something...")
        val p = 17

        var v = 0
            @VerifiesArtifact("PROPERTY_GETTER")
            get() = field
            @VerifiesArtifact("PROPERTY_SETTER")
            set(value) {
                field = value
            }

        @VerifiesArtifact("FUNCTION")
        fun foo(@VerifiesArtifact("VALUE_PARAMETER") arg: Int) = arg.toString()
    }

    @Test fun `annotations are not present at runtime`() {
        assertThat(Dummy::class.annotations, isEmpty)
    }
}
