/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

// through the file the annotation is used on all possible targets
@file:ImplementsArtifact("FILE")

package de.creatorcat.pard.cova.annotation

import de.creatorcat.kotiya.matchers.common.isEmpty
import de.creatorcat.kotiya.test.core.assertThat
import kotlin.test.Test

class ImplementsArtifactTest @ImplementsArtifact("CONSTRUCTOR") constructor() {

    @Suppress("unused")
    @ImplementsArtifact("CLASS", "CLASS AGAIN")
    private class Dummy {
        @field:ImplementsArtifact("FIELD", "otherId", comment = "Why is this annotated?")
        @ImplementsArtifact("PROPERTY")
        val p = 17

        var v = 0
            @ImplementsArtifact("PROPERTY_GETTER")
            get() = field
            @ImplementsArtifact("PROPERTY_SETTER")
            set(value) {
                field = value
            }

        @ImplementsArtifact("FUNCTION")
        fun foo(@ImplementsArtifact("VALUE_PARAMETER") arg: Int) = arg.toString()
    }

    @Test fun `annotations are not present at runtime`() {
        assertThat(Dummy::class.annotations, isEmpty)
    }
}
