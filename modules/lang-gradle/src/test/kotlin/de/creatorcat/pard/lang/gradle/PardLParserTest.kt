/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.pard.lang.gradle

import de.creatorcat.kotiya.matchers.common.isEmpty
import de.creatorcat.kotiya.matchers.not
import de.creatorcat.kotiya.test.core.assertThat
import java.io.File
import kotlin.test.Test
import kotlin.test.assertEquals

class PardLParserTest {

    private val parser = PardLParser()

    @Test fun `encoding - default and set via construction`() {
        assertEquals("UTF-8", parser.encoding)
        val otherParser = PardLParser("other")
        assertEquals("other", otherParser.encoding)
    }

    @Test fun `valid models`() {
        for (file in listOf(
            validPardFile("example1"),
            validPardFile("pardlSpec"),
            validPardFile("singleReq")
        )) {
            file.assertIsValidPardFile()
        }
    }

    @Test fun `models with errors`() {
        for (file in listOf(
            failParsingPardFile("document-missing-id"),
            failParsingPardFile("artifact-missing-id"),
            failParsingPardFile("artifact-missing-default-prop")
        )) {
            file.assertHasParseErrors()
        }
    }

    private fun File.assertIsValidPardFile() {
        val result = parser.parse(this)
        assertThat(result.errors, isEmpty)
        assertThat(result.warnings, isEmpty)
    }

    private fun File.assertHasParseErrors() {
        val result = parser.parse(this)
        assertThat(result.errors, !isEmpty)
        assertThat(result.warnings, isEmpty)
        println(result.errors)
    }
}
