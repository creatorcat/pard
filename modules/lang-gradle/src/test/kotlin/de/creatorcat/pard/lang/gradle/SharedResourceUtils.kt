/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.pard.lang.gradle

import de.creatorcat.pard.model.simple.SimpleRequirementsModelBuilder
import java.io.File

internal val sharedResourceDir = File("../shared-resources")
internal val pardSamplesDir = File(sharedResourceDir, "pard-samples")
internal val validPardSamplesDir = File(pardSamplesDir, "valid")
internal val failParsingPardSamplesDir = File(pardSamplesDir, "fail-parsing")
internal val idConflictingPardSamplesDir = File(pardSamplesDir, "id-conflicts")

internal fun pardFile(dir: File, name: String): File = File(dir, "${name}.pard")
internal fun validPardFile(name: String): File = pardFile(validPardSamplesDir, name)
internal fun failParsingPardFile(name: String): File = pardFile(failParsingPardSamplesDir, name)

internal fun SimpleRequirementsModelBuilder.configExample1() {
    addDocument("Bsp_1", "BeispielDokument", "Beispieldokument zur Veranschaulichung") {
        addArtifact("BspReq_1", "BeispielAnforderung", "Nur ein Beispiel")
        addArtifact("BspReq_2", "BeispielAnforderung", "Ein zweites Beispiel")
    }
}

internal fun SimpleRequirementsModelBuilder.configSingleReq() {
    addDocument("Spec1", "SpecDoc", "Sample document with single requirement") {
        addArtifact("Sample_1", "Requirement", "Just a sample")
    }
}

internal fun SimpleRequirementsModelBuilder.configPardlSpec() {
    addDocument("PardlSpec", "ZielBasierteSpezifikation", "Spezifikation von PARDL") {
        addArtifact("artefactTypes", "Ziel", "Artefakttypen")
        addArtifact("artefactAttributes", "Ziel", "Artefakt mit Attributen")
        addArtifact("attributeName", "Ziel", "Attribut\$:eindeutiger Bezeichner")
        addArtifact("attributeDomain", "Ziel", "Attribut\$:fester Wertebereich")
        addArtifact("attributeBaseTypes", "Ziel", "Attribut\$:Standardwerte")
        addArtifact("editorSupport", "Ziel", "Eingabeunterstützung")
        addArtifact("validation", "Ziel", "Validierung")
        addArtifact("validationInEditor", "Szenario", "Validierung bei der Eingabe")
    }
}

