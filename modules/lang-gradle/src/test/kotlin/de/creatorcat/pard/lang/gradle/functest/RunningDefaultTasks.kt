/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.pard.lang.gradle.functest

import de.creatorcat.kotiya.matchers.common.contains
import de.creatorcat.kotiya.test.core.assertThat
import de.creatorcat.kotiya.test.gradle.FunctionalGradleTestBase
import de.creatorcat.pard.lang.gradle.PardLangPlugin
import de.creatorcat.pard.lang.gradle.PardLangPlugin.Companion.PARSE_TASK_NAME
import kotlin.test.Test

class RunningDefaultTasks : FunctionalGradleTestBase(PardLangPlugin.ID) {

    @Test fun `parse from default spec dir`() {
        writeBuildScript(
            """
            task useModel {
                dependsOn($PARSE_TASK_NAME)
                doFirst {
                    logger.lifecycle("{}", $PARSE_TASK_NAME.model) 
                }
            }
            """.trimIndent()
        )
        runAndAssertTask("useModel")
    }

    @Test fun `parse and print default spec dir`() {
        writeBuildScript("")
        val result = runAndAssertTask("pardPrintSpecModel")
        assertThat(
            result.output,
            contains("Simple requirements model")
        )
    }
}
