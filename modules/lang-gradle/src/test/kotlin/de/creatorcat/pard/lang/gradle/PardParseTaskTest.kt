/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.pard.lang.gradle

import de.creatorcat.kotiya.matchers.Matcher
import de.creatorcat.kotiya.matchers.byDelegationTo
import de.creatorcat.kotiya.matchers.common.containsAllAndOnly
import de.creatorcat.kotiya.matchers.common.endsWith
import de.creatorcat.kotiya.matchers.common.hasElementsEqual
import de.creatorcat.kotiya.matchers.common.isEmpty
import de.creatorcat.kotiya.matchers.operators.each
import de.creatorcat.kotiya.test.core.assertThat
import de.creatorcat.kotiya.test.core.assertThrows
import de.creatorcat.kotiya.test.gradle.DummyClosure
import de.creatorcat.pard.model.simple.SimpleRequirementsModel
import de.creatorcat.pard.model.simple.build
import org.gradle.api.file.SourceDirectorySet
import org.gradle.testfixtures.ProjectBuilder
import java.io.File
import kotlin.test.Test
import kotlin.test.assertEquals

class PardParseTaskTest {

    private val project = ProjectBuilder.builder().build()
    private val task = project.tasks.create("testeeTask", PardParseTask::class.java)

    @Test fun `sourceFiles - simple dir`() {
        task.sourceSet.srcDir(validPardSamplesDir.absoluteFile)
        assertThat(
            task.sourceFiles.map { it.canonicalFile },
            containsAllAndOnly(listOf("example1", "pardlSpec", "singleReq")
                .map { validPardFile(it).canonicalFile })
        )
    }

    @Test fun `sourceFiles - dir hierarchy`() {
        task.sourceSet.srcDir(pardSamplesDir.absoluteFile)
        assertThat(
            task.sourceFiles.map { it.canonicalFile },
            containsAllAndOnly(
                listOf("artifact-missing-default-prop", "artifact-missing-id", "document-missing-id")
                    .map { failParsingPardFile(it).canonicalFile }
                    + listOf(File(idConflictingPardSamplesDir, "conflicts-with-example1.pard").canonicalFile)
                    + listOf("example1", "pardlSpec", "singleReq").map { validPardFile(it).canonicalFile }
            )
        )
    }

    @Test fun `sourceFiles - contains only pard files`() {
        task.sourceSet.srcDir(sharedResourceDir.absoluteFile)
        assertThat(
            task.sourceFiles,
            each(Matcher.byDelegationTo(endsWith(".pard")) { it.toString() })
        )
    }

    @Test fun `model - throws if not parsed`() {
        assertThrows<IllegalStateException> { task.model }
    }

    @Test fun `sourceSet - empty by default`() {
        assertThat(task.sourceSet.srcDirs, isEmpty)
        assertThat(task.sourceSet, isEmpty)
    }

    @Test fun `sourceSet - configuration via closure`() {
        task.sourceSet(DummyClosure.of<Unit, SourceDirectorySet> {
            srcDir(validPardSamplesDir.canonicalFile)
        })
        assertThat(task.sourceSet.srcDirs, hasElementsEqual(validPardSamplesDir.canonicalFile))
    }

    @Test fun `parsePardLDocuments - succeeds`() {
        task.sourceSet.srcDirs(validPardSamplesDir.absoluteFile)
        assertEquals(3, task.parsePardLDocuments().size)
    }

    @Test fun `parsePardLDocuments - no pard files`() {
        assertThat(task.parsePardLDocuments(), isEmpty)
    }

    @Test fun `parsePardLDocuments - has errors`() {
        task.sourceSet.srcDirs(failParsingPardSamplesDir.absoluteFile)
        assertThrows<RuntimeException> { task.parsePardLDocuments() }
    }

    @Test fun `parse - proper model is set`() {
        task.sourceSet.srcDirs(validPardSamplesDir.absoluteFile)
        task.parse()
        val expectedModel = SimpleRequirementsModel.build {
            configExample1()
            configPardlSpec()
            configSingleReq()
        }
        assertThat(task.model.documents, containsAllAndOnly(expectedModel.documents))
    }

    @Test fun `parse - id validation fails`() {
        task.sourceSet.srcDirs(validPardSamplesDir.absoluteFile)
        task.sourceSet.srcDirs(idConflictingPardSamplesDir.absoluteFile)
        // check that doc parsing does NOT throw
        task.parsePardLDocuments()
        assertThrows<RuntimeException> { task.parse() }
    }

    @Test fun `encoding - defaults, get and set`() {
        assertEquals("UTF-8", task.encoding)
        task.encoding = "other"
        assertEquals("other", task.encoding)
    }
}
