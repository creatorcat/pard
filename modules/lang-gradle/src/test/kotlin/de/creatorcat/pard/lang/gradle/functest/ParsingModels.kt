/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.pard.lang.gradle.functest

import de.creatorcat.kotiya.core.text.sloshToSlash
import de.creatorcat.kotiya.test.gradle.FunctionalGradleTestBase
import de.creatorcat.pard.lang.gradle.PardLangPlugin
import de.creatorcat.pard.lang.gradle.PardLangPlugin.Companion.PARSE_TASK_NAME
import de.creatorcat.pard.lang.gradle.failParsingPardSamplesDir
import de.creatorcat.pard.lang.gradle.validPardSamplesDir
import org.gradle.testkit.runner.TaskOutcome.FAILED
import kotlin.test.Test

class ParsingModels : FunctionalGradleTestBase(PardLangPlugin.ID) {

    @Test fun `parse valid model`() {
        writeBuildScript(
            """
            $PARSE_TASK_NAME {
                sourceSet {
                    srcDirs = ["${validPardSamplesDir.canonicalPath.sloshToSlash()}"]
                }
            }
            """.trimIndent()
        )
        runAndAssertTask(PARSE_TASK_NAME)
    }

    @Test fun `parsing fails with invalid model`() {
        writeBuildScript(
            """
            $PARSE_TASK_NAME {
                sourceSet.srcDirs = ["${failParsingPardSamplesDir.canonicalPath.sloshToSlash()}"]
            }
            """.trimIndent()
        )
        runAndAssertTask(PARSE_TASK_NAME, expectedOutcome = FAILED, expectBuildSuccess = false)
    }

    @Test fun `parse and use model`() {
        writeBuildScript(
            """
            $PARSE_TASK_NAME {
                sourceSet.srcDirs= ["${validPardSamplesDir.canonicalPath.sloshToSlash()}"]
            }
            
            task useModel {
                dependsOn($PARSE_TASK_NAME)
                doFirst {
                    logger.lifecycle("{}", $PARSE_TASK_NAME.model)
                }
            }
            """.trimIndent()
        )
        runAndAssertTask("useModel")
    }
}
