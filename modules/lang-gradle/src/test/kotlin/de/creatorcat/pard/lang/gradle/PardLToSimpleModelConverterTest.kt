/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.pard.lang.gradle

import de.creatorcat.kotiya.matchers.common.isEmpty
import de.creatorcat.kotiya.test.core.assertThat
import de.creatorcat.pard.model.simple.SimpleRequirementsModel
import de.creatorcat.pard.model.simple.SimpleRequirementsModelBuilder
import de.creatorcat.pard.model.simple.build
import de.creatorcat.pardl.dsl.pardLanguage.Document
import kotlin.test.Test
import kotlin.test.assertEquals

class PardLToSimpleModelConverterTest {

    private val parser = PardLParser()
    private val converter = PardLToSimpleModelConverter()

    @Test fun `single doc conversion - example1`() {
        assertConversion("example1") {
            configExample1()
        }
    }

    @Test fun `single doc conversion - singleReq`() {
        assertConversion("singleReq") {
            configSingleReq()
        }
    }

    @Test fun `single doc conversion - pardlSpec`() {
        assertConversion("pardlSpec") {
            configPardlSpec()
        }
    }

    @Test fun `multiple docs conversion`() {
        assertConversion("example1", "singleReq", "pardlSpec") {
            configExample1()
            configSingleReq()
            configPardlSpec()
        }
    }

    private fun assertConversion(
        vararg fileNames: String, builderConfig: SimpleRequirementsModelBuilder.() -> Unit
    ) {
        val docs = fileNames.map(::parse)
        val actualModel = convert(docs)
        assertEquals(
            SimpleRequirementsModel.build { builderConfig() },
            actualModel
        )
    }

    private fun convert(docs: List<Document>): SimpleRequirementsModel =
        converter.convert(docs)

    private fun parse(fileName: String): Document {
        val result = parser.parse(validPardFile(fileName))
        assertThat(result.errors, isEmpty)
        assertThat(result.warnings, isEmpty)
        return result.document
    }
}
