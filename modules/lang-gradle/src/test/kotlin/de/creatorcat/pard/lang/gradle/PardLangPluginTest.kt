/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.pard.lang.gradle

import de.creatorcat.kotiya.matchers.common.hasElementsEqual
import de.creatorcat.kotiya.matchers.common.isInstanceOf
import de.creatorcat.kotiya.test.core.assertThat
import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import kotlin.test.Test
import kotlin.test.assertSame
import kotlin.test.assertTrue

class PardLangPluginTest {

    private val project: Project = with(ProjectBuilder.builder()) {
        val project = build()
        project.pluginManager.apply(PardLangPlugin.ID)
        project
    }

    @Test fun `plugin - can be applied`() {
        assertTrue(project.pluginManager.hasPlugin(PardLangPlugin.ID))

    }

    @Test fun `default parse task - is defined with proper id`() {
        assertThat(
            project.tasks.getByPath(":${PardLangPlugin.PARSE_TASK_NAME}"),
            isInstanceOf<PardParseTask>()
        )
    }

    @Test fun `default parse task - has default spec source dir`() {
        val task = project.tasks.getByPath(":${PardLangPlugin.PARSE_TASK_NAME}") as PardParseTask
        assertThat(
            task.sourceSet.srcDirs,
            hasElementsEqual(project.file(PardLangPlugin.DEFAULT_SPEC_SOURCE_DIR))
        )
    }

    @Test fun `default print model task - is defined with proper id`() {
        assertThat(
            project.tasks.getByPath(":${PardLangPlugin.PRINT_MODEL_TASK_NAME}"),
            isInstanceOf<PardPrintModelTask>()
        )
    }

    @Test fun `default print model task - has default parse task as provider`() {
        val printTask = project.tasks.getByPath(":${PardLangPlugin.PRINT_MODEL_TASK_NAME}") as PardPrintModelTask
        val parseTask = project.tasks.getByPath(":${PardLangPlugin.PARSE_TASK_NAME}")
        assertSame<Any?>(parseTask, printTask.modelProvider)
        assertThat(printTask.taskDependencies.getDependencies(printTask), hasElementsEqual<Any?>(parseTask))
    }
}
