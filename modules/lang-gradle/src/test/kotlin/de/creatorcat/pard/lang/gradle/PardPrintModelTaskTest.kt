/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.pard.lang.gradle

import com.nhaarman.mockitokotlin2.mock
import de.creatorcat.kotiya.matchers.common.hasElementsEqual
import de.creatorcat.kotiya.matchers.common.isEmpty
import de.creatorcat.kotiya.test.core.assertThat
import de.creatorcat.kotiya.test.core.assertThrows
import de.creatorcat.pard.model.simple.SimpleRequirementsModel
import de.creatorcat.pard.model.simple.SimpleRequirementsModelBuilder
import de.creatorcat.pard.model.simple.SimpleRequirementsModelProvider
import de.creatorcat.pard.model.simple.build
import org.gradle.api.Task
import org.gradle.testfixtures.ProjectBuilder
import kotlin.test.Test
import kotlin.test.assertNull
import kotlin.test.assertSame

class PardPrintModelTaskTest {

    private val project = ProjectBuilder.builder().build()
    private val task = project.tasks.create("testeeTask", PardPrintModelTask::class.java)

    @Test fun `modelProvider - default, get and set`() {
        assertNull(task.modelProvider)
        val provider = providerOf {}
        task.modelProvider = provider
        assertSame(provider, task.modelProvider)
    }

    @Test fun `modelProvider - set adjusts dependency for tasks`() {
        val taskProvider1 = mock<TaskModelProvider>()
        val taskProvider2 = mock<TaskModelProvider>()

        with(task) {
            assertThat(taskDependencies.getDependencies(this), isEmpty)
            modelProvider = taskProvider1
            assertThat(taskDependencies.getDependencies(this), hasElementsEqual<Any?>(taskProvider1))
            modelProvider = taskProvider2
            assertThat(taskDependencies.getDependencies(this), hasElementsEqual<Any?>(taskProvider2))
            modelProvider = providerOf {}
            assertThat(taskDependencies.getDependencies(this), isEmpty)
        }
    }

    @Test fun `printModel - valid provider`() {
        task.modelProvider = providerOf {
            addDocument("d1", "Spec", "Spec 1") {
                addArtifact("a1", "Req", "Req 1")
            }
        }
        task.printModel()
    }

    @Test fun `printModel - invalid provider`() {
        assertThrows<IllegalStateException> { task.printModel() }
    }

    private fun providerOf(modelConfig: SimpleRequirementsModelBuilder.() -> Unit): SimpleRequirementsModelProvider =
        object : SimpleRequirementsModelProvider {
            override val model = SimpleRequirementsModel.build(modelConfig)
        }

    private interface TaskModelProvider : SimpleRequirementsModelProvider, Task
}
