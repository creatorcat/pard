/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.pard.lang.gradle

import de.creatorcat.pardl.dsl.PardLanguageStandaloneSetup
import de.creatorcat.pardl.dsl.pardLanguage.Document
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.resource.XtextResource
import org.eclipse.xtext.resource.XtextResourceSet
import java.io.File

/**
 * Parser for PARD-Language files.
 * Parsing will produce a [PARDL document][Document] and a list of errors and warnings
 * that occurred during parsing.
 * If any errors are listed, the resulting model will not be valid.
 */
class PardLParser(
    /** Encoding of the files to parse, defaults to `"UTF-8"` */
    val encoding: String = "UTF-8"
) {

    private val resourceSet: XtextResourceSet

    init {
        val injector = PardLanguageStandaloneSetup().createInjectorAndDoEMFRegistration()
        resourceSet = injector.getInstance(XtextResourceSet::class.java)
        resourceSet.addLoadOption(XtextResource.OPTION_RESOLVE_ALL, true)
        resourceSet.addLoadOption(XtextResource.OPTION_ENCODING, encoding)
    }

    /**
     * Tries to parse the given [pardFile].
     */
    fun parse(pardFile: File): ParseResult =
        with(parseResourceFromFile(pardFile)) {
            if (contents.isEmpty())
                throw IllegalArgumentException("could not parse any model from $pardFile")
            val document = contents[0] as? Document
                ?: throw IllegalArgumentException("could not parse PardL document from $pardFile")
            val errorMessages = errors.map { it.message }
            val warningMessages = warnings.map { it.message }

            ParseResult(document, errorMessages, warningMessages)
        }

    private fun parseResourceFromFile(pardFile: File): Resource =
        resourceSet.getResource(URI.createURI(pardFile.toURI().toString()), true)

    /**
     * Result of [PardLParser.parse].
     */
    class ParseResult(
        /** Parsed EMF PardL document object. */
        val document: Document,
        /**
         * Error messages produced by the parse operation.
         * If any errors are listed, the resulting [document] will not be valid.
         */
        val errors: List<String>,
        /** Warning messages produced by the parse operation. */
        val warnings: List<String>
    )
}
