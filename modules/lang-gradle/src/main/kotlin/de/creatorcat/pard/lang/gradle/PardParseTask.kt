/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.pard.lang.gradle

import de.creatorcat.kotiya.gradle.apply
import de.creatorcat.kotiya.gradle.logJavaStyleError
import de.creatorcat.kotiya.gradle.logJavaStyleWarning
import de.creatorcat.pard.model.simple.SimpleModelIdValidator
import de.creatorcat.pard.model.simple.SimpleModelValidationFaultGenerator
import de.creatorcat.pard.model.simple.SimpleRequirementsModel
import de.creatorcat.pard.model.simple.SimpleRequirementsModelProvider
import de.creatorcat.pardl.dsl.pardLanguage.Document
import groovy.lang.Closure
import org.gradle.api.DefaultTask
import org.gradle.api.file.SourceDirectorySet
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.InputFiles
import org.gradle.api.tasks.Internal
import org.gradle.api.tasks.TaskAction
import java.io.File

/**
 * Task to [parse] a set of `.pard` files from the [sourceSet]
 * and convert them into a [simple model][model].
 * The task also performs a basic validation on the model:
 * all documents and artifacts must have unique identifiers.
 *
 * If parsing any source file produces (syntax) errors or the model is not valid
 * the task will fail and errors will be logged.
 *
 * Configuration in Gradle (with default values):
 *
 *     task myParseTask(type: PardParseTask) {
 *
 *         encoding = "UTF-8" // source file encoding
 *
 *         sourceSet {
 *             // This is an ordinary Gradle SourceDirectorySet so it can be configured like e.g. java source sets.
 *             // Only *.pard files will be recognized as sources.
 *             // The set is empty for new tasks but the default plugin task contains the folder "spec".
 *
 *             // add some dir
 *             srcDir("mySpecDir")
 *         }
 *     }
 */
// TODO design
// the task has no inputs defined, because the output is a model object and thus it is NEVER up-to-date
open class PardParseTask : DefaultTask(), SimpleRequirementsModelProvider {

    init {
        description = "parses pard files and performs basic validation"
        group = PardLangPlugin.PARD_TASK_GROUP
    }

    private var parsedModel: SimpleRequirementsModel? = null

    /** Encoding of the [sourceFiles] to parse, defaults to `"UTF-8"` */
    @Input
    var encoding: String = "UTF-8"

    /**
     * The model generated on task execution.
     * Accessing this before parsing has succeeded will fail with an exception.
     */
    override val model: SimpleRequirementsModel
        @Internal
        get() = parsedModel ?: throw IllegalStateException("no model available, run task first")

    /**
     * Source set containing the `.pard` files to parse.
     * Only `.pard` files will be recognized as [sourceFiles], other files are ignored.
     */
    @Suppress("UnstableApiUsage")
    @Internal
    val sourceSet: SourceDirectorySet =
        project.objects.sourceDirectorySet("pardSources", "Sources of a PardParseTask")

    /**
     * Configure the [sourceSet] via the [configuration] closure.
     *
     * @return the [sourceSet] after applying the closure
     */
    fun sourceSet(configuration: Closure<Unit>): SourceDirectorySet {
        sourceSet.apply(configuration)
        return sourceSet
    }

    /**
     * The `.pard` files currently present in the [sourceSet]. Will be re-generated on each call.
     * This is the set of files actually used for [parsing][parse].
     */
    val sourceFiles: Set<File>
        @InputFiles
        get() = with(sourceSet) {
            filter.include("**/*.pard")
            files
        }

    /**
     * The task action: Parses the [sourceFiles] and converts them into a [simple model][model].
     * If parsing any source file produces errors or the model is not valid
     * (meaning it has duplicate document or artifact ids),
     * the action will fail and errors will be logged.
     */
    @TaskAction
    fun parse() {
        val pardLDocs = parsePardLDocuments()
        if (pardLDocs.isEmpty()) return
        parsedModel = PardLToSimpleModelConverter().convert(pardLDocs)
        validateModelIds()
    }

    internal fun parsePardLDocuments(): List<Document> {
        val parser = PardLParser(encoding)
        var hasErrors = false
        val documents = mutableListOf<Document>()
        val pardFiles = sourceFiles
        if (pardFiles.isEmpty())
            logJavaStyleWarning("parsing aborted", "no pard files found in source directories")
        for (file in pardFiles) {
            logger.info("parsing pard file {}", file.canonicalFile)
            with(parser.parse(file)) {
                hasErrors = errors.isNotEmpty()
                for (error in errors)
                    logJavaStyleError("error parsing ${file.canonicalFile}", error)
                for (warning in warnings)
                    logJavaStyleWarning("warning parsing ${file.canonicalFile}", warning)
                documents.add(document)
            }
        }
        if (hasErrors) throw RuntimeException("parsing pard sources produced errors")
        return documents
    }

    private fun validateModelIds() {
        val validator = SimpleModelIdValidator()
        val msgGenerator = SimpleModelValidationFaultGenerator()
        val documentFaults = validator.validateDocumentIds(model)
        val artifactFaults = validator.validateArtifactIds(model)
        for (entry in documentFaults)
            logJavaStyleError("model validation fault", msgGenerator.generateDocumentIdFault(entry.toPair()))
        for (entry in artifactFaults)
            logJavaStyleError("model validation fault", msgGenerator.generateArtifactIdFault(entry.toPair()))
        if (artifactFaults.isNotEmpty() || documentFaults.isNotEmpty())
            throw RuntimeException("model validation failed")
    }
}
