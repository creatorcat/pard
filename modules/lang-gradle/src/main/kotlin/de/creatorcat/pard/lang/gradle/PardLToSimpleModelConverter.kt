/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.pard.lang.gradle

import de.creatorcat.pard.model.simple.SimpleRequirementsArtifactSpec
import de.creatorcat.pard.model.simple.SimpleRequirementsDocumentBuilder
import de.creatorcat.pard.model.simple.SimpleRequirementsModel
import de.creatorcat.pard.model.simple.build
import de.creatorcat.pardl.dsl.pardLanguage.Document
import de.creatorcat.pardl.dsl.pardLanguage.EmbeddedImage
import de.creatorcat.pardl.dsl.pardLanguage.Hyperlink
import de.creatorcat.pardl.dsl.pardLanguage.PardlScript
import de.creatorcat.pardl.dsl.pardLanguage.PlainText
import de.creatorcat.pardl.dsl.pardLanguage.ReqSection
import de.creatorcat.pardl.dsl.pardLanguage.TextPart
import de.creatorcat.pardl.dsl.pardLanguage.TextValue

/**
 * Provides functionality to convert a number of parsed [PardL documents][Document]
 * into a [SimpleRequirementsModel] where each `PardL document` is represented by one simple document
 * and each parsed requirement artifact section is represented by one simple artifact object.
 */
class PardLToSimpleModelConverter {

    /**
     * Converts the given [pardLDocuments] into simple documents inside the
     * resulting [SimpleRequirementsModel].
     */
    fun convert(pardLDocuments: List<Document>): SimpleRequirementsModel =
        SimpleRequirementsModel.build {
            for (pardLDoc in pardLDocuments)
                documentBuilders.add(convert(pardLDoc))
        }

    private fun convert(pardLDocument: Document): SimpleRequirementsDocumentBuilder {
        val docBuilder = with(pardLDocument) {
            val id = retrieveSimpleId(header.id.id)
            val type = header.type
            val title = header.title.generatePlainString()
            SimpleRequirementsDocumentBuilder(id, type, title)
        }
        pardLDocument.sections
            .filterIsInstance<ReqSection>()
            .forEach {
                docBuilder.artifactSpecs.add(convert(it))
            }
        return docBuilder
    }

    private fun convert(reqSection: ReqSection): SimpleRequirementsArtifactSpec =
        with(reqSection) {
            val id = retrieveSimpleId(header.id.id)
            val type = header.type
            val title = header.title.generatePlainString()
            SimpleRequirementsArtifactSpec(id, type, title)
        }

    private fun retrieveSimpleId(plainIdString: String) =
        plainIdString.drop(1) // remove leading '#'

    private fun TextValue.generatePlainString(): String =
        parts.joinToString(separator = "") { it.generatePlainString() }

    private fun TextPart.generatePlainString(): String =
        when (this) {
            is PlainText -> this.text
            is PardlScript -> this.script
            is EmbeddedImage -> this.image
            is Hyperlink -> this.link
            else -> throw IllegalArgumentException("unknown text part type: " + this::class)
        }
}
