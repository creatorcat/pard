/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.pard.lang.gradle

import de.creatorcat.kotiya.core.text.CREATOR_CAT_CLI_HELLO
import de.creatorcat.pard.lang.gradle.PardLangPlugin.Companion.DEFAULT_SPEC_SOURCE_DIR
import de.creatorcat.pard.lang.gradle.PardLangPlugin.Companion.PARSE_TASK_NAME
import de.creatorcat.pard.lang.gradle.PardLangPlugin.Companion.PRINT_MODEL_TASK_NAME
import org.gradle.api.Plugin
import org.gradle.api.Project

/**
 * Pard language plugin.
 *
 * Adds the following when applied to a project:
 * * A [PardParseTask] named [PARSE_TASK_NAME], with its [sourceSet][PardParseTask.sourceSet]
 *   already containing the project folder [DEFAULT_SPEC_SOURCE_DIR].
 * * A [PardPrintModelTask] named [PRINT_MODEL_TASK_NAME].
 */
class PardLangPlugin : Plugin<Project> {

    /**
     * Apply the plugin to the [target] project.
     *
     * @see [Plugin.apply]
     */
    override fun apply(target: Project) {
        target.logger.info(CREATOR_CAT_CLI_HELLO)
        val parseTask = target.tasks.register(PARSE_TASK_NAME, PardParseTask::class.java) {
            it.sourceSet.srcDir(DEFAULT_SPEC_SOURCE_DIR)
        }
        target.tasks.register(PRINT_MODEL_TASK_NAME, PardPrintModelTask::class.java) {
            it.modelProvider = parseTask.get()
        }
    }

    companion object {
        /** The plugin ID - used when applying the plugin in a build script: `de.creatorcat.pard.lang` */
        const val ID: String = "de.creatorcat.pard.lang"
        /** The name of the default [PardParseTask]: `pardParseSpec` */
        const val PARSE_TASK_NAME: String = "pardParseSpec"
        /** The name of the default [PardPrintModelTask]: `pardPrintSpecModel` */
        const val PRINT_MODEL_TASK_NAME: String = "pardPrintSpecModel"
        /**
         * Name of the default pard spec source directory added to the [sourceSet][PardParseTask.sourceSet]
         * of the [default parse task][PARSE_TASK_NAME]: `spec`.
         */
        const val DEFAULT_SPEC_SOURCE_DIR: String = "spec"
        // TODO use generated constant
        /** name of the gradle task group for pard tasks */
        const val PARD_TASK_GROUP: String = "specification"
    }
}
