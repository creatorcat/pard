/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.pard.lang.gradle

import de.creatorcat.kotiya.gradle.isDirectTaskDependency
import de.creatorcat.pard.model.simple.SimpleModelPrinter
import de.creatorcat.pard.model.simple.SimpleRequirementsModel
import de.creatorcat.pard.model.simple.SimpleRequirementsModelProvider
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.Console
import org.gradle.api.tasks.TaskAction
import java.util.concurrent.Callable

/**
 * Task to print a human readable presentation of a [simple model][SimpleRequirementsModel]
 * to the logger output.
 *
 * Configuration in Gradle (with defaults):
 *
 *     task myPrintTask(type: PardPrintModelTask) {
 *         modelProvider = null
 *     }
 */
open class PardPrintModelTask : DefaultTask() {

    init {
        description = "prints a hierarchical model representation to the log"
        group = PardLangPlugin.PARD_TASK_GROUP
    }

    /**
     * The provider of the model to print. For example a [PardParseTask].
     * The model will be retrieved lazily just at print time.
     * Defaults to `null`.
     *
     * If the provider is a [direct task dependency][isDirectTaskDependency], this task will depend on it.
     */
    @Console
    var modelProvider: SimpleRequirementsModelProvider? = null
        set(value) {
            // on first non-null provider set it as potential dependency
            if (field == null && value != null)
                dependsOn(Callable { modelProvider.takeIf(::isDirectTaskDependency) })
            field = value
        }

    /**
     * The task action: Prints the model retrieved form the [modelProvider] using
     * [logger.lifecycle][DefaultTask.logger].
     */
    @TaskAction
    fun printModel() {
        val existingProvider = modelProvider ?: throw IllegalStateException("no model provider defined")
        val model = existingProvider.model
        val buffer = StringBuilder()
        val printer = SimpleModelPrinter(buffer)
        printer.print(model)
        logger.lifecycle("{}", buffer)
    }
}
