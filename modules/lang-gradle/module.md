# Module pard-lang-gradle

This module provides the [Pard language Gradle plugin](https://creatorcat.gitlab.io/pard/docs/lang-gradle/html/pard-lang-gradle/de.creatorcat.pard.lang.gradle/-pard-lang-plugin)
to process *PARDL* (`.pard`) files. 

State: *Alpha*
* Only a [simple model](https://creatorcat.gitlab.io/pard/docs/model/html/pard-model/de.creatorcat.pard.model.simple/-simple-requirements-model) can be generated so far.
  Properties of *PARDL* requirements are not present in the model.

Dependencies:
* [pard-model](https://creatorcat.gitlab.io/pard/docs/model/html/pard-model) for the generated requirements model
* CreatorCat Kotiya: `kotiya-core`, `kotiya-matchers` and `kotiya-gradle`
* `de.creatorcat.pardl.dsl` - the *PARDL* parser based on Eclipse XText 

# Package de.creatorcat.pard.lang.gradle

Contains the plugin, the tasks and all necessary functionality.
