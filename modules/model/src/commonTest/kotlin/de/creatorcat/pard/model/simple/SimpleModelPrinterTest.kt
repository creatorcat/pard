/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.pard.model.simple

import de.creatorcat.kotiya.core.text.SYSTEM_LINE_SEPARATOR
import de.creatorcat.kotiya.core.text.appendln
import kotlin.test.Test
import kotlin.test.assertEquals

class SimpleModelPrinterTest {

    companion object {
        private val model = SimpleRequirementsModel.build {
            addDocument("d1", "Spec", "Spec 1") {
                addArtifact("g1", "Goal", "Goal 1")
                addArtifact("r1", "Req", "Req 1")
                addArtifact("r2", "Req", "Req 2")
            }
            addDocument("d2", "Spec", "Spec 2") {
                addArtifact("g2", "Goal", "Goal 2")
                addArtifact("r3", "Req", "Req 3")
                addArtifact("r4", "Req", "Req 4")
            }
        }

        private const val IND = MODEL_LEVEL_INDENT
        private const val IND2 = "$IND$IND"
    }

    private val testee = SimpleModelPrinter(StringBuilder())

    @Test fun `print single artifact`() {
        testee.print(model.documents[0].artifacts[0])
        assertEquals("Goal #g1 - Goal 1$SYSTEM_LINE_SEPARATOR", testee.appendable.toString())
    }

    @Test fun `print single document`() {
        testee.print(model.documents[0])
        assertEquals(
            StringBuilder()
                .appendln("Document #d1 of type Spec with 3 artifacts,")
                .appendln("title = Spec 1:")
                .appendln("${IND}Goal #g1 - Goal 1")
                .appendln("${IND}Req #r1 - Req 1")
                .appendln("${IND}Req #r2 - Req 2")
                .toString(),
            testee.appendable.toString()
        )
    }

    @Test fun `print model`() {
        testee.print(model)
        assertEquals(
            StringBuilder()
                .appendln("Simple requirements model with 2 documents:")
                .appendln("${IND}Document #d1 of type Spec with 3 artifacts,")
                .appendln("${IND}title = Spec 1:")
                .appendln("${IND2}Goal #g1 - Goal 1")
                .appendln("${IND2}Req #r1 - Req 1")
                .appendln("${IND2}Req #r2 - Req 2")
                .appendln("${IND}Document #d2 of type Spec with 3 artifacts,")
                .appendln("${IND}title = Spec 2:")
                .appendln("${IND2}Goal #g2 - Goal 2")
                .appendln("${IND2}Req #r3 - Req 3")
                .appendln("${IND2}Req #r4 - Req 4")
                .toString(),
            testee.appendable.toString()
        )
    }
}
