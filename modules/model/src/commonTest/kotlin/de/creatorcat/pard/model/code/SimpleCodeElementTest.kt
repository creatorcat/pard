/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.pard.model.code

import kotlin.test.Test
import kotlin.test.assertEquals

class SimpleCodeElementTest {

    @Test fun `creation and properties`() {
        with(SimpleCodeElement("T", "n", "s")) {
            assertEquals("T", type)
            assertEquals("n", name)
            assertEquals("s", scope)
        }
    }
}
