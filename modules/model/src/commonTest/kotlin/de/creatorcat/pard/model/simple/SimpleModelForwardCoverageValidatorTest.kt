/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.pard.model.simple

import de.creatorcat.kotiya.matchers.common.hasElementsEqual
import de.creatorcat.kotiya.matchers.common.isEmpty
import de.creatorcat.kotiya.test.core.assertThat
import de.creatorcat.pard.model.code.CodeToArtifactLink
import de.creatorcat.pard.model.code.SimpleCodeElement
import de.creatorcat.pard.model.code.StandardCodeToArtifactLink
import kotlin.test.Test
import kotlin.test.assertEquals

class SimpleModelForwardCoverageValidatorTest {

    private val validator = SimpleModelForwardCoverageValidator()

    @Test fun `validation - empty model, empty links`() {
        validate {
            assertThat(missingTargetLinkData, isEmpty)
            assertThat(uncoveredArtifacts, isEmpty)
            assertThat(coveredArtifactLinkData, isEmpty)
        }
    }

    @Test fun `validation - empty model, existing links`() {
        validate("a1", "a2") {
            assertEquals(linkData("a1", "a2"), missingTargetLinkData)
            assertThat(uncoveredArtifacts, isEmpty)
            assertThat(coveredArtifactLinkData, isEmpty)
        }
    }

    @Test fun `validation - existing model, empty links`() {
        model {
            addDocument("d", "dt", "t") {
                addArtifact("a1", "at", "t")
                addArtifact("a2", "at", "t")
            }
        }
        validate {
            assertThat(missingTargetLinkData, isEmpty)
            assertThat(uncoveredArtifacts, hasElementsEqual(m[0].artifacts))
            assertThat(coveredArtifactLinkData, isEmpty)
        }
    }

    @Test fun `validation - all missing`() {
        model {
            addDocument("d", "dt", "t") {
                addArtifact("a1", "at", "t")
                addArtifact("a2", "at", "t")
            }
        }
        validate("b0", "b1") {
            assertEquals(linkData("b0", "b1"), missingTargetLinkData)
            assertThat(uncoveredArtifacts, hasElementsEqual(m[0].artifacts))
            assertThat(coveredArtifactLinkData, isEmpty)
        }
    }

    @Test fun `validation - all covered, none missing`() {
        model {
            addDocument("d", "dt", "t") {
                addArtifact("a1", "at", "t")
                addArtifact("a2", "at", "t")
            }
        }
        validate("a1", "a2") {
            assertThat(missingTargetLinkData, isEmpty)
            assertThat(uncoveredArtifacts, isEmpty)
            assertEquals(coverData(m[0, 0], m[0, 1]), coveredArtifactLinkData)
        }
    }

    @Test fun `validation - all covered, some missing`() {
        model {
            addDocument("d1", "dt", "t") {
                addArtifact("a1", "at", "t")
                addArtifact("a2", "at", "t")
            }
            addDocument("d2", "dt", "t") {
                addArtifact("a3", "at", "t")
                addArtifact("a4", "at", "t")
            }
        }
        validate("a1", "a2", "a3", "a4", "a5", "b1") {
            assertEquals(linkData("a5", "b1"), missingTargetLinkData)
            assertThat(uncoveredArtifacts, isEmpty)
            assertEquals(coverData(m[0, 0], m[0, 1], m[1, 0], m[1, 1]), coveredArtifactLinkData)
        }
    }

    @Test fun `validation - some covered, none missing`() {
        model {
            addDocument("d1", "dt", "t") {
                addArtifact("a1", "at", "t")
                addArtifact("a2", "at", "t")
            }
            addDocument("d2", "dt", "t") {
                addArtifact("a3", "at", "t")
                addArtifact("a4", "at", "t")
            }
        }
        validate("a1", "a3") {
            assertThat(missingTargetLinkData, isEmpty)
            assertThat(uncoveredArtifacts, hasElementsEqual(m[0, 1], m[1, 1]))
            assertEquals(coverData(m[0, 0], m[1, 0]), coveredArtifactLinkData)
        }
    }

    @Test fun `validation - some covered, some missing`() {
        model {
            addDocument("d1", "dt", "t") {
                addArtifact("a1", "at", "t")
                addArtifact("a2", "at", "t")
            }
            addDocument("d2", "dt", "t") {
                addArtifact("a3", "at", "t")
                addArtifact("a4", "at", "t")
            }
        }
        validate("a1", "a3", "b1", "c1") {
            assertEquals(linkData("b1", "c1"), missingTargetLinkData)
            assertThat(uncoveredArtifacts, hasElementsEqual(m[0, 1], m[1, 1]))
            assertEquals(coverData(m[0, 0], m[1, 0]), coveredArtifactLinkData)
        }
    }

    @Test fun `validation - with filter`() {
        model {
            addDocument("d1", "dt", "t") {
                addArtifact("a1", "at1", "t")
                addArtifact("a2", "at2", "t")
            }
            addDocument("d2", "dt", "t") {
                addArtifact("a3", "at2", "t")
                addArtifact("a4", "at1", "t")
            }
        }
        val result = SimpleModelForwardCoverageValidator {
            it.type == "at1"
        }.validate(m, linkData("a1", "a3"))
        with(result) {
            assertEquals(linkData("a3"), missingTargetLinkData)
            assertThat(uncoveredArtifacts, hasElementsEqual(m[1, 1]))
            assertEquals(coverData(m[0, 0]), coveredArtifactLinkData)
        }
    }

    private fun validate(vararg linkedIds: String, assertionCode: ForwardCovaResult.() -> Unit) {
        val result = validator.validate(m, linkData(*linkedIds))
        result.assertionCode()
    }

    private fun linkData(vararg linkedIds: String): List<CodeToArtifactLink> =
        linkedIds.map { StandardCodeToArtifactLink(it, codeElementFor(it), "comment") }

    private fun coverData(
        vararg artifacts: SimpleRequirementsArtifact
    ): Map<SimpleRequirementsArtifact, List<StandardCodeToArtifactLink>> =
        artifacts.associate { it to listOf(StandardCodeToArtifactLink(it.id, codeElementFor(it.id), "comment")) }

    private fun codeElementFor(id: String) =
        SimpleCodeElement("type", "c#$id", "scope")

    private var m: SimpleRequirementsModel = SimpleRequirementsModel.build { }

    private operator fun SimpleRequirementsModel.get(d: Int): SimpleRequirementsDocument =
        documents[d]

    private operator fun SimpleRequirementsModel.get(d: Int, a: Int): SimpleRequirementsArtifact =
        get(d).artifacts[a]

    private fun model(modelBuilderConfig: SimpleRequirementsModelBuilder.() -> Unit) {
        m = SimpleRequirementsModel.build(modelBuilderConfig)
    }
}
