/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.pard.model.simple

import de.creatorcat.kotiya.test.core.reflect.assertEqualsContractFor
import de.creatorcat.kotiya.test.core.reflect.assertHashCodeContractFor
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals
import kotlin.test.assertSame

class SimpleModelBuilderTest {

    companion object {
        private fun SimpleRequirementsDocumentBuilder.addDefaultArtifacts() {
            addArtifact("a1", "Req", "Title A")
            addArtifact("a1", "Req", "Title A")
            addArtifact("a1", "Req", "Title B")
            addArtifact("a1", "Goal", "Title A")
            addArtifact("d", "Req", "Title A")
        }

        private val testModelBuilder = SimpleRequirementsModelBuilder().apply {
            val d = SimpleRequirementsDocumentBuilder("d1", "ReqDoc", "Title D").apply {
                addDefaultArtifacts()
            }
            documentBuilders.add(d)
            addDocument("d1", "ReqDoc", "Title D") {
                artifactSpecs.addAll(d.artifactSpecs)
            }
            addDocument("d1", "ReqDoc", "Title D") {
                artifactSpecs.addAll(d.artifactSpecs.take(4))
            }
            addDocument("d1", "ReqDoc", "Title X") {
                artifactSpecs.addAll(d.artifactSpecs)
            }
            addDocument("d1", "ReqDoc2", "Title D") {
                artifactSpecs.addAll(d.artifactSpecs)
            }
            addDocument("d2", "ReqDoc", "Title D") {
                artifactSpecs.addAll(d.artifactSpecs)
            }
        }
        private val testModel = testModelBuilder.build()

        private val doc1 = testModel.documents[0]

        private val testArtifacts = doc1.artifacts

        private val testDocuments = testModel.documents
    }

    @Test fun `artifact - creation with properties`() {
        with(SimpleReqArtifactImplementation(doc1, "a1", "Req", "Title A")) {
            assertSame(doc1, document)
            assertEquals("a1", id)
            assertEquals("Req", type)
            assertEquals("Title A", title)
        }
    }

    @Test fun `artifact - equality`() {
        val a = testArtifacts
        assertEquals(a[0], a[1])
        assertNotEquals(a[0], a[2])
        assertNotEquals(a[0], a[3])
        assertNotEquals(a[0], a[4])

        SimpleRequirementsArtifact::equals.assertEqualsContractFor(a)
        SimpleRequirementsArtifact::hashCode.assertHashCodeContractFor(a)
    }

    @Test fun `document - immutable creation with properties`() {
        val specialDocBuilder = SimpleRequirementsDocumentBuilder("d1", "ReqDoc", "Title D").apply {
            addDefaultArtifacts()
        }
        with(SimpleReqDocumentImplementation(testModel, specialDocBuilder)) {
            assertEquals("d1", id)
            assertEquals("ReqDoc", type)
            assertEquals("Title D", title)
            assertEquals(testArtifacts, artifacts)

            // mutate original list
            specialDocBuilder.artifactSpecs[0].id = "OTHER"
            assertEquals(testArtifacts, artifacts)
        }
    }

    @Test fun `document - equality`() {
        val d = testDocuments
        assertEquals(d[0], d[1])
        assertNotEquals(d[0], d[2])
        assertNotEquals(d[0], d[3])
        assertNotEquals(d[0], d[4])
        assertNotEquals(d[0], d[5])

        SimpleRequirementsDocument::equals.assertEqualsContractFor(d)
        SimpleRequirementsDocument::hashCode.assertHashCodeContractFor(d)
    }

    @Test fun `model - immutable creation with properties`() {
        val mutableBuilders = mutableListOf(
            SimpleRequirementsDocumentBuilder("d1", "ReqDoc", "Title D").apply {
                addDefaultArtifacts()
            },
            SimpleRequirementsDocumentBuilder("d1", "ReqDoc", "Title D").apply {
                addDefaultArtifacts()
                artifactSpecs.removeAt(artifactSpecs.size - 1)
            }
        )
        with(SimpleReqModelImplementation(mutableBuilders)) {
            assertEquals(doc1, documents[0])
            assertEquals(testDocuments[2], documents[1])

            // mutate original list
            mutableBuilders[0].artifactSpecs[0].id = "OTHER"
            assertEquals(doc1, documents[0])
            assertEquals(testDocuments[2], documents[1])
        }
    }

    @Test fun `model - equality`() {
        val m1 = testModel
        val m2 = testModelBuilder.build()
        val m3 = SimpleRequirementsModel.build {
            documentBuilders.addAll(testModelBuilder.documentBuilders.take(3))
            build()
        }

        assertEquals(m1, m2)
        assertNotEquals(m1, m3)

        SimpleRequirementsModel::equals.assertEqualsContractFor(
            listOf(m1, m2, m3)
        )
        SimpleRequirementsModel::hashCode.assertHashCodeContractFor(
            listOf(m1, m2, m3)
        )
    }
}
