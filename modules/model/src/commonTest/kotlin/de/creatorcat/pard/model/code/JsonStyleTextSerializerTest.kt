/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.pard.model.code

import de.creatorcat.kotiya.matchers.AtomicMatcher
import de.creatorcat.kotiya.matchers.Matcher
import de.creatorcat.kotiya.matchers.common.hasElementsEqual
import de.creatorcat.kotiya.matchers.common.isEmpty
import de.creatorcat.kotiya.matchers.common.isNoneNull
import de.creatorcat.kotiya.matchers.common.isNotNull
import de.creatorcat.kotiya.matchers.descriptionOfAtomicCondition
import de.creatorcat.kotiya.matchers.of
import de.creatorcat.kotiya.matchers.operators.which
import de.creatorcat.kotiya.test.core.assertThat
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNull

// TODO move to kotiya
class JsonStyleTextSerializerTest {

    private val serializer = JsonStyleTextSerializer()
    private val buffer = StringBuilder()

    @Test fun `json string content conversion - both directions`() {
        assertJsonStringContentConversion("simple text", "simple text")
        assertJsonStringContentConversion(
            "escapes: / \" \t \b \\ \u000C \r \n",
            "escapes: \\/ \\\" \\t \\b \\\\ \\f \\r \\n"
        )
        assertJsonStringContentConversion(
            """
                multi
                line
                text
                with escape: "bla"
            """.trimIndent(),
            "multi\\nline\\ntext\\nwith escape: \\\"bla\\\""
        )
    }

    @Test fun `single value - write`() {
        serializer.writeSingleValue(buffer, 1234)
        assertEquals("\"1234\"", buffer.toString())

        buffer.clear()
        serializer.writeSingleValue(buffer, "23 is \"not\"\t17!")
        assertEquals("\"23 is \\\"not\\\"\\t17!\"", buffer.toString())
    }

    @Test fun `single value - read`() {
        assertEquals("simple value", serializer.readSingleValue("\"simple value\""))
        assertEquals("23 is \"not\"\t17!", serializer.readSingleValue("\"23 is \\\"not\\\"\\t17!\""))
        assertEquals("only first", serializer.readSingleValue("\"only first\" \"second\""))
        assertEquals("with comments", serializer.readSingleValue("here comes a value \"with comments\" and more"))
        assertEquals("", serializer.readSingleValue("\"\""))
        assertNull(serializer.readSingleValue("no value inside"))
    }

    @Test fun `list - write`() {
        serializer.writeList(buffer, listOf(1, 2, "hello world\n"))
        assertEquals("[\"1\", \"2\", \"hello world\\n\"]", buffer.toString())

        buffer.clear()
        serializer.writeList(buffer, emptyList<String>())
        assertEquals("[]", buffer.toString())
    }

    @Test fun `list - read`() {
        assertThat(
            serializer.readList("[\"1\", \"2\", \"hello world\\n\"]"),
            isNoneNull<List<String>>() which hasElementsEqual("1", "2", "hello world\n")
        )
        assertThat(serializer.readList("[]"), isNotNull which isEmpty)
        assertNull(serializer.readList("no list inside"))
        // with comments
        assertThat(
            serializer.readList("comment [\"1\", comment inside \"2\" missing separator... \"3\"] another comment"),
            isNoneNull<List<String>>() which hasElementsEqual("1", "2", "3")
        )
        // only first
        assertThat(
            serializer.readList("[\"1\", \"2\", \"3\"] []"),
            isNoneNull<List<String>>() which hasElementsEqual("1", "2", "3")
        )
    }

    @Test fun `map - write`() {
        serializer.writeMap(buffer, mapOf(1 to 2, "key\n" to "value\t\""))
        assertEquals("{\"1\": \"2\", \"key\\n\": \"value\\t\\\"\"}", buffer.toString())

        buffer.clear()
        serializer.writeMap(buffer, emptyMap<String, String>())
        assertEquals("{}", buffer.toString())
    }

    @Test fun `map - read`() {
        assertThat(
            serializer.readMap("{\"1\": \"2\", \"key\\n\": \"value\\t\\\"\"}"),
            isNoneNull<Map<String, String>>() which hasEntriesEqual("1" to "2", "key\n" to "value\t\"")
        )
        assertThat(serializer.readMap("{}"), isNotNull which isEmpty)
        assertNull(serializer.readMap("no map inside"))
        // with comments
        assertThat(
            serializer.readMap("comment {\"1\" : \"2\", comment inside \"3\":\"4\"} another comment"),
            isNoneNull<Map<String, String>>() which hasEntriesEqual("1" to "2", "3" to "4")
        )
        // only first
        assertThat(
            serializer.readMap("{\"1\": \"2\"} {\"3\": \"4\"}"),
            isNoneNull<Map<String, String>>() which hasEntriesEqual("1" to "2")
        )
    }

    private fun assertJsonStringContentConversion(text: String, jsonContent: String) {
        assertEquals(jsonContent, toJson(text), "to JSON")
        assertEquals(text, fromJson(jsonContent), "from JSON")
    }

    private fun toJson(text: CharSequence) = JsonStyleTextSerializer.convertToJsonStringContent(text)
    private fun fromJson(jsonStringContent: String) = JsonStyleTextSerializer.convertToText(jsonStringContent)

    // TODO extract and move to kotiya 
    fun <K, V> hasEntriesEqual(vararg expectedEntries: Pair<K, V>): AtomicMatcher<Map<out K, V>> =
    // TODO use proper array-to-string
        Matcher.of({ descriptionOfAtomicCondition("have entries equal ${expectedEntries.joinToString()}") }) {
            if (expectedEntries.size != it.size)
                return@of Pair(
                    false,
                    { "has wrong number of elements: ${it.size} instead of ${expectedEntries.size}}" }
                )
            for ((expKey, expVal) in expectedEntries) {
                val actualVal = it[expKey]
                if (actualVal != expVal)
                    return@of Pair(
                        false,
                        { "has wrong value for key=$expKey: $actualVal instead of $expVal" }
                    )
            }
            Pair(true, { "has elements equal ${expectedEntries.joinToString()}}" })
        }
}
