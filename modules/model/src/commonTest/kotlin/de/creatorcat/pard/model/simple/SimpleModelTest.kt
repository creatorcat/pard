/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.pard.model.simple

import de.creatorcat.kotiya.matchers.common.hasElementsEqual
import de.creatorcat.kotiya.matchers.common.isEmpty
import de.creatorcat.kotiya.test.core.assertThat
import kotlin.test.Test

class SimpleModelTest {

    @Test fun `getArtifactsById - in document`() {
        val model = SimpleRequirementsModel.build {
            addDocument("d", "dt", "Doc 1") {
                addArtifact("a1", "at", "Req 1")
                addArtifact("a2", "at", "Req 2")
                addArtifact("a3", "at", "Req 3")
                addArtifact("a1", "at", "Req 1a")
            }
        }
        val document = model.documents[0]
        val artifacts = document.artifacts

        assertThat(document.getArtifactsById("a4"), isEmpty)
        assertThat(document.getArtifactsById("a1"), hasElementsEqual(artifacts[0], artifacts[3]))
        assertThat(document.getArtifactsById("a2"), hasElementsEqual(artifacts[1]))
    }

    @Test fun `getArtifactsById - in model`() {
        val model = SimpleRequirementsModel.build {
            addDocument("d1", "dt", "Doc 1") {
                addArtifact("a1", "at", "Req 1")
                addArtifact("a2", "at", "Req 2")
                addArtifact("a3", "at", "Req 3")
                addArtifact("a1", "at", "Req 1a")
            }
            addDocument("d2", "dt", "Doc 2") {
                addArtifact("a1", "at", "Req 1b")
                addArtifact("a2", "at", "Req 2a")
                addArtifact("a4", "at", "Req 4")
            }
        }
        val a1 = model.documents[0].artifacts
        val a2 = model.documents[1].artifacts

        assertThat(model.getArtifactsById("a5"), isEmpty)
        assertThat(model.getArtifactsById("a1"), hasElementsEqual(a1[0], a1[3], a2[0]))
        assertThat(model.getArtifactsById("a2"), hasElementsEqual(a1[1], a2[1]))
        assertThat(model.getArtifactsById("a4"), hasElementsEqual(a2[2]))
    }
}
