/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.pard.model.simple

import de.creatorcat.kotiya.matchers.common.isEmpty
import de.creatorcat.kotiya.matchers.common.isEqual
import de.creatorcat.kotiya.test.core.assertThat
import de.creatorcat.kotiya.test.core.stubbing.mustNotBeCalled
import kotlin.test.Test

class SimpleModelIdValidatorTest {

    private val validator = SimpleModelIdValidator()

    @Test fun `validateIds - all unique`() {
        val faults = validator.validateIdsAreUnique(
            (0..9).map { req(it.toString()) }
        )
        assertThat(faults, isEmpty)
    }

    @Test fun `validateIds - 2 x 2 faults`() {
        val r2 = Artifact("2", "Req", "Other")
        val r4 = Artifact("4", "Req", "Other")
        val faults = validator.validateIdsAreUnique(
            reqsOf("1", "2", "3", "4") + listOf(r2, r4)
        )
        assertThat(faults["2"], isEqual(listOf(req("2"), r2)))
        assertThat(faults["4"], isEqual(listOf(req("4"), r4)))
    }

    @Test fun `validateIds - 1 x 3 faults`() {
        val r2a = Artifact("2", "Req", "Other")
        val r2b = Artifact("2", "Req", "Another")
        val faults = validator.validateIdsAreUnique(
            reqsOf("1", "2", "3", "4") + listOf(r2a, r2b)
        )
        assertThat(faults["2"], isEqual(listOf(req("2"), r2a, r2b)))
    }

    @Test fun `validate documents`() {
        val validModel = SimpleRequirementsModel.build {
            addDocument("1", "t", "t") {}
            addDocument("2", "t", "t") {}
            build()
        }
        assertThat(validator.validateDocumentIds(validModel), isEmpty)

        val invalidModel = SimpleRequirementsModel.build {
            addDocument("1", "t", "t") {}
            addDocument("2", "t", "t") {}
            addDocument("1", "t", "t") {}
            build()
        }
        assertThat(
            validator.validateDocumentIds(invalidModel)["1"],
            isEqual(listOf(invalidModel.documents[0], invalidModel.documents[2]))
        )
    }

    @Test fun `validate artifacts`() {
        val validModel = SimpleRequirementsModel.build {
            addDocument("1", "t", "t") {
                addArtifact("1", "t", "t")
                addArtifact("2", "t", "t")
                addArtifact("3", "t", "t")
            }
            addDocument("2", "t", "t") {
                addArtifact("4", "t", "t")
                addArtifact("5", "t", "t")
                addArtifact("6", "t", "t")
            }
            build()
        }
        assertThat(validator.validateArtifactIds(validModel), isEmpty)

        val invalidModel = SimpleRequirementsModel.build {
            addDocument("1", "t", "t") {
                addArtifact("1", "t", "t")
                addArtifact("2", "t", "t")
                addArtifact("3", "t", "t")
            }
            addDocument("2", "t", "t") {
                addArtifact("4", "t", "t")
                addArtifact("2", "t", "t")
                addArtifact("6", "t", "t")
            }
            build()
        }
        assertThat(
            validator.validateArtifactIds(invalidModel)["2"],
            isEqual(listOf(invalidModel.documents[0].artifacts[1], invalidModel.documents[1].artifacts[1]))
        )
    }

    private fun reqsOf(vararg ids: String) = ids.map(::req)
    private fun req(id: String) = Artifact(id, "ReqType", "title")

    private data class Artifact(
        override val id: String, override val title: String, override val type: String
    ) : SimpleRequirementsArtifact {
        override val document get() = mustNotBeCalled()
    }
}
