/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.pard.model.code

import de.creatorcat.kotiya.core.text.appendln
import de.creatorcat.kotiya.test.core.assertThrows
import kotlin.test.Test
import kotlin.test.assertEquals

class CodeToArtifactLinkDataSerializerTest {

    private val serializer = CodeToArtifactLinkDataSerializer()

    @Test fun `serializer - write`() {
        assertEquals(sampleStandardAsString, sampleStandardAsList.writeString())
    }

    @Test fun `serializer - read`() {
        assertEquals(sampleStandardAsList, serializer.read(sampleStandardAsString))
        assertEquals(sampleStandardAsList, serializer.read(sampleStandardAsStringWithComments))
    }

    @Test fun `serializer - read invalid data`() {
        assertThrows<IllegalArgumentException> { serializer.read("{}") }
        assertThrows<IllegalArgumentException> {
            serializer.read("""{"bla": "blub"}""")
        }
        assertThrows<IllegalArgumentException> {
            serializer.read(
                """{"artifact": "a1", "comment": "", """ +
                    """"code.X": "n1", "code.type": "t1", "code.scope": "s1"}"""
            )
        }
    }

    private fun List<CodeToArtifactLink>.writeString(): String {
        val buffer = StringBuilder()
        serializer.write(buffer, this)
        return buffer.toString()
    }

    companion object {
        private val sampleStandardAsList = listOf(
            linkOf("a1", "t1", "n1", "s1"),
            linkOf("a1", "t1", "n2", "s1", "comment1"),
            linkOf("a3", "t1", "n3", "s2"),
            linkOf("a3", "t2", "nx\"4\"", "s2\nmore"),
            linkOf("a3", "t3", "n5", "s3", "comment2"),
            linkOf("a\t\"4\"", "t3", "n6", "s3")
        )

        private val sampleStandardAsString = StringBuilder()
            .appendln(
                """{"artifact": "a1", "comment": "", """ +
                    """"code.name": "n1", "code.type": "t1", "code.scope": "s1"}"""
            )
            .appendln(
                """{"artifact": "a1", "comment": "comment1", """ +
                    """"code.name": "n2", "code.type": "t1", "code.scope": "s1"}"""
            )
            .appendln(
                """{"artifact": "a3", "comment": "", """ +
                    """"code.name": "n3", "code.type": "t1", "code.scope": "s2"}"""
            )
            .appendln(
                """{"artifact": "a3", "comment": "", """ +
                    """"code.name": "nx\"4\"", "code.type": "t2", "code.scope": "s2\nmore"}"""
            )
            .appendln(
                """{"artifact": "a3", "comment": "comment2", """ +
                    """"code.name": "n5", "code.type": "t3", "code.scope": "s3"}"""
            )
            .appendln(
                """{"artifact": "a\t\"4\"", "comment": "", """ +
                    """"code.name": "n6", "code.type": "t3", "code.scope": "s3"}"""
            )
            .toString()

        private val sampleStandardAsStringWithComments = StringBuilder()
            .appendln("Some heading")
            .appendln(
                """{"artifact": "a1", "comment": "", """ +
                    """"code.name": "n1", "code.type": "t1", "code.scope": "s1"}"""
            )
            .appendln(
                """{"artifact": "a1", "comment": "comment1", """ +
                    """"code.name": "n2", "code.type": "t1", "code.scope": "s1"}"""
            )
            .appendln(
                """{"artifact": "a3", "comment": "", """ +
                    """"code.name": "n3", "code.type": "t1", "code.scope": "s2"}"""
            )
            .appendln("# some comment")
            .appendln(
                """{"artifact": "a3", "comment": "", """ +
                    """"code.name": "nx\"4\"", "code.type": "t2", "code.scope": "s2\nmore"}"""
            )
            .appendln(
                """{"artifact": "a3", "comment": "comment2", """ +
                    """"code.name": "n5", INSIDE COMMENT "code.type": "t3", "code.scope": "s3"}"""
            )
            .appendln(
                """{"artifact": "a\t\"4\"", "comment": "", """ +
                    """"code.name": "n6", "code.type": "t3", "code.scope": "s3"}"""
            )
            .toString()

        private fun linkOf(
            artifactId: String,
            codeType: String, codeName: String, codeScope: String,
            comment: String = ""
        ): CodeToArtifactLink =
            StandardCodeToArtifactLink(
                artifactId,
                SimpleCodeElement(codeType, codeName, codeScope),
                comment
            )
    }
}
