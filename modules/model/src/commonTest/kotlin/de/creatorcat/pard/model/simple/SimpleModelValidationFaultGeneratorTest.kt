/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.pard.model.simple

import de.creatorcat.pard.model.code.SimpleCodeElement
import kotlin.test.Test
import kotlin.test.assertEquals

class SimpleModelValidationFaultGeneratorTest {

    private var testee = SimpleModelValidationFaultGenerator()

    @Test fun `generate document id fault`() {
        val model = SimpleRequirementsModel.build {
            addDocument("d1", "Spec", "Spec 1") {}
            addDocument("d2", "Spec", "Spec 2") {}
            addDocument("d1", "Spec", "Spec 1 duplicate") {}
        }
        val faultData = "d1" to listOf(model.documents[0], model.documents[2])
        val faultMsg = testee.generateDocumentIdFault(faultData)
        assertEquals(
            """duplicate document id=d1 in: Spec("Spec 1"), Spec("Spec 1 duplicate")""",
            faultMsg
        )
    }

    @Test fun `generate artifact id fault`() {
        val model = SimpleRequirementsModel.build {
            addDocument("d1", "Spec", "Spec 1") {
                addArtifact("a1", "Req", "Req 1")
                addArtifact("a2", "Req", "Req 2-1")
            }
            addDocument("d2", "Spec", "Spec 2") {
                addArtifact("a2", "Req", "Req 2-2")
                addArtifact("a3", "Req", "Req 3")
                addArtifact("a2", "Req", "Req 2-3")
            }
        }
        val faultData = "a2" to listOf(
            model.documents[0].artifacts[1],
            model.documents[1].artifacts[0],
            model.documents[1].artifacts[2]
        )
        val faultMsg = testee.generateArtifactIdFault(faultData)
        assertEquals(
            "duplicate artifact id=a2 in: " +
                """Req("Req 2-1") in document #d1, Req("Req 2-2") in document #d2, Req("Req 2-3") in document #d2""",
            faultMsg
        )
    }

    @Test fun `generate artifact uncovered fault`() {
        val model = SimpleRequirementsModel.build {
            addDocument("d", "dt", "doc") {
                addArtifact("a", "at", "req")
            }
        }
        val artifact = model.documents[0].artifacts[0]
        assertEquals(
            "artifact is not covered: at(#a - \"req\") in document #d",
            testee.generateArtifactUncoveredFault(artifact)
        )

        testee = SimpleModelValidationFaultGenerator("implemented")
        assertEquals(
            "artifact is not implemented: at(#a - \"req\") in document #d",
            testee.generateArtifactUncoveredFault(artifact)
        )
    }

    @Test fun `generate missing link target fault`() {
        val codeElement = SimpleCodeElement("class", "Dummy", "package")
        assertEquals(
            "missing target artifact id=a1 in code-to-artifact-link on: class(Dummy) in package",
            testee.generateMissingCodeLinkTargetFault("a1", codeElement)
        )

        testee = SimpleModelValidationFaultGenerator(codeLinkName = "@CodeLink")
        assertEquals(
            "missing target artifact id=a1 in @CodeLink on: class(Dummy) in package",
            testee.generateMissingCodeLinkTargetFault("a1", codeElement)
        )
    }
}
