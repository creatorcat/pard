/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.pard.model.code

import de.creatorcat.kotiya.test.core.stubbing.mustNotBeCalled
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertSame

class CodeToArtifactLinkingTest {

    @Test fun `standard link implementation`() {
        val code = SimpleCodeElement("t", "n", "s")
        with(StandardCodeToArtifactLink("a", code, "c")) {
            assertEquals("a", artifactId)
            assertSame(code, codeElement)
            assertEquals("c", comment)
        }
    }

    @Test fun `provider - default values`() {
        val dummy = DummyProvider()
        assertEquals("covered", dummy.coverageDescribingVerb)
        assertEquals("code-to-artifact-link", dummy.codeToArtifactLinkName)
    }

    private class DummyProvider : CodeToArtifactLinkDataProvider {
        override val data get() = mustNotBeCalled()
    }
}
