/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.pard.model.simple

import de.creatorcat.kotiya.core.structures.PropertyBasedDataDefinition

/**
 * Builder to create immutable [SimpleRequirementsModel] objects.
 * The model content is defined by [documentBuilders].
 *
 * @see SimpleRequirementsModel.Companion.build
 */
class SimpleRequirementsModelBuilder {

    /**
     * List of builders that will be applied to create the
     * [models documents][SimpleRequirementsModel.documents] on [build] call.
     * May be manipulated at will.
     */
    val documentBuilders: MutableList<SimpleRequirementsDocumentBuilder> = mutableListOf()

    /**
     * Add a [document builder][SimpleRequirementsDocumentBuilder]
     * with the specified properties to the [list][documentBuilders].
     *
     * @param artifactConfig code to configure the artifacts contained in the created builder
     */
    fun addDocument(
        id: String, type: String, title: String,
        artifactConfig: SimpleRequirementsDocumentBuilder.() -> Unit
    ): SimpleRequirementsDocumentBuilder {
        val builder = SimpleRequirementsDocumentBuilder(id, type, title)
        documentBuilders.add(builder)
        builder.artifactConfig()
        return builder
    }

    /**
     * Creates an immutable model based on the current [documentBuilders].
     */
    fun build(): SimpleRequirementsModel = SimpleReqModelImplementation(documentBuilders)
}

/**
 * Convenience method to create a model via a [builder][SimpleRequirementsModelBuilder]
 * after applying the [contentConfiguration].
 */
fun SimpleRequirementsModel.Companion.build(
    contentConfiguration: SimpleRequirementsModelBuilder.() -> Unit
): SimpleRequirementsModel =
    with(SimpleRequirementsModelBuilder()) {
        contentConfiguration()
        build()
    }

/**
 * Builder to define the [documents][SimpleRequirementsDocument] of a model
 * that is to be build by a [SimpleRequirementsModelBuilder].
 *
 * The document content is defined by [artifactSpecs].
 *
 * @param id value of the [SimpleRequirementsDocument.id]
 * @param type value of the [SimpleRequirementsDocument.type]
 * @param title value of the [SimpleRequirementsDocument.title]
 */
class SimpleRequirementsDocumentBuilder(
    var id: String, var type: String, var title: String
) {
    /**
     * Specification of the [artifacts][SimpleRequirementsArtifact] to be build.
     * May be manipulated at will.
     */
    val artifactSpecs: MutableList<SimpleRequirementsArtifactSpec> = mutableListOf()

    /**
     * Adds an [artifact specification][SimpleRequirementsArtifactSpec] with given properties
     * to the [list][artifactSpecs].
     */
    fun addArtifact(id: String, type: String, title: String) {
        artifactSpecs.add(SimpleRequirementsArtifactSpec(id, type, title))
    }
}

/**
 * Specification of an [SimpleRequirementsArtifact] to be build via a [SimpleRequirementsDocumentBuilder].
 *
 * @param id value of the [SimpleRequirementsArtifact.id]
 * @param type value of the [SimpleRequirementsArtifact.type]
 * @param title value of the [SimpleRequirementsArtifact.title]
 */
class SimpleRequirementsArtifactSpec(
    var id: String, var type: String, var title: String
)

internal class SimpleReqModelImplementation(
    documentBuilders: List<SimpleRequirementsDocumentBuilder>
) : SimpleRequirementsModel {
    override val documents: List<SimpleRequirementsDocument> =
        documentBuilders.map { SimpleReqDocumentImplementation(this, it) }

    override fun equals(other: Any?): Boolean = eqChecker.computeEquals(this, other)
    override fun hashCode(): Int = eqChecker.computeHashCode(this)
    override fun toString(): String = eqChecker.generateString(this)

    companion object {
        private val eqChecker = PropertyBasedDataDefinition.of(
            SimpleReqModelImplementation::documents
        )
    }
}

internal class SimpleReqDocumentImplementation(
    override val model: SimpleRequirementsModel,
    documentBuilder: SimpleRequirementsDocumentBuilder
) : SimpleRequirementsDocument {

    override val id: String = documentBuilder.id
    override val type: String = documentBuilder.type
    override val title: String = documentBuilder.title
    override val artifacts: List<SimpleRequirementsArtifact> =
        documentBuilder.artifactSpecs.map {
            SimpleReqArtifactImplementation(this, it.id, it.type, it.title)
        }

    override fun equals(other: Any?): Boolean = eqChecker.computeEquals(this, other)
    override fun hashCode(): Int = eqChecker.computeHashCode(this)
    override fun toString(): String = eqChecker.generateString(this)

    companion object {
        private val eqChecker = PropertyBasedDataDefinition.of(
            SimpleReqDocumentImplementation::id,
            SimpleReqDocumentImplementation::type,
            SimpleReqDocumentImplementation::title,
            SimpleReqDocumentImplementation::artifacts
        )
    }
}

internal class SimpleReqArtifactImplementation(
    override val document: SimpleRequirementsDocument,
    override val id: String,
    override val type: String,
    override val title: String
) : SimpleRequirementsArtifact {

    override fun equals(other: Any?): Boolean = eqChecker.computeEquals(this, other)
    override fun hashCode(): Int = eqChecker.computeHashCode(this)
    override fun toString(): String = eqChecker.generateString(this)

    companion object {
        private val eqChecker = PropertyBasedDataDefinition.of(
            SimpleReqArtifactImplementation::id,
            SimpleReqArtifactImplementation::type,
            SimpleReqArtifactImplementation::title
        )
    }
}
