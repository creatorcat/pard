/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.pard.model.simple

import de.creatorcat.kotiya.core.text.appendln

/**
 * Facility to print a [SimpleRequirementsModel] as human readable hierarchical text.
 * All [print] methods append new lines to the associated [appendable].
 *
 * @param appendable The appendable that receives all printed text
 */
class SimpleModelPrinter(
    val appendable: Appendable
) {

    /** Prints the whole [model] hierarchical structured into documents and artifacts. */
    fun print(model: SimpleRequirementsModel) {
        appendLine("", "Simple requirements model with ${model.documents.size} documents:")
        for (document in model.documents)
            printDocument(document, MODEL_LEVEL_INDENT)
    }

    /** Prints one [document] with all its artifacts. */
    fun print(document: SimpleRequirementsDocument) {
        printDocument(document, "")
    }

    private fun printDocument(document: SimpleRequirementsDocument, indentation: String) {
        with(document) {
            appendLine(indentation, "Document #$id of type $type with ${artifacts.size} artifacts,")
            appendLine(indentation, "title = $title:")
            for (artifact in artifacts)
                printArtifact(artifact, "$MODEL_LEVEL_INDENT$indentation")
        }
    }

    /** Prints a single [artifact]. */
    fun print(artifact: SimpleRequirementsArtifact) {
        printArtifact(artifact, "")
    }

    private fun printArtifact(artifact: SimpleRequirementsArtifact, indentation: String) {
        with(artifact) {
            appendLine(indentation, "$type #$id - $title")
        }
    }

    private fun appendLine(indentation: String, csq: CharSequence) =
        appendable.appendln("$indentation$csq")
}

/** Indentation to visualize model hierarchy levels between model - document - artifact */
internal const val MODEL_LEVEL_INDENT = "  "
