/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.pard.model.simple

/**
 * Root object of a simple requirements model.
 * The model is based on [documents] that each contain [artifacts][SimpleRequirementsDocument.artifacts].
 * Both have only a few basic properties.
 *
 * Use the [model builder][SimpleRequirementsModel.Companion.build] to create objects.
 */
interface SimpleRequirementsModel {

    /** List of contained documents. */
    val documents: List<SimpleRequirementsDocument>

    /**
     * A simple model is equal to [other] is it is another simple model
     * with an equal list of [documents].
     */
    override fun equals(other: Any?): Boolean

    /** Provides static creation methods. */
    companion object
}

/**
 * Find the artifacts with given [id] in all [artifacts][SimpleRequirementsDocument.artifacts]
 * of the contained [documents][SimpleRequirementsModel.documents].
 *
 * @return all found artifacts with given [id], maybe empty
 */
fun SimpleRequirementsModel.getArtifactsById(id: String): List<SimpleRequirementsArtifact> {
    val result = mutableListOf<SimpleRequirementsArtifact>()
    for (doc in documents) {
        result.addAll(doc.getArtifactsById(id))
    }
    return result
}

/**
 * Representation of any requirements document in a [SimpleRequirementsModel].
 *
 * Use the [model builder][SimpleRequirementsModel.Companion.build] to create objects.
 */
interface SimpleRequirementsDocument : TypedIdentifiableRequirementsObject {

    /** The parent model */
    val model: SimpleRequirementsModel

    /** List of contained artifacts. */
    val artifacts: List<SimpleRequirementsArtifact>

    /**
     * Find the artifacts with given [id] in the list of [artifacts].
     *
     * @return all found artifacts with given [id], maybe empty
     */
    fun getArtifactsById(id: String): List<SimpleRequirementsArtifact> =
        artifacts.filter { it.id == id }

    /**
     * A simple document is equal to [other] if it is another document
     * with equal properties ([id], [title] and [type])
     * and an equal list of [artifacts].
     */
    override fun equals(other: Any?): Boolean
}

/**
 * Representation of any requirements artifact in a [SimpleRequirementsModel].
 *
 * Use the [model builder][SimpleRequirementsModel.Companion.build] to create objects.
 */
interface SimpleRequirementsArtifact : TypedIdentifiableRequirementsObject {

    /** The parent document */
    val document: SimpleRequirementsDocument

    /**
     * A simple artifact is equal to [other] if it is another artifact
     * with equal properties ([id], [title] and [type]).
     */
    override fun equals(other: Any?): Boolean
}

/**
 * Basic interface for [SimpleRequirementsModel] objects that have a [type]
 * and can by identified by an [id]. They also have a [title].
 */
interface TypedIdentifiableRequirementsObject {
    /** Identifier used to uniquely reference this object. */
    val id: String
    /** Title text. */
    val title: String
    /** Type used as grouping information of this object. */
    val type: String
}
