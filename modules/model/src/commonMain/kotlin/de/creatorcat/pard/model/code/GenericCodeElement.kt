/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.pard.model.code

/**
 * Basic representation of a source code element.
 * To achieve maximum flexibility, all properties are just strings.
 * This way there is no constraint on the available code element [type]s
 * and the interpretation of [name] and [scope] is free to the implementor.
 *
 * A canonical implementation is given through [SimpleCodeElement].
 */
interface GenericCodeElement {

    /**
     * The type of the code element.
     *
     * Code elements represent different language constructs e.g. classes, functions, variables or function arguments.
     * The type should clearly identify this kind of construct.
     * It is meant to be used for filtering code elements to e.g. define and validate that only functions
     * are allowed to carry a *verifies* link to a a functional requirement.
     */
    val type: String

    /**
     * Name of the code element.
     *
     * Together with the [scope], the name is meant to make the element uniquely identifiable
     * to the human developer, e.g. if denoted in a warning message.
     * This may be the name of a class, function or variable as defined in the source file.
     */
    val name: String

    /**
     * Scope of the code element.
     *
     * Together with the [name], the scope is meant to make the element uniquely identifiable
     * to the human developer, e.g. if denoted in a warning message.
     * This may be the fully qualified name of the enclosing element of the element, e.g. a package or class.
     */
    val scope: String
}
