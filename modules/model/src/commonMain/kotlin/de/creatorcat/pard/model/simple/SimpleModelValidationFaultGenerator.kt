/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.pard.model.simple

import de.creatorcat.pard.model.code.GenericCodeElement

/**
 * Generator for human readable messages for validation faults produced when validating
 * a [SimpleRequirementsModel].
 *
 * @see SimpleModelIdValidator
 * @see SimpleModelForwardCoverageValidator
 *
 * @param coverageDescribingVerb A verb (in simple past form) to be used in [generateArtifactUncoveredFault]
 *   instead of `"covered"`. For example, instead of "artifact is not covered" a more precise message might be
 *   "artifact is not implemented".
 *   Defaults to `"covered"`.
 * @param codeLinkName This name is used in [generateMissingCodeLinkTargetFault] to denote the
 *   code link object that has a missing link target. If the code link is an annotation,
 *   this should be the actual name of the annotation so the developer will directly recognize it.
 *   Defaults to `"code-to-artifact-link"`.
 */
class SimpleModelValidationFaultGenerator(
    val coverageDescribingVerb: String = "covered",
    val codeLinkName: String = "code-to-artifact-link"
) {

    /**
     * Generate a fault message for a [faultEntry] produced by [SimpleModelIdValidator.validateDocumentIds].
     */
    fun generateDocumentIdFault(faultEntry: Pair<String, List<SimpleRequirementsDocument>>): String =
        "duplicate document id=${faultEntry.first} in: " +
            faultEntry.second.joinToString { it.generateDocInfoWithoutId() }

    private fun SimpleRequirementsDocument.generateDocInfoWithoutId() =
        "${type}(\"${title}\")"

    /**
     * Generate a fault message for a [faultEntry] produced by [SimpleModelIdValidator.validateArtifactIds].
     */
    fun generateArtifactIdFault(faultEntry: Pair<String, List<SimpleRequirementsArtifact>>): String =
        "duplicate artifact id=${faultEntry.first} in: " +
            faultEntry.second.joinToString { it.generateArtifactInfoWithoutId() }

    private fun SimpleRequirementsArtifact.generateArtifactInfoWithoutId() =
        "${type}(\"${title}\") in document #${document.id}"

    /**
     * Generate a fault message for an uncovered artifact produced by [SimpleModelForwardCoverageValidator.validate].
     * Uses [coverageDescribingVerb].
     */
    fun generateArtifactUncoveredFault(artifact: SimpleRequirementsArtifact): String =
        "artifact is not $coverageDescribingVerb: ${artifact.generateArtifactInfo()}"

    private fun SimpleRequirementsArtifact.generateArtifactInfo() =
        "${type}(#${id} - \"${title}\") in document #${document.id}"

    /**
     * Generate a fault message for a missing code link target
     * produced by [SimpleModelForwardCoverageValidator.validate].
     * Uses [codeLinkName].
     */
    fun generateMissingCodeLinkTargetFault(artifactId: String, codeElement: GenericCodeElement): String =
        "missing target artifact id=$artifactId in $codeLinkName on: ${codeElement.generateCodeElementInfo()}"

    private fun GenericCodeElement.generateCodeElementInfo() =
        "$type($name) in $scope"
}
