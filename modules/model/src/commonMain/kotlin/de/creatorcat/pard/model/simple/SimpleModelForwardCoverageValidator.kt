/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.pard.model.simple

import de.creatorcat.kotiya.core.predicates.Predicate
import de.creatorcat.pard.model.code.CodeToArtifactLink

/**
 * Validator for requirement coverage in forward direction: code *covers* all requirement artifacts
 * of a [SimpleRequirementsModel].
 *
 * @param artifactFilter filter to select the artifacts that shall be considered when validating coverage
 *   and link target existence; defaults to the trivial filter `{ true }`
 */
class SimpleModelForwardCoverageValidator(
    val artifactFilter: Predicate<SimpleRequirementsArtifact> = { true }
) {

    /**
     * Validates the forward coverage of the given [model] based on the [linkData]
     * that tells which artifacts are covered by code elements.
     *
     * The validation has 2 aspects:
     * * Artifact coverage: do all artifacts have respectively linking code?
     * * Link target existence: do all links point to existing artifacts?
     *
     * The [validation result][ForwardCovaResult] holds all answers to these questions.
     */
    fun validate(model: SimpleRequirementsModel, linkData: List<CodeToArtifactLink>): ForwardCovaResult {
        val missingTargetLinkData = mutableListOf<CodeToArtifactLink>()
        val uncoveredArtifacts = mutableListOf<SimpleRequirementsArtifact>()
        val coveredArtifactLinkData = mutableMapOf<SimpleRequirementsArtifact, MutableList<CodeToArtifactLink>>()

        for (document in model.documents) uncoveredArtifacts.addAll(document.artifacts.filter(artifactFilter))

        for (link in linkData) {
            val linkedArtifacts = model.getArtifactsById(link.artifactId).filter(artifactFilter)
            if (linkedArtifacts.isEmpty())
                missingTargetLinkData.add(link)
            else
                uncoveredArtifacts.removeAll(linkedArtifacts)
            for (artifact in linkedArtifacts)
                coveredArtifactLinkData.getOrPut(artifact) { mutableListOf() }
                    .add(link)
        }

        return ForwardCovaResult(missingTargetLinkData, uncoveredArtifacts, coveredArtifactLinkData)
    }
}

/**
 * Result of a forward requirement coverage validation.
 *
 * @see SimpleModelForwardCoverageValidator
 */
data class ForwardCovaResult(
    /**
     * List of code-to-artifact links with invalid [artifact identifier][CodeToArtifactLink.artifactId],
     * meaning there exists no artifact with this id in the validated model.
     */
    val missingTargetLinkData: List<CodeToArtifactLink>,
    /**
     * List of artifacts in the validated [model][SimpleRequirementsModel] that are not *covered*,
     * meaning there exist no code-to-artifact links with respective
     * [artifact identifiers][SimpleRequirementsArtifact.id] in the [link data][CodeToArtifactLink]
     * used in the validation.
     */
    val uncoveredArtifacts: List<SimpleRequirementsArtifact>,
    /**
     * A map from artifacts to a list of [CodeToArtifactLink]s that link to the respective artifact.
     * These artifacts have been verified as *covered* in the validation.
     */
    val coveredArtifactLinkData: Map<SimpleRequirementsArtifact, List<CodeToArtifactLink>>
) 
