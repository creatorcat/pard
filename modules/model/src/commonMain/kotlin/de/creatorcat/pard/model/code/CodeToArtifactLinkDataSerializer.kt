/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.pard.model.code

import de.creatorcat.kotiya.core.text.appendln

/**
 * This class enables serialization of [CodeToArtifactLink]s in string format.
 * Data can be [written][write] and [read].
 */
class CodeToArtifactLinkDataSerializer {

    private val serializer = JsonStyleTextSerializer()

    /**
     * Appends the given [linkData] to the [destination].
     *
     * Each link is written in JSON object style "{...}".
     */
    fun write(destination: Appendable, linkData: List<CodeToArtifactLink>) {
        for (link in linkData) {
            destination.writeLinkData(link)
        }
    }

    /**
     * Reads link data from the given [source].
     *
     * The reader is very tolerant. It ignores any lines that do not contain a valid JSON object pattern "{...}".
     * It will only throw an exception if it finds an object that does not contain proper link data.
     */
    fun read(source: String): List<CodeToArtifactLink> {
        val lines = source.lines()
        val result = mutableListOf<CodeToArtifactLink>()
        for (currentLine in 0 until lines.size) {
            lines.readLinkData(currentLine)?.let { result.add(it) }
        }
        return result
    }

    private fun List<String>.readLinkData(currentLine: Int): CodeToArtifactLink? =
        serializer.readMap(this[currentLine])?.asLink(currentLine)

    private fun Appendable.writeLinkData(link: CodeToArtifactLink) {
        serializer.writeMap(this, link.asStringMap())
        appendln()
    }

    private fun CodeToArtifactLink.asStringMap() =
        mapOf(
            ARTIFACT_ID_TAG to artifactId, COMMENT_TAG to comment,
            CODE_NAME_TAG to codeElement.name, CODE_TYPE_TAG to codeElement.type, CODE_SCOPE_TAG to codeElement.scope
        )

    private fun Map<String, String>.asLink(currentLine: Int) =
        StandardCodeToArtifactLink(
            artifactId = getTag(ARTIFACT_ID_TAG, currentLine),
            comment = getTag(COMMENT_TAG, currentLine),
            codeElement = SimpleCodeElement(
                name = getTag(CODE_NAME_TAG, currentLine),
                type = getTag(CODE_TYPE_TAG, currentLine),
                scope = getTag(CODE_SCOPE_TAG, currentLine)
            )
        )

    private fun Map<String, String>.getTag(tag: String, currentLine: Int): String =
        get(tag) ?: throw IllegalArgumentException("missing link data property $tag in line $currentLine")

    companion object {
        /** JSON object property tag for writing [CodeToArtifactLink.artifactId] */
        const val ARTIFACT_ID_TAG: String = "artifact"
        /** JSON object property tag for writing [CodeToArtifactLink.comment] */
        const val COMMENT_TAG: String = "comment"
        /** JSON object property tag for writing [link.codeElement.name][CodeToArtifactLink.codeElement] */
        const val CODE_NAME_TAG: String = "code.name"
        /** JSON object property tag for writing [link.codeElement.type][CodeToArtifactLink.codeElement] */
        const val CODE_TYPE_TAG: String = "code.type"
        /** JSON object property tag for writing [link.codeElement.scope][CodeToArtifactLink.codeElement] */
        const val CODE_SCOPE_TAG: String = "code.scope"
    }
}
