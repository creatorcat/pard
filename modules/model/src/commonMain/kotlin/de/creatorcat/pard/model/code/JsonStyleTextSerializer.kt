/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

// TODO documentation
// TODO unit tests
// TODO check usage & visibility
// suppress some warnings while the file is under first development
// cause the corresponding findings are inevitable in the beginning and do no harm
@file:Suppress("KDocMissingDocumentation", "unused", "MemberVisibilityCanBePrivate")

package de.creatorcat.pard.model.code

// TODO move to kotiya
open class JsonStyleTextSerializer {

    open fun stringify(value: Any?): String = value.toString()

    fun writeSingleValue(destination: Appendable, singleValue: Any?) {
        destination.append(STRING_VALUE_WRAP_CHAR)
        destination.append(convertToJsonStringContent(stringify(singleValue)))
        destination.append(STRING_VALUE_WRAP_CHAR)
    }

    fun readSingleValue(source: String): String? {
        val firstValueMatch = singleValueRegex.find(source) ?: return null
        val jsonContent = firstValueMatch.getMandatoryGroupValue(1)
        return convertToText(jsonContent)
    }

    fun writeList(destination: Appendable, valueList: List<*>) {
        destination.append(LIST_START_CHAR)
        for ((idx, value) in valueList.withIndex()) {
            if (idx > 0) destination.append(PADDED_ENTRY_SEPARATOR_CHAR)
            writeSingleValue(destination, value)
        }
        destination.append(LIST_END_CHAR)
    }

    fun readList(source: String): List<String>? {
        val firstListMatch = listRegex.find(source) ?: return null
        val listContent = firstListMatch.getMandatoryGroupValue(1)
        return singleValueRegex.findAll(listContent).map { match ->
            val jsonContent = match.getMandatoryGroupValue(1)
            convertToText(jsonContent)
        }.toList()
    }

    fun writeMap(destination: Appendable, valueMap: Map<*, *>) {
        destination.append(MAP_START_CHAR)
        var isFirst = true
        for ((key, value) in valueMap) {
            if (isFirst)
                isFirst = false
            else
                destination.append(PADDED_ENTRY_SEPARATOR_CHAR)
            writeSingleValue(destination, key)
            destination.append(PADDED_MAP_ASSOCIATION_CHAR)
            writeSingleValue(destination, value)
        }
        destination.append(MAP_END_CHAR)
    }

    fun readMap(source: String): Map<String, String>? {
        val firstMapMatch = mapRegex.find(source) ?: return null
        val mapContent = firstMapMatch.getMandatoryGroupValue(1)
        return mapEntryRegex.findAll(mapContent).associate { match ->
            val key = match.getMandatoryGroupValue(1)
            val value = match.getMandatoryGroupValue(2)
            Pair(convertToText(key), convertToText(value))
        }
    }

    companion object {

        const val STRING_VALUE_WRAP_CHAR: String = "\""
        const val LIST_START_CHAR: String = "["
        const val LIST_END_CHAR: String = "]"
        private const val LIST_START_CHAR_IN_REGEX = "\\["
        private const val LIST_END_CHAR_IN_REGEX = "\\]"
        const val MAP_START_CHAR: String = "{"
        const val MAP_END_CHAR: String = "}"
        private const val MAP_START_CHAR_IN_REGEX = "\\{"
        private const val MAP_END_CHAR_IN_REGEX = "\\}"
        const val MAP_ASSOCIATION_CHAR: String = ":"
        const val ENTRY_SEPARATOR_CHAR: String = ","
        private const val PADDED_MAP_ASSOCIATION_CHAR = "$MAP_ASSOCIATION_CHAR "
        private const val PADDED_ENTRY_SEPARATOR_CHAR = "$ENTRY_SEPARATOR_CHAR "

        private val textToJsonStringRegex = Regex("[\"\\\\/\b\\r\\n\\t\\f]")
        private val jsonStringToTextRegex = Regex("\\\\(.)")

        private const val singleValuePattern =
            "$STRING_VALUE_WRAP_CHAR([^\\\\\"]*(?:\\\\.[^\\\\\"]*)*)$STRING_VALUE_WRAP_CHAR"
        private val singleValueRegex = Regex(singleValuePattern)
        private val mapEntryRegex = Regex("$singleValuePattern\\s*$MAP_ASSOCIATION_CHAR\\s*$singleValuePattern")
        private val listRegex = Regex("$LIST_START_CHAR_IN_REGEX([^$LIST_END_CHAR_IN_REGEX]*)$LIST_END_CHAR_IN_REGEX")
        private val mapRegex = Regex("$MAP_START_CHAR_IN_REGEX([^$MAP_END_CHAR_IN_REGEX]*)$MAP_END_CHAR_IN_REGEX")

        fun convertToJsonStringContent(text: CharSequence): String =
            textToJsonStringRegex.replace(text) { match ->
                when (match.getMandatoryGroupValue(0)) {
                    "\"" -> "\\\""
                    "\\" -> "\\\\"
                    "/" -> "\\/"
                    "\b" -> "\\b"
                    "\r" -> "\\r"
                    "\n" -> "\\n"
                    "\t" -> "\\t"
                    "\u000C" -> "\\f"
                    else -> throwOnUnexpectedMatch(match)
                }
            }

        fun convertToText(jsonStringContent: String): String =
            jsonStringToTextRegex.replace(jsonStringContent) { match ->
                when (val escapedChar = match.getMandatoryGroupValue(1)) {
                    "b" -> "\b"
                    "r" -> "\r"
                    "n" -> "\n"
                    "t" -> "\t"
                    "f" -> "\u000C"
                    else -> escapedChar
                }
            }

        private fun MatchResult.getMandatoryGroupValue(grpIdx: Int): String =
            groups[grpIdx]?.value ?: throwOnUnexpectedMatch(this)

        private fun throwOnUnexpectedMatch(match: MatchResult): Nothing =
            throw AssertionError("unexpected match result: ${match}")
    }
}
