/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.pard.model.simple

/**
 * Validator for the ids of objects in a [SimpleRequirementsModel].
 */
class SimpleModelIdValidator {

    /**
     * Validates that the ids of all [model documents][SimpleRequirementsModel.documents]
     * are unique.
     *
     * @return A map of validation faults:
     *   The map is empty if all ids are unique.
     *   Otherwise, it contains mappings from any duplicate key to a list of objects that share this same key.
     */
    fun validateDocumentIds(model: SimpleRequirementsModel): Map<String, List<SimpleRequirementsDocument>> {
        return validateIdsAreUnique(model.documents)
    }

    /**
     * Validates that the ids of all [model artifacts][SimpleRequirementsDocument.artifacts]
     * of all contained [documents][SimpleRequirementsModel.documents] are globally unique.
     *
     * @return A map of validation faults:
     *   The map is empty if all ids are unique.
     *   Otherwise, it contains mappings from any duplicate key to a list of objects that share this same key.
     */
    fun validateArtifactIds(model: SimpleRequirementsModel): Map<String, List<SimpleRequirementsArtifact>> {
        val allArtifacts = sequence {
            for (doc in model.documents) {
                yieldAll(doc.artifacts)
            }
        }.asIterable()
        return validateIdsAreUnique(allArtifacts)
    }

    internal fun <T : TypedIdentifiableRequirementsObject> validateIdsAreUnique(
        reqObjects: Iterable<T>
    ): Map<String, List<T>> {
        val faultMap = mutableMapOf<String, MutableList<T>>()
        val idMap = mutableMapOf<String, T>()
        for (reqObject in reqObjects) {
            val lastWithSameId = idMap.put(reqObject.id, reqObject)
            if (lastWithSameId != null) {
                val idList = faultMap.getOrPut(reqObject.id) {
                    mutableListOf(lastWithSameId)
                }
                idList.add(reqObject)
            }
        }
        return faultMap
    }
}
