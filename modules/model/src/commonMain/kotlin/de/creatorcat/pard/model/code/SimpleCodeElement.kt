/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.pard.model.code

/**
 * Canonical implementation of a [GenericCodeElement].
 */
data class SimpleCodeElement(
    override val type: String,
    override val name: String,
    override val scope: String
) : GenericCodeElement
