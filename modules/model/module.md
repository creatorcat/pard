# Module pard-model

This module is concerned with models representing:
* requirement artifacts and their structure
* source code elements
* references between model objects

Furthermore, it contains validation and analysis functionality for these models. 

It is the base of all other Pard modules.

State: *Alpha*
* Contains only a [simple model](https://creatorcat.gitlab.io/pard/docs/model/html/pard-model/de.creatorcat.pard.model.simple/-simple-requirements-model)
  that cannot represent requirement properties.
* Contains a basic model of [source code elements](https://creatorcat.gitlab.io/pard/docs/model/html/pard-model/de.creatorcat.pard.model.code/-generic-code-element).

Dependencies:
* CreatorCat Kotiya: `kotiya-core` and `kotiya-matchers`

# Package de.creatorcat.pard.model.simple

Contains a very simple requirements model definition.

# Package de.creatorcat.pard.model.code

Contains a basic model of source code elements.
