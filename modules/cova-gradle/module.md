# Module pard-cova-gradle

Provides a Pard coverage validation Gradle plugin for Kotlin source code:
[PardCovaKotlinPlugin](https://creatorcat.gitlab.io/pard/docs/cova-gradle/html/pard-cova-gradle/de.creatorcat.pard.cova.gradle/-pard-cova-kotlin-plugin)

The coverage validation is based on [Kotlin annotations](https://creatorcat.gitlab.io/pard/docs/cova-annotation/html/pard-cova-annotation) that link code to a
[simple requirement model](https://creatorcat.gitlab.io/pard/docs/model/html/pard-model/de.creatorcat.pard.model.simple/-simple-requirements-model).

State: *Alpha*
* Tasks work fluently but may be extended or adjusted anytime
* No coverage reports available, only errors and warnings on validation faults
* Only simple models supported 

Dependencies:
* [pard-model](https://creatorcat.gitlab.io/pard/docs/model/html/pard-model) and [pard-cova-annotation](https://creatorcat.gitlab.io/pard/docs/cova-annotation/html/pard-cova-annotation)
* CreatorCat Kotiya: `kotiya-core`, `kotiya-matchers` and `kotiya-gradle`
* `kotlin-gradle-plugin` to use the `kapt` annotation processing tools

# Package de.creatorcat.pard.cova.gradle

Contains the plugin, the tasks and all necessary functionality.
