/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

@file:Suppress("PackageDirectoryMismatch", "unused")

package functest

import de.creatorcat.pard.cova.annotation.ImplementsArtifact

/**
 * A dummy class for testing pard-cova annotations.
 */
@ImplementsArtifact("a1", comment = "implementation comment")
class AnnotatedJvmDummy {

    @ImplementsArtifact("a2")
    val property: Int = 17

    @ImplementsArtifact("a3")
    fun foo(): Int = 23
}
