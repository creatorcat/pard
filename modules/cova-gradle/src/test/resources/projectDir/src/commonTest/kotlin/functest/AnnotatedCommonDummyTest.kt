/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

@file:Suppress("PackageDirectoryMismatch", "unused")

package functest

import de.creatorcat.pard.cova.annotation.VerifiesArtifact

/**
 * A dummy class for testing pard-cova annotations.
 */
@VerifiesArtifact("a1", "a2", comment = "verification comment")
class AnnotatedCommonDummyTest {

    @VerifiesArtifact("a3")
    fun fooTest() {
    }
}
