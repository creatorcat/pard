/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.pard.cova.gradle.functest

import de.creatorcat.kotiya.core.text.sloshToSlash
import de.creatorcat.kotiya.matchers.common.contains
import de.creatorcat.kotiya.matchers.operators.isAllOf
import de.creatorcat.kotiya.test.core.assertThat
import de.creatorcat.kotiya.test.gradle.FunctionalGradleTestBase
import de.creatorcat.pard.cova.gradle.KaptBasedCovaAnnoGatheringTask
import de.creatorcat.pard.cova.gradle.PardCovaKotlinPlugin
import de.creatorcat.pard.cova.gradle.PardValidateForwardCoverageTask
import java.io.File
import kotlin.test.Test

// TODO contains to much redundancies to CovaGradleFunctestBase
class MppProjectAnnoGathering : FunctionalGradleTestBase(PardCovaKotlinPlugin.ID) {

    init {
        // kotlin plugin needs to be applied first
        versionedPlugins["org.jetbrains.kotlin.multiplatform"] = KotlinVersion.CURRENT.toString()
    }

    // TODO remove once kotiya provides a better solution
    // the default plugin apply order of FunctionalGradleTestBase does not fit here 
    override fun generateBuildScriptPluginsPart(): String =
        with(StringBuilder()) {
            if (applyBasePlugin) applyPlugin("base")
            for ((id, version) in versionedPlugins) {
                applyPlugin(id, version)
            }
            for (pluginId in defaultPlugins) {
                applyPlugin(pluginId)
            }
            toString()
        }

    // TODO remove once kotiya provides a better solution
    private fun Appendable.applyPlugin(id: String) =
        appendln("id \"$id\"")

    // TODO remove once kotiya provides a better solution
    private fun Appendable.applyPlugin(id: String, version: String) =
        appendln("id \"$id\" version \"$version\"")

    companion object {
        private val tempRepoDir = File("../../build/temp-repo").absolutePath.sloshToSlash()
        private val dependencyConfig = """
            repositories {
                // local temp repo to retrieve most current pard-artifacts
                maven {
                    name = "temp"
                    url = "$tempRepoDir"
                }
                maven {
                    name = "creatorCatBintray"
                    url = "https://dl.bintray.com/knolle/creatorcat"
                }
                jcenter()
            }

            import ${KaptBasedCovaAnnoGatheringTask::class.qualifiedName}
            import ${PardValidateForwardCoverageTask::class.qualifiedName}

            kotlin {
                jvm()
            }

            // dependency to use annotations in code
            dependencies {
                commonMainCompileOnly "de.creatorcat.pard:pard-cova-annotation-metadata:+"
                commonTestCompileOnly "de.creatorcat.pard:pard-cova-annotation-metadata:+"
                jvmMainCompileOnly "de.creatorcat.pard:pard-cova-annotation-jvm:+"
                jvmTestCompileOnly "de.creatorcat.pard:pard-cova-annotation-jvm:+"
            }
        """.trimIndent()
    }

    @Test fun `gather common and jvm annotations`() {
        writeBuildScript(
            """
            $dependencyConfig

            task gatherAnnotations(type: KaptBasedCovaAnnoGatheringTask) {
                compilationTarget = "jvm"
                sourceSetNames = ["main", "test"]
                doLast {
                    println implementingCodeLinkDataProvider.data
                    println verifyingCodeLinkDataProvider.data
                }
            }

            """.trimIndent()
        )
        val result = runAndAssertTask("gatherAnnotations", "--stacktrace", "clean")
        assertThat(
            result.output, isAllOf(
                contains("(artifactId=a1, codeElement=SimpleCodeElement(type=CLASS, name=AnnotatedCommonDummy, scope=functest), comment=implementation comment)"),
                contains("(artifactId=a1, codeElement=SimpleCodeElement(type=CLASS, name=AnnotatedCommonDummyTest, scope=functest), comment=verification comment)"),
                contains("(artifactId=a1, codeElement=SimpleCodeElement(type=CLASS, name=AnnotatedJvmDummy, scope=functest), comment=implementation comment)"),
                contains("(artifactId=a1, codeElement=SimpleCodeElement(type=CLASS, name=AnnotatedJvmDummyTest, scope=functest), comment=verification comment)")
            )
        )
    }

}
