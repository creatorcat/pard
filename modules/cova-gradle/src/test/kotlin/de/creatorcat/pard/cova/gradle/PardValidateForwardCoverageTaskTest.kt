/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.pard.cova.gradle

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import de.creatorcat.kotiya.matchers.common.hasElementsEqual
import de.creatorcat.kotiya.matchers.common.isEmpty
import de.creatorcat.kotiya.matchers.common.isNotNull
import de.creatorcat.kotiya.matchers.not
import de.creatorcat.kotiya.matchers.operators.which
import de.creatorcat.kotiya.test.core.assertThat
import de.creatorcat.kotiya.test.core.assertThrows
import de.creatorcat.pard.model.code.CodeToArtifactLinkDataProvider
import de.creatorcat.pard.model.code.SimpleCodeElement
import de.creatorcat.pard.model.code.StandardCodeToArtifactLink
import de.creatorcat.pard.model.simple.SimpleRequirementsArtifact
import de.creatorcat.pard.model.simple.SimpleRequirementsModel
import de.creatorcat.pard.model.simple.SimpleRequirementsModelBuilder
import de.creatorcat.pard.model.simple.SimpleRequirementsModelProvider
import de.creatorcat.pard.model.simple.build
import org.gradle.api.Buildable
import org.gradle.api.Task
import org.gradle.api.tasks.TaskDependency
import org.gradle.testfixtures.ProjectBuilder
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertNull
import kotlin.test.assertTrue

class PardValidateForwardCoverageTaskTest {

    private val project = ProjectBuilder.builder().build()
    private val task = project.tasks.create("testeeTask", PardValidateForwardCoverageTask::class.java)

    @Test fun `default properties`() {
        with(task) {
            assertFalse(failOnUncoveredArtifact)
            assertTrue(failOnMissingLinkTarget)
            assertNull(modelProvider)
            assertNull(linkDataProvider)
            assertThat(description, isNotNull which !isEmpty)
            assertEquals(PardCovaKotlinPlugin.PARD_TASK_GROUP, group)
        }
    }

    @Test fun `modelProvider - set adjusts dependency for tasks`() {
        val taskProvider1 = mock<TaskModelProvider>()
        val taskProvider2 = mock<TaskModelProvider>()

        with(task) {
            assertThat(taskDependencies.getDependencies(this), isEmpty)
            modelProvider = taskProvider1
            assertThat(taskDependencies.getDependencies(this), hasElementsEqual<Any?>(taskProvider1))
            modelProvider = taskProvider2
            assertThat(taskDependencies.getDependencies(this), hasElementsEqual<Any?>(taskProvider2))
            modelProvider = providerOf {}
            assertThat(taskDependencies.getDependencies(this), isEmpty)
        }
    }

    @Test fun `linkDataProvider - set adjusts dependency for task produced data`() {
        val task1 = mock<Task>()
        val task2 = mock<Task>()

        val taskDep1 = mock<TaskDependency> {
            on { getDependencies(any()) } doReturn mutableSetOf(task1)
        }
        val taskDep2 = mock<TaskDependency> {
            on { getDependencies(any()) } doReturn mutableSetOf(task2)
        }

        val provider1 = mock<LinkDataFromTask> {
            on { buildDependencies } doReturn taskDep1
        }
        val provider2 = mock<LinkDataFromTask> {
            on { buildDependencies } doReturn taskDep2
        }

        with(task) {
            assertThat(taskDependencies.getDependencies(this), isEmpty)
            linkDataProvider = provider1
            assertThat(taskDependencies.getDependencies(this), hasElementsEqual<Any?>(task1))
            linkDataProvider = provider2
            assertThat(taskDependencies.getDependencies(this), hasElementsEqual<Any?>(task2))
            linkDataProvider = providerOf()
            assertThat(taskDependencies.getDependencies(this), isEmpty)
        }
    }

    @Test fun `linkDataProvider - modelDataProvider - dependency intersections`() {
        val task1 = mock<TaskModelProvider>()
        val task2 = mock<TaskModelProvider>()

        val taskDep1 = mock<TaskDependency> {
            on { getDependencies(any()) } doReturn mutableSetOf(task1)
        }

        val provider1 = mock<LinkDataFromTask> {
            on { buildDependencies } doReturn taskDep1
        }

        with(task) {
            assertThat(taskDependencies.getDependencies(this), isEmpty)
            linkDataProvider = provider1
            assertThat(taskDependencies.getDependencies(this), hasElementsEqual<Any?>(task1))
            modelProvider = task1
            assertThat(taskDependencies.getDependencies(this), hasElementsEqual<Any?>(task1))
            linkDataProvider = providerOf()
            assertThat(taskDependencies.getDependencies(this), hasElementsEqual<Any?>(task1))
            modelProvider = task2
            assertThat(taskDependencies.getDependencies(this), hasElementsEqual<Any?>(task2))
        }
    }

    @Test fun `artifact filter - default and setter`() {
        val artifactX = mock<SimpleRequirementsArtifact> {
            on { type } doReturn "typeX"
        }
        val artifactY = mock<SimpleRequirementsArtifact> {
            on { type } doReturn "typeY"
        }

        with(task) {
            assertTrue(artifactFilter(artifactX))
            assertTrue(artifactFilter(artifactY))

            artifactFilter = { it.type == "typeX" }
            assertTrue(artifactFilter(artifactX))
            assertFalse(artifactFilter(artifactY))
        }
    }

    // TODO test filter closure method

    @Test fun `validate - invalid and empty providers`() {
        with(task) {
            assertThrows<IllegalStateException> { validate() }
            modelProvider = providerOf { }
            assertThrows<IllegalStateException> { validate() }
            modelProvider = null
            linkDataProvider = providerOf()
            assertThrows<IllegalStateException> { validate() }
            modelProvider = providerOf { }
            validate()
        }
    }

    @Test fun `validate - with and without faults`() {
        with(task) {
            modelProvider = providerOf {
                addDocument("d", "dt", "doc") {
                    addArtifact("a1", "at", "art 1")
                    addArtifact("a2", "at", "art 2")
                }
            }
            linkDataProvider = providerOf("a1", "a2")
            failOnMissingLinkTarget = true
            failOnUncoveredArtifact = true
            validate()

            linkDataProvider = providerOf("a1", "b1")

            failOnMissingLinkTarget = false
            failOnUncoveredArtifact = false
            validate()

            failOnMissingLinkTarget = true
            assertThrows<RuntimeException> { validate() }

            failOnMissingLinkTarget = false
            failOnUncoveredArtifact = true
            assertThrows<RuntimeException> { validate() }
        }
    }

    // TODO test validation with filter

    private fun providerOf(modelConfig: SimpleRequirementsModelBuilder.() -> Unit): SimpleRequirementsModelProvider =
        object : SimpleRequirementsModelProvider {
            override val model = SimpleRequirementsModel.build(modelConfig)
        }

    private fun providerOf(vararg artifactIds: String): CodeToArtifactLinkDataProvider =
        object : CodeToArtifactLinkDataProvider {
            override val data = artifactIds.map {
                StandardCodeToArtifactLink(it, SimpleCodeElement("type", "c#$it", "scope"), "comment")
            }
        }

    private interface TaskModelProvider : SimpleRequirementsModelProvider, Task
    private interface LinkDataFromTask : CodeToArtifactLinkDataProvider, Buildable
}
