/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.pard.cova.gradle

import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import de.creatorcat.kotiya.matchers.common.exists
import de.creatorcat.kotiya.matchers.not
import de.creatorcat.kotiya.test.core.assertThat
import de.creatorcat.kotiya.test.core.assertThrows
import de.creatorcat.kotiya.test.core.io.FileCreatingTestBase
import de.creatorcat.pard.cova.annotation.DataDirOptionCovaAnnoProc
import de.creatorcat.pard.cova.annotation.ImplementsArtifact
import java.io.File
import javax.annotation.processing.Messager
import kotlin.test.Test
import kotlin.test.assertEquals

class KaptAwareCovaAnnoProcTest : KaptAwareCovaAnnoProc(), FileCreatingTestBase {
    private val messager: Messager = mock()
    private val linkDataDirFromOption = testOutFile("link/data/dir")
    private val sourceSetLinkDataDir = testOutFile("link/data/dir/main")

    init {
        processingEnv = mock {
            on { messager } doReturn messager
            on { options } doReturn mutableMapOf(
                DataDirOptionCovaAnnoProc.ARGUMENT_LINK_DATA_DIR to linkDataDirFromOption.absolutePath,
                KAPT_ARGUMENT_KOTLIN_GENERATED_SRC_DIR to "kapt/generated/dir/main"
            )
        }
    }

    @Test fun `initialize - creates source set specific link data dir from option`() {
        assertThat(sourceSetLinkDataDir, !exists)
        initialize()
        assertThat(sourceSetLinkDataDir, exists)
    }

    @Test fun `initialize - fails on missing kapt option`() {
        processingEnv = mock {
            on { messager } doReturn messager
            on { options } doReturn mutableMapOf(
                DataDirOptionCovaAnnoProc.ARGUMENT_LINK_DATA_DIR to linkDataDirFromOption.absolutePath
            )
        }
        assertThrows<IllegalStateException> { initialize() }
    }

    @Test fun `getLinkDataFileFor - uses source set link data dir`() {
        initialize()
        assertEquals(
            File(sourceSetLinkDataDir, getLinkDataFileNameFor(ImplementsArtifact::class)).canonicalFile,
            getLinkDataFileFor(ImplementsArtifact::class).canonicalFile
        )
    }
}
