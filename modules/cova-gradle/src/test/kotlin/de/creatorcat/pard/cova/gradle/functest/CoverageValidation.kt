/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.pard.cova.gradle.functest

import de.creatorcat.kotiya.matchers.common.contains
import de.creatorcat.kotiya.test.core.assertThat
import de.creatorcat.pard.model.simple.SimpleRequirementsModel
import de.creatorcat.pard.model.simple.SimpleRequirementsModelBuilder
import org.gradle.testkit.runner.TaskOutcome.FAILED
import kotlin.test.Test

class CoverageValidation : CovaGradleFunctestBase() {

    @Test fun `gather annotations and validate`() {
        writeBuildScript(
            """
            $dependencyConfig

            import ${SimpleRequirementsModel::class.qualifiedName}
            import ${SimpleRequirementsModelBuilder::class.qualifiedName}

            def modelBuilder = new SimpleRequirementsModelBuilder()
            def docBuilder = modelBuilder.addDocument("d", "spec", "Spec Doc") {}
            docBuilder.addArtifact("a1", "req", "Req 1")
            docBuilder.addArtifact("a2", "req", "Req 2")
            def incompleteModel = modelBuilder.build()

            docBuilder.addArtifact("a3", "req", "Req 3")
            def completeModel = modelBuilder.build()

            task gatherAnnotations(type: KaptBasedCovaAnnoGatheringTask) {
                sourceSetNames = ["main", "test"]
            }

            task validateImplementationCoverage(type: PardValidateForwardCoverageTask) {
                failOnUncoveredArtifact = true
                failOnMissingLinkTarget = true
                modelProvider = { completeModel }
                linkDataProvider = gatherAnnotations.implementingCodeLinkDataProvider
            }

            task validateVerificationCoverage(type: PardValidateForwardCoverageTask) {
                modelProvider = { incompleteModel }
                linkDataProvider = gatherAnnotations.verifyingCodeLinkDataProvider
            }
            """.trimIndent()
        )
        runAndAssertTask("validateImplementationCoverage", "--stacktrace", "clean")
        val failure = runAndAssertTask(
            "validateVerificationCoverage", expectedOutcome = FAILED, expectBuildSuccess = false
        )
        assertThat(failure.output, contains("missing target artifact id=a3 in @VerifiesArtifact on: METHOD(fooTest)"))
    }

    @Test fun `validate with artifact filter`() {
        writeBuildScript(
            """
            $dependencyConfig

            import ${SimpleRequirementsModel::class.qualifiedName}
            import ${SimpleRequirementsModelBuilder::class.qualifiedName}

            def modelBuilder = new SimpleRequirementsModelBuilder()
            def docBuilder = modelBuilder.addDocument("d", "spec", "Spec Doc") {}
            docBuilder.addArtifact("a1", "reqX", "Req 1")
            docBuilder.addArtifact("a2", "reqY", "Req 2")
            docBuilder.addArtifact("a3", "reqX", "Req 3")
            def completeModel = modelBuilder.build()

            task gatherAnnotations(type: KaptBasedCovaAnnoGatheringTask) {
                sourceSetNames = ["main"]
            }

            task validateImplementationCoverage(type: PardValidateForwardCoverageTask) {
                filterArtifacts {
                    type == "reqX"
                }
                failOnMissingLinkTarget = true
                failOnUncoveredArtifact = true
                modelProvider = { completeModel }
                linkDataProvider = gatherAnnotations.implementingCodeLinkDataProvider
            }

            """.trimIndent()
        )
        val failure = runAndAssertTask(
            "validateImplementationCoverage", "--stacktrace", "clean",
            expectedOutcome = FAILED, expectBuildSuccess = false
        )
        assertThat(failure.output, contains("e: coverage validation error: missing target artifact id=a2"))
    }
}
