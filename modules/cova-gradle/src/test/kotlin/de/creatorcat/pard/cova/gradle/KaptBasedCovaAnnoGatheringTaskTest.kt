/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and testProject contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.pard.cova.gradle

import de.creatorcat.kotiya.matchers.common.hasElementsEqual
import de.creatorcat.kotiya.matchers.common.isEmpty
import de.creatorcat.kotiya.matchers.common.isInstanceOf
import de.creatorcat.kotiya.matchers.common.isNotNull
import de.creatorcat.kotiya.matchers.not
import de.creatorcat.kotiya.matchers.operators.each
import de.creatorcat.kotiya.matchers.operators.which
import de.creatorcat.kotiya.test.core.assertThat
import org.gradle.testfixtures.ProjectBuilder
import org.jetbrains.kotlin.gradle.plugin.KaptExtension
import java.util.concurrent.Callable
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertSame

class KaptBasedCovaAnnoGatheringTaskTest {

    internal open class TesteeTask : KaptBasedCovaAnnoGatheringTask() {
        val configurator: KaptLazyConfigurator by lazy { aptConfigurator as KaptLazyConfigurator }
    }

    private val testProject = ProjectBuilder.builder().build()
    private val task = testProject.tasks.create("testeeTask", TesteeTask::class.java)
    private val kaptExtension = testProject.extensions.create("testeeKaptExtension", KaptExtension::class.java)
    private val configurator = task.configurator

    @Test fun `task - properties`() {
        with(task) {
            assertThat(description, isNotNull which !isEmpty)
            assertEquals(PardCovaKotlinPlugin.PARD_TASK_GROUP, group)
        }
    }

    @Test fun `configurator - properties`() {
        assertSame(task, configurator.task)
        assertSame(testProject, configurator.project)
        assertSame(kaptExtension, configurator.kaptExtension)
    }

    @Test fun `configurator - getKaptTaskNameFor source set`() {
        assertEquals("kaptKotlin", configurator.getKaptTaskNameFor("main"))
        assertEquals("kaptTestKotlin", configurator.getKaptTaskNameFor("test"))
        assertEquals("kaptCustomKotlin", configurator.getKaptTaskNameFor("custom"))
    }

    @Test fun `configurator - getKaptTaskNameFor source set - with target`() {
        task.compilationTarget = "target"

        assertEquals("kaptKotlinTarget", configurator.getKaptTaskNameFor("main"))
        assertEquals("kaptTestKotlinTarget", configurator.getKaptTaskNameFor("test"))
        assertEquals("kaptCustomKotlinTarget", configurator.getKaptTaskNameFor("custom"))
    }

    @Test fun `configurator - configuration dependencies`() {
        val kaptMainConfig = testProject.configurations.create("kapt")
        val kaptTestConfig = testProject.configurations.create("kaptTest")
        val kaptCustomConfig = testProject.configurations.create("kaptCustom")
        task.sourceSetNames = listOf("main", "test", "custom")
        assertThat(kaptMainConfig.dependencies, hasElementsEqual(configurator.pardCovaDependency))
        assertThat(kaptTestConfig.dependencies, hasElementsEqual(configurator.pardCovaDependency))
        assertThat(kaptCustomConfig.dependencies, hasElementsEqual(configurator.pardCovaDependency))
        task.sourceSetNames = listOf("custom")
        assertThat(kaptMainConfig.dependencies, isEmpty)
        assertThat(kaptTestConfig.dependencies, isEmpty)
        assertThat(kaptCustomConfig.dependencies, hasElementsEqual(configurator.pardCovaDependency))
    }

    @Test fun `configurator - task dependencies`() {
        assertThat(task.dependsOn, isEmpty)
        task.sourceSetNames = listOf("main", "test")
        assertThat(task.dependsOn, each(isInstanceOf<Callable<*>>()))
        assertEquals(1, task.dependsOn.size)
        task.sourceSetNames = listOf("main", "test", "c1", "c2")
        assertEquals(1, task.dependsOn.size)
        task.sourceSetNames = listOf()
        assertEquals(1, task.dependsOn.size)
    }

    @Test fun `configurator - annotation processor`() {
        configurator.configureAnnotationProcessor()
        assertEquals(false, kaptExtension.includeCompileClasspath)
        assertEquals(KaptAwareCovaAnnoProc::class.qualifiedName, kaptExtension.processors)
        configurator.configureAnnotationProcessor()
        assertEquals(KaptAwareCovaAnnoProc::class.qualifiedName, kaptExtension.processors)
    }
}
