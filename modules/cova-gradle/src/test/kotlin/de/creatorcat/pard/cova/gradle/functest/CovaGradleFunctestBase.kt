/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.pard.cova.gradle.functest

import de.creatorcat.kotiya.core.text.sloshToSlash
import de.creatorcat.kotiya.test.gradle.FunctionalGradleTestBase
import de.creatorcat.pard.cova.gradle.CovaAnnoGatheringTask
import de.creatorcat.pard.cova.gradle.KaptBasedCovaAnnoGatheringTask
import de.creatorcat.pard.cova.gradle.PardCovaKotlinPlugin
import de.creatorcat.pard.cova.gradle.PardValidateForwardCoverageTask
import java.io.File

/**
 * Base class for cova-gradle functional tests providing some common initialization logic and utilities.
 */
abstract class CovaGradleFunctestBase(
    vararg pluginIds: String = arrayOf(PardCovaKotlinPlugin.ID)
) : FunctionalGradleTestBase(*pluginIds) {

    init {
        // kotlin plugin needs to be applied first
        versionedPlugins["org.jetbrains.kotlin.jvm"] = KotlinVersion.CURRENT.toString()
    }

    // TODO remove once kotiya provides a better solution
    // the default plugin apply order of FunctionalGradleTestBase does not fit here 
    override fun generateBuildScriptPluginsPart(): String =
        with(StringBuilder()) {
            if (applyBasePlugin) applyPlugin("base")
            for ((id, version) in versionedPlugins) {
                applyPlugin(id, version)
            }
            for (pluginId in defaultPlugins) {
                applyPlugin(pluginId)
            }
            toString()
        }

    // TODO remove once kotiya provides a better solution
    private fun Appendable.applyPlugin(id: String) =
        appendln("id \"$id\"")

    // TODO remove once kotiya provides a better solution
    private fun Appendable.applyPlugin(id: String, version: String) =
        appendln("id \"$id\" version \"$version\"")

    protected fun getLinkDataDirFor(sourceSetName: String) =
        getBuildFile("${CovaAnnoGatheringTask.LINK_DATA_MAIN_DIR_SUB_PATH}/$sourceSetName")

    companion object {
        private val tempRepoDir = File("../../build/temp-repo").absolutePath.sloshToSlash()
        internal val dependencyConfig = """
            repositories {
                // local temp repo to retrieve most current pard-artifacts
                maven {
                    name = "temp"
                    url = "$tempRepoDir"
                }
                maven {
                    name = "creatorCatBintray"
                    url = "https://dl.bintray.com/knolle/creatorcat"
                }
                jcenter()
            }

            import ${KaptBasedCovaAnnoGatheringTask::class.qualifiedName}
            import ${PardValidateForwardCoverageTask::class.qualifiedName}

            // dependency to use annotations in code
            dependencies {
                compileOnly "de.creatorcat.pard:pard-cova-annotation-jvm:+"
                testCompileOnly "de.creatorcat.pard:pard-cova-annotation-jvm:+"
            }
        """.trimIndent()
    }
}
