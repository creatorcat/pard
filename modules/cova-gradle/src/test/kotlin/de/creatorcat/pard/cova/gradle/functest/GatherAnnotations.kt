/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.pard.cova.gradle.functest

import de.creatorcat.kotiya.matchers.and
import de.creatorcat.kotiya.matchers.common.contains
import de.creatorcat.kotiya.matchers.operators.which
import de.creatorcat.kotiya.test.core.assertThat
import de.creatorcat.kotiya.test.gradle.hasOutcome
import de.creatorcat.kotiya.test.gradle.hasTask
import de.creatorcat.pard.cova.gradle.KaptAwareCovaAnnoProc
import org.gradle.testkit.runner.TaskOutcome.SUCCESS
import org.gradle.testkit.runner.TaskOutcome.UP_TO_DATE
import kotlin.test.Test
import kotlin.test.assertTrue

class GatherAnnotations : CovaGradleFunctestBase() {

    @Test fun `gather annotations`() {
        writeBuildScript(
            """
            $dependencyConfig

            task gatherAnnotations(type: KaptBasedCovaAnnoGatheringTask) {
                sourceSetNames = ["main", "test"]
                doLast {
                    println implementingCodeLinkDataProvider.data
                    println verifyingCodeLinkDataProvider.data
                }
            }

            ${printAssertableValue("kapt.processors")} 
            """.trimIndent()
        )
        val result = runAndAssertTask("gatherAnnotations", "--stacktrace", "clean")
        result.assertOutputValues("kapt.processors" to KaptAwareCovaAnnoProc::class.qualifiedName)
        assertThat(
            result.output,
            contains(
                "StandardCodeToArtifactLink(artifactId=a1, codeElement=SimpleCodeElement(type=CLASS, " +
                    "name=AnnotatedDummy, scope=functest), comment=implementation comment)"
            )
        )
        assertThat(
            result.output,
            contains(
                "StandardCodeToArtifactLink(artifactId=a1, codeElement=SimpleCodeElement(type=CLASS, " +
                    "name=AnnotatedDummyTest, scope=functest), comment=verification comment)"
            )
        )
        assertThat(
            runAndAssertTask("gatherAnnotations"),
            (hasTask("kaptKotlin") which hasOutcome(UP_TO_DATE))
                and (hasTask("kaptTestKotlin") which hasOutcome(UP_TO_DATE))
        )
    }

    @Test fun `up-to-date depends on link data`() {
        writeBuildScript(
            """
            $dependencyConfig

            task gatherAnnotations(type: KaptBasedCovaAnnoGatheringTask) {
                sourceSetNames = ["main", "test"]
            }
            """.trimIndent()
        )
        assertThat(
            runAndAssertTask("gatherAnnotations", "--stacktrace", "clean"),
            (hasTask("kaptKotlin") which hasOutcome(SUCCESS))
                and (hasTask("kaptTestKotlin") which hasOutcome(SUCCESS))
        )
        assertTrue(getLinkDataDirFor("main").deleteRecursively())
        assertThat(
            runAndAssertTask("gatherAnnotations"),
            (hasTask("kaptKotlin") which hasOutcome(SUCCESS))
                and (hasTask("kaptTestKotlin") which hasOutcome(UP_TO_DATE))
        )
        assertTrue(getLinkDataDirFor("test").deleteRecursively())
        assertThat(
            runAndAssertTask("gatherAnnotations"),
            (hasTask("kaptKotlin") which hasOutcome(UP_TO_DATE))
                and (hasTask("kaptTestKotlin") which hasOutcome(SUCCESS))
        )
    }
}
