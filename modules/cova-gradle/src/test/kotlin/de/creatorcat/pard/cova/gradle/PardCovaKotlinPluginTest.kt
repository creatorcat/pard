/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.pard.cova.gradle

import de.creatorcat.kotiya.matchers.common.containsAllAndOnly
import de.creatorcat.kotiya.matchers.common.isBlank
import de.creatorcat.kotiya.matchers.common.isEmpty
import de.creatorcat.kotiya.matchers.common.isInstanceOf
import de.creatorcat.kotiya.matchers.operators.matches
import de.creatorcat.kotiya.matchers.operators.which
import de.creatorcat.kotiya.test.core.assertThat
import de.creatorcat.pard.lang.gradle.PardLangPlugin
import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import org.jetbrains.kotlin.gradle.dsl.KotlinMultiplatformExtension
import org.jetbrains.kotlin.gradle.dsl.KotlinProjectExtension
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class PardCovaKotlinPluginTest {

    // TODO use generated constant
    private val kotlinPluginId = "org.jetbrains.kotlin.jvm"
    private val kotlinMppPluginId = "org.jetbrains.kotlin.multiplatform"

    private val project: Project = with(ProjectBuilder.builder()) {
        val project = build()
        // apply kotlin plugin first to avoid warning - this is what the user has to do
        project.pluginManager.apply(kotlinPluginId)
        // add some source sets to test default source set behavior
        with(project.extensions.getByName("kotlin") as KotlinProjectExtension) {
            sourceSets.create("custom")
        }
        project.pluginManager.apply(PardCovaKotlinPlugin.ID)
        project
    }

    @Test fun `plugin - can be applied`() {
        assertTrue(project.pluginManager.hasPlugin(PardCovaKotlinPlugin.ID))
    }

    @Test fun `apply - also applies kapt`() {
        assertTrue(project.pluginManager.hasPlugin(PardCovaKotlinPlugin.KAPT_PLUGIN_ID))
    }

    @Test fun `creates and configures default tasks`() {
        val gatherTask = project.tasks.getByName(PardCovaKotlinPlugin.GATHER_ANNO_TASK_NAME)
            as KaptBasedCovaAnnoGatheringTask

        assertThat(gatherTask.compilationTarget, isBlank)
        assertThat(gatherTask.sourceSetNames, containsAllAndOnly(listOf("custom", "main", "test")))

        assertThat(
            project.tasks.getByName(PardCovaKotlinPlugin.FORWARD_IMPLEMENTATION_COVA_TASK_NAME),
            isInstanceOf<PardValidateForwardCoverageTask>() which matches {
                it.linkDataProvider === gatherTask.implementingCodeLinkDataProvider
            }
        )
        assertThat(
            project.tasks.getByName(PardCovaKotlinPlugin.FORWARD_VERIFICATION_COVA_TASK_NAME),
            isInstanceOf<PardValidateForwardCoverageTask>() which matches {
                it.linkDataProvider === gatherTask.verifyingCodeLinkDataProvider
            }
        )
    }

    @Test fun `configures target specific source sets - jvm`() {
        // TODO redundant code
        val mppProject: Project = with(ProjectBuilder.builder()) {
            val project = build()
            // apply kotlin plugin first to avoid warning - this is what the user has to do
            project.pluginManager.apply(kotlinMppPluginId)
            // add some source sets to test default source set behavior
            with(project.extensions.getByName("kotlin") as KotlinMultiplatformExtension) {
                jvm()
                sourceSets.create("jvmCustom")
            }
            project.pluginManager.apply(PardCovaKotlinPlugin.ID)
            project
        }

        val gatherTask = mppProject.tasks.getByName(PardCovaKotlinPlugin.GATHER_ANNO_TASK_NAME)
            as KaptBasedCovaAnnoGatheringTask

        assertEquals("jvm", gatherTask.compilationTarget)
        assertThat(gatherTask.sourceSetNames, containsAllAndOnly(listOf("custom", "main", "test")))
    }

    @Test fun `configures no target specific source sets if no supported platform`() {
        // TODO redundant code
        val mppProject: Project = with(ProjectBuilder.builder()) {
            val project = build()
            // apply kotlin plugin first to avoid warning - this is what the user has to do
            project.pluginManager.apply(kotlinMppPluginId)
            // add some source sets to test default source set behavior
            with(project.extensions.getByName("kotlin") as KotlinMultiplatformExtension) {
                js()
            }
            project.pluginManager.apply(PardCovaKotlinPlugin.ID)
            project
        }

        val gatherTask = mppProject.tasks.getByName(PardCovaKotlinPlugin.GATHER_ANNO_TASK_NAME)
            as KaptBasedCovaAnnoGatheringTask

        assertThat(gatherTask.compilationTarget, isBlank)
        assertThat(gatherTask.sourceSetNames, isEmpty)
    }

    @Test fun `configures lang model provider if present`() {
        project.plugins.apply(PardLangPlugin.ID)
        val parseTask = project.tasks.getByName(PardLangPlugin.PARSE_TASK_NAME)
        assertThat(
            project.tasks.getByName(PardCovaKotlinPlugin.FORWARD_IMPLEMENTATION_COVA_TASK_NAME),
            isInstanceOf<PardValidateForwardCoverageTask>() which matches {
                it.modelProvider === parseTask
            }
        )
        assertThat(
            project.tasks.getByName(PardCovaKotlinPlugin.FORWARD_VERIFICATION_COVA_TASK_NAME),
            isInstanceOf<PardValidateForwardCoverageTask>() which matches {
                it.modelProvider === parseTask
            }
        )
    }
}
