/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.pard.cova.gradle

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import de.creatorcat.kotiya.matchers.common.hasElementsEqual
import de.creatorcat.kotiya.matchers.common.isEmpty
import de.creatorcat.kotiya.test.core.assertThat
import de.creatorcat.kotiya.test.core.assertThrows
import de.creatorcat.pard.cova.annotation.ImplementsArtifact
import de.creatorcat.pard.cova.annotation.VerifiesArtifact
import de.creatorcat.pard.model.code.CodeToArtifactLink
import de.creatorcat.pard.model.code.SimpleCodeElement
import de.creatorcat.pard.model.code.StandardCodeToArtifactLink
import org.gradle.api.Task
import org.gradle.testfixtures.ProjectBuilder
import java.io.File
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertSame

class CovaAnnoGatheringTaskTest {

    companion object {
        private val aptConfig: AnnoProcToolLazyConfigurator = mock()
    }

    internal open class CovaAnnotationGatheringTestTask : CovaAnnoGatheringTask() {
        override val aptConfigurator: AnnoProcToolLazyConfigurator = aptConfig
    }

    private val projectDir = File("src/test/resources/projectDir")
    private val project = ProjectBuilder.builder().withProjectDir(projectDir).build()
    private val task = project.tasks.create("testeeTask", CovaAnnotationGatheringTestTask::class.java)

    @Test fun `default properties`() {
        with(task) {
            with(implementingCodeLinkDataProvider) {
                assertSame(task, producer)
                assertEquals("@ImplementsArtifact", codeToArtifactLinkName)
                assertThrows<IllegalStateException> { data }
            }
            with(verifyingCodeLinkDataProvider) {
                assertSame(task, producer)
                assertEquals("@VerifiesArtifact", codeToArtifactLinkName)
                assertThrows<IllegalStateException> { data }
            }
            assertThat(sourceSetNames, isEmpty)
            assertThat(sourceLinkDataDirs, isEmpty)
            assertEquals(PardCovaKotlinPlugin.PARD_TASK_GROUP, task.group)
        }
    }

    @Test fun `AnnotationLinkDataProvider - creation and properties`() {
        val linkData: List<CodeToArtifactLink> = emptyList()
        with(task.AnnotationLinkDataProvider({ linkData }, VerifiesArtifact::class)) {
            assertSame(linkData, data)
            assertSame(task, producer)
            assertThat(buildDependencies.getDependencies(null), hasElementsEqual<Task>(task))
            assertEquals("@VerifiesArtifact", codeToArtifactLinkName)
            assertEquals("verified", coverageDescribingVerb)
        }

        with(task.AnnotationLinkDataProvider({ linkData }, ImplementsArtifact::class)) {
            assertEquals("@ImplementsArtifact", codeToArtifactLinkName)
            assertEquals("implemented", coverageDescribingVerb)
        }
    }

    @Test fun `sourceSetNames - triggers configuration`() {
        task.sourceSetNames = listOf("main")
        assertThat(task.sourceSetNames, hasElementsEqual("main"))
        verify(aptConfig).bindLazyConfigurationActions()
        verify(aptConfig).updateNonLazyConfiguration()
        task.sourceSetNames = listOf("main", "test")
        assertThat(task.sourceSetNames, hasElementsEqual("main", "test"))
        verify(aptConfig) {
            2 * { updateNonLazyConfiguration() }
        }
    }

    @Test fun `sourceLinkDataDirs - generation`() {
        task.sourceSetNames = listOf("main", "test")
        assertThat(
            task.sourceLinkDataDirs.map(File::getCanonicalFile),
            hasElementsEqual(
                File(project.buildDir, "${CovaAnnoGatheringTask.LINK_DATA_MAIN_DIR_SUB_PATH}/main").canonicalFile,
                File(project.buildDir, "${CovaAnnoGatheringTask.LINK_DATA_MAIN_DIR_SUB_PATH}/test").canonicalFile
            )
        )
    }

    @Test fun `gather annotation - existing link data`() {
        project.setBuildDir("buildWithLinkData")
        task.sourceSetNames = listOf("test", "main")
        task.gatherAnnotations()
        assertThat(
            task.implementingCodeLinkDataProvider.data,
            hasElementsEqual(
                linkOf("a1", "CLASS", "Dummy", "cova.gradle", "nice comment"),
                linkOf("a1", "FIELD", "_privateProp", "cova.gradle.Dummy"),
                linkOf("a2", "METHOD", "foo", "cova.gradle.Dummy")
            )
        )
        assertThat(
            task.verifyingCodeLinkDataProvider.data,
            hasElementsEqual(
                linkOf("a1", "CLASS", "SpecificDummyTest", "cova.gradle", "some comment"),
                linkOf("a2", "METHOD", "testFoo", "cova.gradle.DummyTest")
            )
        )
    }

    private fun linkOf(
        artifactId: String,
        codeType: String, codeName: String, codeScope: String,
        comment: String = ""
    ): CodeToArtifactLink =
        StandardCodeToArtifactLink(
            artifactId,
            SimpleCodeElement(codeType, codeName, codeScope),
            comment
        )
}
