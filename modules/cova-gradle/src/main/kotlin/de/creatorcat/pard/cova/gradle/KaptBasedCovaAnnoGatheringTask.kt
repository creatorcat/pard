/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.pard.cova.gradle

import de.creatorcat.pard.cova.annotation.DataDirOptionCovaAnnoProc
import org.gradle.api.Project
import org.gradle.api.artifacts.Dependency
import org.gradle.api.tasks.Input
import org.jetbrains.kotlin.gradle.plugin.KaptExtension
import java.util.concurrent.Callable

/**
 * A task to gather code-to-artifact link data from kotlin code based on cova annotations.
 * The task uses `kotlin-kapt` to process the code with a [KaptAwareCovaAnnoProc].
 * The link data gathered while processing the annotations will be stored into link data files
 * and is made available by [implementingCodeLinkDataProvider] and [verifyingCodeLinkDataProvider].
 * These providers can be used in coverage validation tasks.
 *
 * Configuration in Gradle (with default values):
 *
 *     task myCovaAnnoGatheringTask(type: KaptBasedCovaAnnoGatheringTask) {
 *
 *         // Name of the compilation target for the annotation processor,
 *         // since kapt has specific tasks for each target.
 *         // Only JVM-targets are supported.
 *         //
 *         // This will be empty for new tasks,
 *         // but it is set to the jvm target in multi-platform projects for the plugins default task.
 *         compilationTarget = "jvm"
 *
 *         // The names of the processed source sets, without target platform specific prefix.
 *         // This will be empty for new tasks,
 *         // but it is set to contain all jvm sets in the plugins default gathering task.
 *         sourceSetNames = ["main", "test"]
 *     }
 *
 * @see PardValidateForwardCoverageTask
 */
open class KaptBasedCovaAnnoGatheringTask : CovaAnnoGatheringTask() {

    init {
        description = "gathers pard-cova annotation link data using kotlin-kapt"
    }

    /**
     * Name of the compilation target for which the annotation processing shall be performed.
     * This defines which kapt-tasks will be run, as there are separate kapt-tasks for each platform.
     * This also defines the target prefix of the actually processed [sourceSetNames].
     * Only JVM-targets are supported so far.
     * This must be empty for non-multi-platform projects.
     *
     * Defaults to the empty string (non-multi-platform).
     */
    @Input
    var compilationTarget: String = ""

    override val aptConfigurator: AnnoProcToolLazyConfigurator = KaptLazyConfigurator()

    internal inner class KaptLazyConfigurator : AnnoProcToolLazyConfigurator {

        inline val task: CovaAnnoGatheringTask get() = this@KaptBasedCovaAnnoGatheringTask
        inline val project: Project get() = task.project
        val kaptExtension: KaptExtension by lazy { project.extensions.getByType(KaptExtension::class.java) }
        val pardCovaDependency: Dependency by lazy {
            project.dependencies.create(ANNOTATION_PROCESSOR_DEPENDENCY_NOTATION)
        }

        override fun bindLazyConfigurationActions() {
            configureTaskDependencies()
            configureTaskOutputs()
            configureAnnotationProcessor()
        }

        override fun updateNonLazyConfiguration() {
            // TODO configure lazily once possible
            // to configure configuration dependencies lazily, there are two approaches:
            // * provide a custom configuration and make the kapt-config extend from it
            //   this custom config must then still be changed non-lazily 
            // * usage of Configuration.withDependencies is probably the best solution
            // both are currently not supported by kapt
            configureConfigurationDependencies()
        }

        private fun configureConfigurationDependencies() {
            project.configurations.filter { it.name.startsWith(KAPT_NAMED_OBJECT_PREFIX) }
                .forEach {
                    val associatedSourceSet =
                        if (it.name == KAPT_NAMED_OBJECT_PREFIX) DEFAULT_SOURCE_SET_NAME
                        else it.name.drop(KAPT_NAMED_OBJECT_PREFIX.length).decapitalize()
                    if (task.sourceSetNames.contains(associatedSourceSet))
                        it.dependencies.add(pardCovaDependency)
                    else
                        it.dependencies.remove(pardCovaDependency)
                }
        }

        fun configureAnnotationProcessor() {
            with(kaptExtension) {
                includeCompileClasspath = false
                arguments {
                    arg(
                        DataDirOptionCovaAnnoProc.ARGUMENT_LINK_DATA_DIR,
                        "${project.buildDir}/${CovaAnnoGatheringTask.LINK_DATA_MAIN_DIR_SUB_PATH}"
                    )
                }
                val annoProcName = checkNotNull(KaptAwareCovaAnnoProc::class.qualifiedName)
                // only add the anno proc once!
                if (!processors.contains(annoProcName))
                    annotationProcessor(annoProcName)
            }
        }

        private fun configureTaskDependencies() {
            task.dependsOn(Callable { task.sourceSetNames.map { getKaptTaskNameFor(it) } })
        }

        private fun configureTaskOutputs() {
            project.afterEvaluate {
                for (sourceSet in task.sourceSetNames) {
                    project.tasks.getAt(getKaptTaskNameFor(sourceSet))
                        .outputs.dir(getLinkDataDirFor(sourceSet))
                }
            }
        }

        fun getKaptTaskNameFor(sourceSetName: String): String {
            val sourceSetPart = if (sourceSetName == DEFAULT_SOURCE_SET_NAME) "" else sourceSetName.capitalize()
            return "$KAPT_NAMED_OBJECT_PREFIX${sourceSetPart}Kotlin${compilationTarget.capitalize()}"
        }
    }

    companion object {
        private const val DEFAULT_SOURCE_SET_NAME = "main"
        private const val KAPT_NAMED_OBJECT_PREFIX = "kapt"
        // TODO use dynamic generated version
        private const val ANNOTATION_PROCESSOR_DEPENDENCY_NOTATION = "de.creatorcat.pard:pard-cova-gradle:0.2+"
    }
}
