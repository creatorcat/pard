/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.pard.cova.gradle

import de.creatorcat.kotiya.core.text.CREATOR_CAT_CLI_HELLO
import de.creatorcat.pard.cova.gradle.PardCovaKotlinPlugin.Companion.GATHER_ANNO_TASK_NAME
import de.creatorcat.pard.model.simple.SimpleRequirementsModelProvider
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.jetbrains.kotlin.gradle.dsl.KotlinMultiplatformExtension
import org.jetbrains.kotlin.gradle.dsl.KotlinProjectExtension
import org.jetbrains.kotlin.gradle.plugin.KotlinSourceSet

/**
 * Pard coverage validation plugin for projects with Kotlin source code.
 *
 * Does the following when applied to a project:
 * * Applies the `kotlin-kapt` plugin that will be used for annotation processing
 * * Adds a [KaptBasedCovaAnnoGatheringTask] named [GATHER_ANNO_TASK_NAME] that already has the default
 *   [compilationTarget][KaptBasedCovaAnnoGatheringTask.compilationTarget] ("jvm" or empty)
 *   and respective [sourceSetNames][KaptBasedCovaAnnoGatheringTask.sourceSetNames]:
 *    * all jvm-target source sets for multi-platform project
 *    * all source sets for non-multi-platform projects
 * * Adds two [PardValidateForwardCoverageTask]s:
 *    * The [FORWARD_IMPLEMENTATION_COVA_TASK_NAME] that is bound to the default gathering tasks
 *      [implementingCodeLinkDataProvider][KaptBasedCovaAnnoGatheringTask.implementingCodeLinkDataProvider]
 *    * The [FORWARD_VERIFICATION_COVA_TASK_NAME] that is bound to the default gathering tasks
 *      [verifyingCodeLinkDataProvider][KaptBasedCovaAnnoGatheringTask.verifyingCodeLinkDataProvider]
 * * The [modelProvider][PardValidateForwardCoverageTask.modelProvider] of both forward cova tasks will be
 *   set to the default parse task of the `pard-lang` plugin if it is also applied to the project.
 *
 * See the user guide for further information.
 */
class PardCovaKotlinPlugin : Plugin<Project> {

    /**
     * Apply the plugin to the [target] project.
     *
     * @see [Plugin.apply]
     */
    override fun apply(target: Project) {
        with(target) {
            logger.info(CREATOR_CAT_CLI_HELLO)
            plugins.apply(KAPT_PLUGIN_ID)
            // register cannot be used here since sourceSetNames setting triggers the creations
            // of an afterEvaluate closure which is not allowed in the lazy-evaluated register closure
            val gatherTask = tasks.create(GATHER_ANNO_TASK_NAME, KaptBasedCovaAnnoGatheringTask::class.java) {
                it.compilationTarget = target.getDefaultCompilationTarget()
                it.sourceSetNames = target.getDefaultKotlinSourceSetNames(it.compilationTarget)
            }
            tasks.register(FORWARD_IMPLEMENTATION_COVA_TASK_NAME, PardValidateForwardCoverageTask::class.java) {
                it.linkDataProvider = gatherTask.implementingCodeLinkDataProvider
                it.usePardLangModelIfPresent(target)
            }
            tasks.register(FORWARD_VERIFICATION_COVA_TASK_NAME, PardValidateForwardCoverageTask::class.java) {
                it.linkDataProvider = gatherTask.verifyingCodeLinkDataProvider
                it.usePardLangModelIfPresent(target)
            }
        }
    }

    // TODO more tests for default getters

    private fun Project.getDefaultCompilationTarget(): String {
        val kotlinEx = extensions.getByName("kotlin")
        return if (kotlinEx is KotlinMultiplatformExtension) {
            kotlinEx.targets.findByName("jvm")?.name ?: ""
        } else
            ""
    }

    private fun Project.getDefaultKotlinSourceSetNames(target: String): List<String> {
        val kotlinEx = extensions.getByName("kotlin") as KotlinProjectExtension
        return if (kotlinEx is KotlinMultiplatformExtension) {
            if (target.isNotBlank())
                kotlinEx.sourceSets.mapNotNull { it.getNameWithoutTarget(target) }
            else
                emptyList()
        } else
            kotlinEx.sourceSets.map { it.name }
    }

    private fun KotlinSourceSet.getNameWithoutTarget(target: String): String? =
        if (name.startsWith(target))
            name.drop(target.length).decapitalize()
        else
            null

    private fun PardValidateForwardCoverageTask.usePardLangModelIfPresent(project: Project) {
        if (project.plugins.hasPlugin(PARD_LANG_PLUGIN_ID)) {
            modelProvider = project.tasks.getByName(PARD_LANG_PARSE_TASK_NAME) as SimpleRequirementsModelProvider
        }
    }

    companion object {
        /** The plugin ID - used when applying the plugin in a build script: `de.creatorcat.pard.cova-kotlin` */
        const val ID: String = "de.creatorcat.pard.cova-kotlin"
        /** name of the default cova annotation gathering task: `pardGatherCovaAnnotations` */
        const val GATHER_ANNO_TASK_NAME: String = "pardGatherCovaAnnotations"
        /** name of the default implementation forward validation task: `pardValidateImplementationForwardCoverage` */
        const val FORWARD_IMPLEMENTATION_COVA_TASK_NAME: String = "pardValidateImplementationForwardCoverage"
        /** name of the default verification forward validation task: `pardValidateVerificationForwardCoverage` */
        const val FORWARD_VERIFICATION_COVA_TASK_NAME: String = "pardValidateVerificationForwardCoverage"

        // TODO use generated constants
        internal const val KAPT_PLUGIN_ID = "org.jetbrains.kotlin.kapt"
        /** name of the gradle task group for pard tasks */
        const val PARD_TASK_GROUP: String = "specification"
        internal const val PARD_LANG_PLUGIN_ID = "de.creatorcat.pard.lang"
        internal const val PARD_LANG_PARSE_TASK_NAME = "pardParseSpec"
    }
}
