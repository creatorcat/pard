/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.pard.cova.gradle

import de.creatorcat.kotiya.core.predicates.Predicate
import de.creatorcat.kotiya.gradle.apply
import de.creatorcat.kotiya.gradle.isDirectTaskDependency
import de.creatorcat.kotiya.gradle.logJavaStyleError
import de.creatorcat.kotiya.gradle.logJavaStyleWarning
import de.creatorcat.pard.model.code.CodeToArtifactLinkDataProvider
import de.creatorcat.pard.model.simple.ForwardCovaResult
import de.creatorcat.pard.model.simple.SimpleModelForwardCoverageValidator
import de.creatorcat.pard.model.simple.SimpleModelValidationFaultGenerator
import de.creatorcat.pard.model.simple.SimpleRequirementsArtifact
import de.creatorcat.pard.model.simple.SimpleRequirementsModelProvider
import groovy.lang.Closure
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.Console
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.TaskAction
import java.util.concurrent.Callable

/**
 * A task to validate the forward coverage of requirements.
 * Validates that for each requirement artifact in the [provided model][modelProvider]
 * there is a code element in the [provided link data][linkDataProvider] that links to it.
 *
 * Configuration in Gradle (with default values):
 *
 *     task myForwardCovaTask(type: PardValidateForwardCoverageTask) {
 *
 *         // Filter to select the artifacts that shall be considered on validation.
 *         // defaults to the trivial filter `{ true }`
 *         filterArtifacts {
 *             // the closure's receiver is a SimpleRequirementsArtifact, so all properties of this type may be used
 *             type == "MyReqType" && !title.contains("ignore me")
 *         }
 *
 *         // toggle validation fault handling
 *         failOnMissingLinkTarget = true
 *         failOnUncoveredArtifact = false
 *
 *         // null for new tasks
 *         // the default tasks of the pard-cova-plugin are bound to the default parse task of pard-lang if present
 *         modelProvider = myModelProvider
 *
 *         // null for new tasks
 *         // the default tasks of the pard-cova-plugin are bound to the default annotation gathering task providers
 *         linkDataProvider = myLinkDataProvider
 *     }
 */
open class PardValidateForwardCoverageTask : DefaultTask() {

    init {
        description = "validates requirements model forward coverage based on link data"
        group = PardCovaKotlinPlugin.PARD_TASK_GROUP
    }

    /**
     * Specifies whether the task shall fail if a code element links to a non-existing artifact.
     * If `false`, the task will still generate respective warnings.
     *
     * Defaults to `true`.
     */
    @Input
    var failOnMissingLinkTarget: Boolean = true
    /**
     * Specifies whether the task shall fail if any requirements artifact is not covered.
     * If `false`, the task will still generate respective warnings.
     *
     * Defaults to `false`.
     */
    @Input
    var failOnUncoveredArtifact: Boolean = false

    /**
     * Provider of the requirements model to be validated.
     * If the provider is a [direct task dependency][isDirectTaskDependency],
     * this task will depend on the associated task.
     *
     * This may be a `PardParseTask` for example.
     */
    @Console
    var modelProvider: SimpleRequirementsModelProvider? = null
        set(value) {
            // on first non-null provider set it as potential dependency
            if (field == null && value != null)
                dependsOn(Callable { modelProvider.takeIf(::isDirectTaskDependency) })
            field = value
        }

    /**
     * Provider of the code-to-artifact link data to be validated.
     * If the provider is a [direct task dependency][isDirectTaskDependency],
     * this task will depend on the associated task.
     *
     * This may be a provider of a [KaptBasedCovaAnnoGatheringTask] for example.
     */
    @Console
    var linkDataProvider: CodeToArtifactLinkDataProvider? = null
        set(value) {
            // on first non-null provider set it as potential dependency
            if (field == null && value != null)
                dependsOn(Callable { linkDataProvider.takeIf(::isDirectTaskDependency) })
            field = value
        }

    /**
     * Filter to select the artifacts that shall be considered on [validate];
     * defaults to the trivial filter `{ true }`
     */
    @Console
    var artifactFilter: Predicate<SimpleRequirementsArtifact> = { true }

    /**
     * Gradle script convenience method to define the [artifactFilter] by the given [filter] closure.
     */
    fun filterArtifacts(filter: Closure<Boolean>) {
        artifactFilter = {
            it.apply(filter)
        }
    }

    /**
     * Performs the forward coverage validation based on the configured parameters.
     */
    @TaskAction
    fun validate() {
        val existingModelProvider = modelProvider ?: throw IllegalStateException("no model provider defined")
        val existingLinkDataProvider = linkDataProvider ?: throw IllegalStateException("no link data provider defined")
        val validator = SimpleModelForwardCoverageValidator(artifactFilter)
        val result = validator.validate(existingModelProvider.model, existingLinkDataProvider.data)
        with(
            SimpleModelValidationFaultGenerator(
                existingLinkDataProvider.coverageDescribingVerb,
                existingLinkDataProvider.codeToArtifactLinkName
            )
        ) {
            validateLinkTargets(result)
            validateArtifactCoverage(result)
        }
    }

    private fun SimpleModelValidationFaultGenerator.validateLinkTargets(result: ForwardCovaResult) {
        var hasFaults = false
        for (link in result.missingTargetLinkData) {
            printFault(
                failOnMissingLinkTarget,
                generateMissingCodeLinkTargetFault(link.artifactId, link.codeElement)
            )
            hasFaults = true
        }
        if (hasFaults && failOnMissingLinkTarget) throwValidationFailed()
    }

    private fun SimpleModelValidationFaultGenerator.validateArtifactCoverage(result: ForwardCovaResult) {
        for (artifact in result.uncoveredArtifacts)
            printFault(
                failOnUncoveredArtifact,
                generateArtifactUncoveredFault(artifact)
            )
        if (failOnUncoveredArtifact && result.uncoveredArtifacts.isNotEmpty()) throwValidationFailed()
    }

    private fun throwValidationFailed(): Nothing =
        throw RuntimeException("coverage validation failed, see log for error details")

    private fun printFault(isError: Boolean, reason: String) =
        if (isError)
            logJavaStyleError("coverage validation error", reason)
        else
            logJavaStyleWarning("coverage validation warning", reason)
}
