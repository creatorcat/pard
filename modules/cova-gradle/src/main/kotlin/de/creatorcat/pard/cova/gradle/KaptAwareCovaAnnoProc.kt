/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.pard.cova.gradle

import de.creatorcat.pard.cova.annotation.DataDirOptionCovaAnnoProc
import java.io.File
import java.nio.file.Paths
import kotlin.reflect.KClass

/**
 * Extension of the [DataDirOptionCovaAnnoProc] that is based on `kotlin-kapt`.
 * It uses the [link data dir option][DataDirOptionCovaAnnoProc.ARGUMENT_LINK_DATA_DIR]
 * to define the main link data directory but uses another sub directory for each processed source set.
 * The currently processed source set information is retrieved from the kapt processor options.
 */
open class KaptAwareCovaAnnoProc : DataDirOptionCovaAnnoProc() {

    private lateinit var sourceSetLinkDataDir: File

    override fun <A : Annotation> getLinkDataFileFor(annoClass: KClass<A>): File {
        return File(sourceSetLinkDataDir, getLinkDataFileNameFor(annoClass))
    }

    /**
     * Initializes the [mainLinkDataDir] and a source set specific sub dir for each processed source set.
     */
    override fun initialize() {
        super.initialize()

        val kaptKotlinSourceDir = processingEnv.options[KAPT_ARGUMENT_KOTLIN_GENERATED_SRC_DIR]
            ?: throw IllegalStateException("Kapt option $KAPT_ARGUMENT_KOTLIN_GENERATED_SRC_DIR undefined")
        val sourceSetName = Paths.get(kaptKotlinSourceDir).fileName?.toString()
            ?: throw IllegalStateException("Kapt generated kotlin source dir is invalid: $kaptKotlinSourceDir")
        sourceSetLinkDataDir = File(mainLinkDataDir, sourceSetName)

        sourceSetLinkDataDir.mkdirs()
        check(sourceSetLinkDataDir.exists()) {
            "failed to create source set specific link data directory: $sourceSetLinkDataDir"
        }
        logInfo("using source set specific link data directory: $sourceSetLinkDataDir")
    }

    companion object {
        internal const val KAPT_ARGUMENT_KOTLIN_GENERATED_SRC_DIR = "kapt.kotlin.generated"
    }
}
