/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

//////////////////////////////////////////////////////////////////////
// Bintray publishing
//
// Supports JVM and MPP-with-JVM projects.
//
// Supported options:
//
// cccProjectName: used as name of the published bintray package
// cccProjectRepoUrl: used as vcsUrl of the published bintray package if not cccClosedSource
//
// cccNoPublish: set true if project shall not be published
// cccClosedSource: set true if project sources shall not be published (only binaries then)
//
// cccKotlinProject: kotlin project type (see kotlin.gradle)
// cccKotlinMPPPlatforms: list of platforms supported by a mpp-project (see kotlin.gradle)
//
//////////////////////////////////////////////////////////////////////

cccHandlers += { project ->
    if (project.cccNoPublish) return

    def bintrayPublications = []
    switch (project.cccKotlinProject) {
        case "mpp":
            project.kotlin.targets.each { target ->
                bintrayPublications.add(target.name)
            }
            break
        case "jvm":
            bintrayPublications.add("jvm")
            break
        default:
            throw new IllegalArgumentException("unknown kotlin project type: ${project.cccKotlinProject}")
    }
    // publish binaries only if closed source
    if (project.cccClosedSource) bintrayPublications.eachWithIndex { publication, index ->
        bintrayPublications[index] = "${publication}Bin"
    }
    configureBintrayPublishing(project, bintrayPublications)
}

def configureBintrayPublishing(project, bintrayPublications) {
    configure(project) {
        apply plugin: "com.jfrog.bintray"

        bintray {
            // bintray properties should be specified in gradle.properties at GRADLE_USER_HOME
            user = project.hasProperty("BINTRAY_USER") ? BINTRAY_USER : "0"
            key = project.hasProperty("BINTRAY_API_KEY") ? BINTRAY_API_KEY : "0"
            pkg {
                repo = "creatorcat"
                // publish each module as own package
                name = cccFullModuleName
                desc = project.description
                publicDownloadNumbers = true
                licenses = ["Apache-2.0"]
                if (!cccClosedSource) vcsUrl = cccProjectRepoUrl
                version {
                    name = project.version
                    released  = new Date()
                }
            }
            publications = bintrayPublications
        }

        bintrayUpload.doFirst {
            if (project.version.endsWith("SNAPSHOT"))
                throw new IllegalStateException("cannot upload snapshot to bintray")
        }
    }
}

